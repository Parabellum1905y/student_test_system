-- dropdb AURA_SHOP;
-- createdb AURA_SHOP;
CREATE TABLESPACE TESTING location '/home/postgres/TESTING';
CREATE DATABASE "TESTING" OWNER postgres TABLESPACE TESTING;
-- If you have a dedicated database server
-- with 1GB or more of RAM, a reasonable starting value for shared_buffers is 25% of the memory in your system.
-- see in postgresql.conf
-- SET seq_page_cost = 0.1;
-- SET random_page_cost = 0.1;
-- SET cpu_tuple_cost = 0.05;
-- SET effective_cache_size = '3GB'; 