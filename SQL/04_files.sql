-- postgreSQL
-- основная таблица с файлами
CREATE SEQUENCE fileId_seq;
CREATE TABLE files (
	 fileId bigint default nextval('fileId_seq'), -- id файла в БД
	 userId BIGINT not null,  --
	 name VARCHAR (200), -- наименование файлй в файловой системе
	 realName VARCHAR (200),  -- наименование файла (как видит его пользователь)
	 size BIGINT, -- размер файла
	 mediaFormat smallint, -- формат файла 0 - other, 1 - application, 2 - audio, 3 - image, 4 - text, 5 - video, 6 - application/pdf
	 isDirectory smallint,  -- является ли директорией
	 parentId bigint, -- id директории, в которой лежит файл
	 createDate TIMESTAMP DEFAULT current_timestamp, -- дата создания
	 depth smallint, -- глубина. относительно корня
	 hidden smallint, -- является ли скрытым
	 readonly smallint, -- только для чтения
	 state smallint, -- удален или не удален
	 width smallint, -- ширина фото(Для типа файла 3 и 5)
	 height smallint, -- длина фото(Для типа файла 3 и 5)
	 PRIMARY KEY( fileId ),
	 Foreign Key (userId) references USERS (UserID)
) ;

-- таблица для управлений доступами к файлам
CREATE SEQUENCE idLine_seq;

CREATE TABLE fileAccesses (
	 accessId bigint default nextval('idLine_seq'),  -- Id записи в таблице
	 fileId bigint , -- id файла в БД
	-- groupName VARCHAR (200),  -- наименование группы
	 memberType smallint, -- определяет это группа или пользователь (1 - пользователь, 2 - группа)
	 memberId bigint, -- логин или наименование группы
	 isOwner smallint, -- Является ли пользователь создателем файла
	 statusUse smallint, -- 1 - активная запись, 0 - неактивная (можно удалять)
	 PRIMARY KEY( accessId ),
	 Foreign Key (fileId) references files (fileId)
) ;