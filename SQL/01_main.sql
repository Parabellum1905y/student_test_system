------------------------------------
-- DB AURA_SHOP
CREATE SEQUENCE user_id_seq;
CREATE TABLE USERS(
	UserID 			bigint default nextval('user_id_seq'),
	userLogin 		varchar(100) NOT NULL UNIQUE,
	userPass 		varchar(400) NOT NULL,
	userMail		varchar(200),
	hash			varchar(200),
	enabled 		smallint not null default 1,
	rememberMe		smallint default 0,
	Surname 		varchar(100),
	Name 			varchar(100),
	middlename		varchar(100),
	birthDay 		date,
	nick 			varchar(50) NOT NULL,
	avatarFileId	bigint,
	sex				smallint default -1,
	aboutMe			varchar (1024),
	timeZone 		float,
	phone			varchar (15),
	country 		bigint,
	city 			varchar (256),
	address 		varchar (512),
	RegisterDate 	timestamp NOT NULL default current_timestamp,
	isTempGuestUser smallint NOT NULL default 0,
	Primary Key(UserID)
);
alter table users add constraint unique_userMail UNIQUE(userMail);

INSERT INTO USERS (UserID, userLogin, userPass, userMail, nick)
values
(1, 'guest', 'guest', 'guest@guest', 'guest'),
(2, 'test', 'test', 'test@test.test', 'test'),
(3, 'admin', 'admin', 'admin@admin.com', 'admin');

SELECT setval('user_id_seq', (SELECT MAX(UserID) FROM USERS));

CREATE SEQUENCE user_log_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

-- section:main log
CREATE TABLE USER_LOG(
	LogID 			bigserial NOT NULL,
	UserID 			bigint NOT NULL,
	UserIP			VARCHAR(20) NOT NULL,
	SessionId		VARCHAR(50) NOT NULL,
	LogAction		Smallint not null default 0, -- if 0 = log In, if 1 = log Out, 2 = Payment Access, 3 = Payment cancel, 
	--4 -- pwd recovery request, -- 5 - change pwd
	RowDate			timestamp NOT NULL default current_timestamp,
	CONSTRAINT user_log_id_pk PRIMARY KEY (LogID),
	Foreign Key (UserID) references USERS(UserID)
)WITH ( OIDS=FALSE );

CREATE OR REPLACE FUNCTION user_log_insert_trigger()
    RETURNS trigger AS
$BODY$
DECLARE
    table_master    varchar(255)        := 'USER_LOG';
    table_part      varchar(255)        := '';	
    found_table     boolean				:= false;
BEGIN
    -- set partition --------------------------------------------------
    table_part := table_master 
                    || '_y' || date_part( 'year', NEW.RowDate )::text 
                    || '_m' || date_part( 'month', NEW.RowDate )::text 
                    || '_d' || date_part( 'day', NEW.RowDate )::text;
    -- check partition --------------------------------
IF NOT EXISTS (
    SELECT 1
    FROM  pg_catalog.pg_tables 
    WHERE lower(tablename)=lower(table_part::text)
    ) THEN
        -- create new partition --------------------------
		EXECUTE '
		    CREATE TABLE ' || table_part || '
		    (
		        id bigint NOT NULL DEFAULT nextval(''' || table_master || '_id_seq''::regclass),
		        CONSTRAINT ' || table_part || '_id_pk PRIMARY KEY (id)
		    )
		    INHERITS ( ' || table_master || ' )
		    WITH ( OIDS=FALSE )'; 
        -- Create index -------------------------------
        EXECUTE '
            CREATE INDEX ' || table_part || '_adid_date_index
            ON ' || table_part || '
            USING btree
            (UserID, RowDate)';
    END IF;
 
    -- insert data to partition --------------------------------------------
    EXECUTE '
        INSERT INTO ' || table_part || ' 
        SELECT ( (' || quote_literal(NEW) || ')::' || TG_RELNAME || ' ).*';
 
    RETURN NULL;
END;
$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;

CREATE TRIGGER user_log_insert_trigger
  BEFORE INSERT
  ON USER_LOG
  FOR EACH ROW
  EXECUTE PROCEDURE user_log_insert_trigger();
  
create SEQUENCE registr_id_seq;
create TABLE USER_REGISTRATION_LOG(
	RegId 		bigint default nextval('registr_id_seq'),
	hashTag		varchar (200) not null,
	ExpireDate	timestamp NOT NULL,
	userId		bigint NOT NULL,
	enabled		smallint not null default 0,
	Primary Key (RegId),
	Foreign Key (userId) references USERS (userId)
);


create SEQUENCE group_id_seq;
CREATE TABLE USER_GROUPS (
	GroupID 		bigint default nextval('group_id_seq'),
	GroupName 		varchar (100),
	GroupDescr 		varchar (100),
	groupUtfName	varchar (100),
	ENABLED 		smallint not null default 1,
	hidden			smallint not null default 0,
	Primary Key (GroupID)
);

INSERT INTO USER_GROUPS (GroupID, GroupName, GroupDescr, groupUtfName, hidden) values
(1, 'root', 'root', 'key:root', 1),
(2, 'all', 'все', 'key:allusers', 1),
(3, 'moder', 'moderators', 'key:moderusers', 0),
(4, 'developers', 'developers', 'key:devusers', 1),
(5, 'allNoGuest', 'no guests', 'key:noguests', 0);

SELECT setval('group_id_seq', (SELECT MAX(GroupID) FROM USER_GROUPS));

CREATE TABLE MEMBER_CATEGORY(
	CAT_ID 			Smallint NOT NULL UNIQUE,
	CAT_NAME 		varchar (100) NOT NULL,
	ENABLED 		smallint not null default 1,
	Primary Key (CAT_ID)
);

INSERT INTO MEMBER_CATEGORY (CAT_ID, CAT_NAME)
VALUES
(1, 'Пользователь'),
(2, 'Группа'),
(3, 'Компания');

CREATE SEQUENCE group_rel_id_seq;
CREATE TABLE GROUP_REL (
	groupRelId	 	BIGINT NOT NULL default nextval('group_rel_id_seq'),
	groupId 		BIGINT NOT NULL,
	typeId	 		SMALLINT NOT NULL DEFAULT 1, -- user or group member
	objectId 		BIGINT NOT NULL,
	enabled 		smallint not null default 1,
	isowner         smallint not null default 0,
	Primary Key (groupRelId),
	Foreign Key (groupId) references USER_GROUPS (GroupID),
	Foreign Key (typeId) references MEMBER_CATEGORY (CAT_ID)
);

INSERT INTO GROUP_REL (groupId, typeId, objectId) values
(1, 1, 2),
(2, 1, 1);

INSERT INTO GROUP_REL (groupId, typeId, objectId) values
(1, 1, 3);

INSERT INTO GROUP_REL (groupId, typeId, objectId) values
(2, 1, 1);

-- test
INSERT INTO GROUP_REL (groupId, typeId, objectId) values
(2, 2, 3), -- moders in all
(3, 2, 4), -- developers in moders
(4, 1, 3);

-- insert admin to groupRel adminID = 3
-- admin is owner of ALL-group (id = 2)
INSERT INTO GROUP_REL (groupId, typeId, objectId, isowner) values
(2, 1, 3, 1);

INSERT INTO USER_GROUPS (GroupID, GroupName, GroupDescr, groupUtfName, hidden) values
(6, 'for_test', 'for_test', 'key:for_test', 0);
INSERT INTO GROUP_REL (groupId, typeId, objectId, isowner) values
(6, 1, 6, 1);

SELECT setval('group_rel_id_seq', (SELECT MAX(groupRelId) FROM GROUP_REL));


-- ############################################################################################
-- intersection of two arrays of int
create or replace function array_intersect(a1 bigint[], a2 bigint[]) returns int as $$
declare
    ret int;
begin
    -- The reason for the kludgy NULL handling comes later.
    if a1 is null then
        return 0;
    elseif a2 is null then
        return 0;
    end if;
    if exists (select 1
    from (
        select unnest(a1)
        intersect
        select unnest(a2)
    ) as T) then ret=1;
    else ret=0;
    end if;
    return ret;
end;
$$ language plpgsql;