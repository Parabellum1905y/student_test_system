-- Supported Languages
CREATE SEQUENCE lan_id_seq;
CREATE TABLE LANGUAGES (
	langId			BIGINT NOT NULL default nextval('lan_id_seq'),
	langNameUtf		VARCHAR (150) NOT NULL UNIQUE,
	langCode		VARCHAR (6) NOT NULL UNIQUE,
	langLocale		VARCHAR (6) NOT NULL UNIQUE,
	PRIMARY KEY (langId)
);

INSERT INTO LANGUAGES (langId, langNameUtf, langCode, langLocale)values
(1, 'Русский', 'ru', 'ru_ru'),
(2, 'English', 'en', 'en_gb'),
(3, 'Кыргызча', 'ky','ky_kg');

SELECT setval('lan_id_seq', (SELECT MAX(langId) FROM LANGUAGES));