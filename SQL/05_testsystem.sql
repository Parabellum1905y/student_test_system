/*
DROP table ANSWERS_LOG;
DROP table TESTS_LOG;
DROP table ANSWERS;
DROP table TASKS;
DROP table SHORT_TESTS_LINKS;
DROP table TESTS_ACCESS;
DROP table TEST_REPEAT_RULES;
DROP table TESTS;
DROP SEQUENCE tests_id_seq;
DROP SEQUENCE tasks_id_seq;
DROP SEQUENCE answers_id_seq;
DROP SEQUENCE test_log_id_seq;
*/

CREATE SEQUENCE tests_id_seq;
CREATE TABLE TESTS (
    testId bigint       default nextval('tests_id_seq'),
    langId bigint       not null,
    creatorId bigint    not null,
    testName            varchar(512) not null,
    testDescription     text,
    createDate          TIMESTAMP not null default current_timestamp,
    lastModifiedDate    TIMESTAMP not null default current_timestamp,
    randomTasks         smallint default 0,--  рандомная сортировка вопросов
    randomAnswers       smallint default 0,--  рандомная сортировка ответов
    posibilityToJumpTask smallint default 0,--  возможность пропускать вопросы и возвращаться
    jumpTaskFine        smallint default 0,--   штраф за перескакивание одного вопроса в баллах
    testExecTime        integer default 0, --  время на выполнение теста, в секундах(например 1 час необходимо сконвертировать в 3600 сек)
    maxTimePerTask      integer default 0,--  максимальное время , позволительное на 1 вопрос, в секундах(если общее время
    --  3600 и время на ответ 3600 - значит студент может весь тест просидеть над одним ответом и сдуться)
    useTaskedTime       smallint default 0,-- флаг, показывающий, какое время мы должны использовать.
    -- например, если флаг равен нулю - используем общее время testExecTime и maxTime PerTask
    -- если флаг равен 1 - то максимальное время ответа на вопрос задается в самом вопросе. при этом
    -- нельзя использовать остаток времени в вопросе для других ответов на другие вопросы.
    allowViewDetailedResults smallint default 0, -- разрешить тестируемому просмотр подробных результатов
      -- если значение флага равно нулю - пользователь увидит простой отчет формата:
      -- Вы набрали Х баллов. Y правильных ответов. Z неправильных ответов. G процентов ответов верных.
    allowHideResult     smallint not null default 0, -- разрешить тестируемому скрыть свой результат
    activeCopy          smallint not null default 1,
	Primary Key (testId),
	Foreign Key (langId) references LANGUAGES (langId),
	Foreign Key (creatorId) references USERS (userId)
);

/**
* таблица в которой показано, как часто (с каким временным интервалом )разным пользователям и группам
* разрешено пересдавать тест. Время считается относительно даты и времени последней пересдачи
*/
create table TEST_REPEAT_RULES(
    testId      bigint not null,
    memberId    Smallint not null,
    subjectId   bigint not null,
    period      bigint not null default 0, -- период в секундах
    Foreign Key (testId) references TESTS (testId) on delete cascade,
    Foreign Key (memberId) references MEMBER_CATEGORY(CAT_ID)
);

create table TESTS_ACCESS (
    testId      bigint not null,
    memberId    Smallint not null,
    subjectId   bigint not null,
    read        smallint not null default 0,
    write       smallint not null default 0,
    exec        smallint not null default 0,
    execByLink  smallint not null default 0,
    Foreign Key (testId) references TESTS (testId) on delete cascade,
    Foreign Key (memberId) references MEMBER_CATEGORY(CAT_ID)
);


/*
* таблица, хранящая сведения о прямых ссылках на тесты
*/
create table SHORT_TESTS_LINKS (
    testId      bigint not null,
    link        varchar (200),
    Primary Key (link),
    Foreign Key (testId) references TESTS (testId) on delete cascade
);

CREATE SEQUENCE tasks_id_seq;
create table TASKS (
    taskId          bigint default nextval('tasks_id_seq'), -- идентификатор вопроса
    testId          bigint not null, -- идентификатор теста
    theme           varchar (512), -- тема вопроса - опционально
    title           varchar (1024), -- заголовок вопроса - опционально
    task            text not null,-- текст самого вопроса
    jumpTaskFine    smallint default 0,--   штраф за перескакивание конкретного вопроса в баллах
    taskPoint       smallint not null default 0,-- балл за правильный ответ
    taskFine        smallint not null default 0,-- штраф за неправильный ответ
    randomAnswers   smallint not null default 0,--  рандомная сортировка ответов конкретного вопроса
    multiAnswer     smallint not null default 0,--  множественный выбор ответов
    testExecTime    integer default 0, --  время на выполнение вопроса, в секундах
    --(например 1 час необходимо сконвертировать в 3600 сек). если у теста useTaskedTime == 0 - это время
    -- игнорируется
    Foreign Key (testId) references TESTS (testId) on delete cascade,
    Primary Key (taskId)
);

CREATE SEQUENCE answers_id_seq;

create table ANSWERS (
    answerId          bigint default nextval('answers_id_seq'), -- идентификатор ответа
    taskId          bigint not null, -- идентификатор вопроса
    answer          text not null,-- текст самого ответа
    isRight         smallint not null default 0, -- является ли ответ верным.
    -- Для одного вопроса может быть несколько верных ответов
    Foreign Key (taskId) references TASKS (taskId) on delete cascade,
    Primary Key (answerId)
);

CREATE SEQUENCE test_log_id_seq;

create table TESTS_LOG (
    testLogId          bigint default nextval('test_log_id_seq'), -- идентификатор лога теста
    testId              bigint not null, -- идентификатор теста
    startDateTime       timestamp NOT NULL default current_timestamp,
    finishDateTime      timestamp NOT NULL default current_timestamp,
    userPoint           integer not null default 0,
    fullPoint           integer not null default 0,
    userId              bigint not null,
    finished            smallint not null default 0,
    Foreign Key (userId) references USERS (userId),
    Primary Key (testLogId)
);

CREATE SEQUENCE task_log_id_seq;

create table TASK_LOG (
    taskLogId          bigint default nextval('task_log_id_seq'), -- идентификатор лога теста
    taskId              bigint not null, -- идентификатор теста
    testLogId           bigint not null,
    seen                smallint not null default 0,
    seenDate            timestamp NOT NULL default current_timestamp,
    remainingTime       integer not null default 0,
    Primary Key (taskLogId),
    Foreign Key (taskId) references TASKS (taskId) on delete cascade,
    Foreign Key (testLogId) references TESTS_LOG (testLogId) on delete cascade
);

create table ANSWERS_LOG (
    answerId          bigint not null,-- идентификатор варианта ответа
    testLogId          bigint not null, -- идентификатор лога теста
    taskId          bigint not null, -- идентификатор вопроса
    answer          text not null,-- текст самого ответа
    Foreign Key (taskId) references TASKS (taskId) on delete cascade,
    Foreign Key (testLogId) references TESTS_LOG (testLogId) on delete cascade,
    Foreign Key (taskId) references TASKS (taskId) on delete cascade,
    CONSTRAINT answer_log_lock UNIQUE (testLogId, taskId, answerId)
);
