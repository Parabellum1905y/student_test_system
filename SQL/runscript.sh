#!/bin/bash
dropdb=$1
# name specified in 00_createdb.sql
dbname=$2
# root path where scripts exists, example /home/luger/work/test_for_jersey/xitrum/SQL/
scriptroot=$3

sudo mkdir -p /home/postgres/TESTING
sudo chown -R postgres:postgres /home/postgres/TESTING
sudo chmod -R 755 /home/postgres/TESTING

if [ -z "$dropdb" ]
then exit 1
fi

if [ -z "$dbname" ]
then exit 1
fi

if [ -z "$scriptroot" ]
then exit 1
fi

if [[ "$dropdb" -eq 'drop=true' ]]; then
    sudo su - postgres -c "dropdb $dbname"
fi

# running scripts
sudo su - postgres -c "echo \"$dbname\""
sudo su - postgres -c "psql -U postgres -c \"\i $scriptroot/00_createdb.sql\""
sudo su - postgres -c "psql -U postgres $dbname -c \"\i $scriptroot/01_main.sql\""
sudo su - postgres -c "psql -U postgres $dbname -c \"\i $scriptroot/02_tags.sql\""
sudo su - postgres -c "psql -U postgres $dbname -c \"\i $scriptroot/03_classifiers.sql\""
sudo su - postgres -c "psql -U postgres $dbname -c \"\i $scriptroot/04_files.sql\""
sudo su - postgres -c "psql -U postgres $dbname -c \"\i $scriptroot/05_testsystem.sql\""

