package util

/**
  * Created by luger on 13.03.16.
  */
trait CaseEnumValue {
  def name:String
  def index:Short
}

trait CaseEnum {
  type V <: CaseEnumValue
  def values:List[V]
  def unapply(name:String):Option[String] = {
    if (values.exists(_.name == name)) Some(name) else None
  }
  def unapply(value:V):(String, Int) = {
    (value.name, value.index)
  }
  def apply(name:String):Option[V] = {
    values.find(_.name == name)
  }
  def apply(index:Short):Option[V] = {
    values.find(_.index == index)
  }
}

abstract class AcceptableMimeTypes (override val name:String, override val index:Short) extends CaseEnumValue

object AcceptableMimeTypes extends CaseEnum{
  type V = AcceptableMimeTypes
  case object bmp extends AcceptableMimeTypes("image/bmp", 1)
  case object gif extends AcceptableMimeTypes("image/gif", 2)
  case object jpg extends AcceptableMimeTypes("image/jpeg", 3)
  case object png extends AcceptableMimeTypes("image/png", 4)
  case object ampeg extends AcceptableMimeTypes("audio/mpeg", 5)
  case object axmpeg extends AcceptableMimeTypes("audio/x-mpeg", 6)
  case object ampeg3 extends AcceptableMimeTypes("audio/mpeg3", 7)
  case object axmpeg3 extends AcceptableMimeTypes("audio/x-mpeg-3", 8)
  case object awav extends AcceptableMimeTypes("audio/wav", 9)
  case object axwav extends AcceptableMimeTypes("audio/x-wav", 10)
  case object pdf extends AcceptableMimeTypes("application/pdf", 11)
  case object msword extends AcceptableMimeTypes("application/msword", 12)
  case object msppt extends AcceptableMimeTypes("application/mspowerpoint", 13)
  case object ppt extends AcceptableMimeTypes("application/powerpoint", 14)
  case object exl extends AcceptableMimeTypes("application/excel", 15)
  case object msexl extends AcceptableMimeTypes("application/x-msexcel", 16)
  case object text extends AcceptableMimeTypes("text/plain", 17)
  case object avi extends AcceptableMimeTypes("video/avi", 18)
  case object mov extends AcceptableMimeTypes("video/quicktime", 19)
  case object vmpeg extends AcceptableMimeTypes("video/mpeg", 20)
  case object vxmpeg extends AcceptableMimeTypes("video/x-mpeg", 21)
  case object vmp4 extends AcceptableMimeTypes("video/mp4", 22)
  var values = List(bmp, gif, jpg, png, ampeg, axmpeg, ampeg3, axmpeg3, awav, axwav, pdf,
                    msword, msppt, ppt, exl, msexl, text, avi, mov, vmpeg, vxmpeg, vmp4)
}
