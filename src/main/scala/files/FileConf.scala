package files

/**
  * Created by luger on 11.03.16.
  */
class FileConfRef {
  lazy val filesConfig = if (xitrum.Config.application.hasPath("files"))
    Some (xitrum.Config.application.getConfig("files"))
  else None
  def config(param:String) = filesConfig match {
    case Some(x)=>x.getString(param)
    case None   =>""
  }

  def rootPath     = config("rootPath")
}

object FileConf{
  def apply:FileConfRef = new FileConfRef
}