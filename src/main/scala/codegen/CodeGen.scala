package codegen

import java.util.concurrent.TimeUnit

import slick.jdbc.meta.MTable
import slick.codegen.SourceCodeGenerator
import scala.concurrent.ExecutionContext.Implicits.global
import db.dbpoolconf.Datasource
import db.PostgresDriverPgExt.api._
import db._
import slick.profile.SqlProfile.ColumnOption
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future}
import db.Crypt
import testing.dao.Db


/**
 *  This customizes the Slick code generator. We only do simple name mappings.
 *  For a more advanced example see https://github.com/cvogt/slick-presentation/tree/scala-exchange-2013
 */
object CustomizedCodeGenerator extends Db {
  import scala.concurrent.ExecutionContext.Implicits.global

  def main(args: Array[String]) = {

    // write the generated results to file
    codegenFuture.onSuccess { case codegen =>
      codegen.writeToFile(
        "db.PostgresDriverPgExt", // use our customized postgres driver
        "tmp/",
        "aura_shop.model",
        "Tables",
        "Tables.scala"
      )
    }
  }
 val slickProfile = PostgresDriverPgExt
//  val db = slickProfile.api.Database.forURL(url,driver=jdbcDriver)
  // filter out desired tables
  val included = Seq("TESTS","TEST_REPEAT_RULES","TESTS_ACCESS", "SHORT_TESTS_LINKS", "TASKS", "ANSWERS", "TESTS_LOG", "ANSWERS_LOG")
  val modelFuture = db.run(slickProfile.createModel(
    Option(slickProfile.defaultTables
      .map(_.filter{m => included.contains(m.name.name.toUpperCase)})
    )))

  val codegenFuture = modelFuture.map { model =>
      new slick.codegen.SourceCodeGenerator(model) {
      override def Table = new Table(_) { table =>
        override def Column = new Column(_) { column =>
          println (s"options ${model.options}, column:${column.options}, col default:${column.defaultCode}, colname:${column.name}")
          // customize db type -> scala type mapping, pls adjust it according to your environment
          override def defaultCode = {
            case Some(v) => {println (s"colname:${column.name},bugoga");s"Some(${defaultCode(v)})"}
            case s:String  => {println (s"colname:${column.name},bugoga1");"\""+s+"\""}
            case None      => {println (s"colname:${column.name},bugoga2");s"None"}
            case v:Byte    => {println (s"colname:${column.name},bugoga3");s"$v"}
            case v:Int     => {println (s"colname:${column.name},bugoga4");s"$v"}
            case v:Long    => {println (s"colname:${column.name},bugoga5");s"${v}L"}
            case v:Float   => {println (s"colname:${column.name},bugoga6");s"${v}F"}
            case v:Double  => {println (s"colname:${column.name},bugoga7");s"$v"}
            case v:Boolean => {println (s"colname:${column.name},bugoga8");s"$v"}
            case v:Short   => {println (s"colname:${column.name},bugoga9");s"$v"}
            case v:Char   => {println (s"colname:${column.name},bugoga10");s"'$v'"}
            case v:BigDecimal => {println ("colname:${column.name},bugoga11");s"new scala.math.BigDecimal(new java.math.BigDecimal($v))"}
            case v => {
                println (s"colname:${column.name},tratata")
                throw new SlickException( s"Dont' know how to generate code for default value $v of ${v.getClass}. Override def defaultCode to render the value." )
              }
          }
          override def rawType: String = model.tpe match {
            case "java.sql.Date" => "java.time.LocalDate"
            case "java.sql.Time" => "java.time.LocalTime"
            case "java.sql.Timestamp" => "java.time.LocalDateTime"
            // currently, all types that's not built-in support were mapped to `String`
            case "String" => model.options.find(_.isInstanceOf[ColumnOption.SqlType])
              .map(_.asInstanceOf[ColumnOption.SqlType].typeName).map({
                case "hstore" => "Map[String, String]"
                case "geometry" => "com.vividsolutions.jts.geom.Geometry"
                case "int8[]" => "List[Long]"
                case _ =>  "String"
              }).getOrElse("String")
            case _ => super.rawType.asInstanceOf[String]
          }
        }
      }

      // ensure to use our customized postgres driver at `import profile.simple._`
      override def packageCode(profile: String, pkg: String, container: String, parentType: Option[String]) : String = {
        s"""
package ${pkg}
// AUTO-GENERATED Slick data model
/** Stand-alone Slick data model for immediate use */
object ${container} extends {
  val profile = ${profile}
} with ${container}
/** Slick data model trait for extension, choice of backend or usage in the cake pattern. (Make sure to initialize this late.) */
trait ${container}${parentType.map(t => s" extends $t").getOrElse("")} {
  val profile: $profile
  import profile.api._
  ${indent(code)}
}
      """.trim()
      }
    }
  }
}