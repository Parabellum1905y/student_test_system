package codegen

import testing.controllers.{ChatRoom, RunTests, UsersLists}
import testing.dao.Db
import testing.dao.tests._
import testing.dao.usersDAO.GroupsDao._
import testing.dao.usersDAO.{ElasticGroupDao, ElasticUserDao, GroupsDao, UserDao}
import testing.model.users.User
import xitrum.util.SeriDeseri

import scala.concurrent.{Await, Future}
import scala.util.{Failure, Success}
import scala.concurrent.ExecutionContext.Implicits.global
import com.typesafe.akka.extension.quartz.QuartzSchedulerExtension
import akka.actor.{ActorSystem, Props}
import glokka.Registry
import testing.controllers.RunTests.Tick

import scala.concurrent.duration.Duration



object TrySlick extends xitrum.Log {
  def main(args: Array[String]):Unit = {
    val elastic = ElasticUserDao.apply
    val elasticTest = ElasticTestDao.apply
    val elasticGroup = ElasticGroupDao.apply
    GroupsDao.searchUsersInGroups(6) onComplete {
      case Success(useq) =>
        log info s"useq: $useq"
      case Failure(er) =>
        log error s"$er"
    }
    UserDao.listAll onComplete {
      case Success(seqUsers) => seqUsers foreach{d => elastic.updateIndex(d)}
    }
    TestsDao.listAll onComplete {
      case Success(seqTests) => seqTests foreach{d => elasticTest.updateIndex(d)}
    }
    GroupsDao.listAll onComplete {
      case Success(seqTests) => seqTests foreach{d => elasticGroup.updateIndex(d)}
    }

    elastic getById 8 onComplete {
      case Success(x) =>
        log info s"hits:${x}"
        log info s"suggests : ${SeriDeseri.toJson(x.sourceAsString)}"
      case Failure(err) =>
        log error s"$err"
    }
    elastic.searchUser("рося") onComplete {
      case Success(x) =>
        log info s"hits:${x.hits.length}"
        val suggests = x.hits.map{hit =>
          Map (hit.getId->hit.getSourceAsString)
        }
        log info s"suggests : ${SeriDeseri.toJson(suggests)}"
      case Failure(err) =>
        log error s"$err"
    }

    elasticTest.searchMatchTest("est") onComplete {
      case Success(x) =>
        log info s"hits tests:${x.hits.length}"
        val suggests = x.hits.map{hit =>
          Map (hit.getId->hit.getSourceAsString)
        }
        log info s"suggests tests: ${SeriDeseri.toJson(suggests)}"
      case Failure(err) =>
        log error s"$err"
    }

    elasticGroup.searchMatchGroup("al") onComplete {
      case Success(x) =>
        log info s"hits tests:${x.hits.length}"
        val suggests = x.hits.map{hit =>
          Map (hit.getId->hit.getSourceAsString)
        }
        log info s"suggests groups: ${SeriDeseri.toJson(suggests)}"
      case Failure(err) =>
        log error s"$err"
    }
    val tasksDao = TasksDao.apply
    tasksDao.mixedArrayOfTasks(1L, shuffle = false) onComplete {
      case Success(x) =>
        log info s"queue : ${x}"
    }
    /*for (a <- 1 to 1000) {
      val taskDao = TasksDao.apply

      taskDao getForTest(1L, 6L, 8L) onComplete {
        case Success(s) => log info s"$s"
        case Failure(err) => log error s"$err"
      }
    }*/
      /*taskDao blaTest(8L) onComplete {
        case Success (s) => log info s"$s"
        case Failure (err) => log error s"$err"
      }*/
      /*taskDao blaTest2(6L, 8L) onComplete {
        case Success (s) => log info s"$s"
        case Failure (err) => log error s"$err"
      }*/
      /*Future.successful(taskDao blaTest3(1L, 6L, 8L))
      taskDao blaTest3(1L, 6L, 8L) onComplete {
        case Success (s) => log info s"$s"
        case Failure (err) => log error s"$err"
      }*/
      /*taskDao getForTest(6L, 8L) onComplete {
        case Success (s) => log info s"$s"
        case Failure (err) => log error s"$err"
      }
      TestsDao.getAccessesFor(6l) onComplete {
        case Success (s) => log info s"$s"
        case Failure (err) => log error s"$err"
      }*/
    /*}*/
    val system    = ActorSystem("MyClusterSystem")
    val proxyName = "my proxy name"
    val registry  = Registry.start(system, proxyName)
    val cleaner = system.actorOf(Props[RunTests])
    val v: QuartzSchedulerExtension = QuartzSchedulerExtension(system)
    v.schedule("cronEvery10Seconds", cleaner, Tick("sdfsdfsdfsfsd"))
    Thread.sleep(500)
    val s = SeriDeseri.toJson(List(MixedTask(1L, 1), MixedTask(2L, 3), MixedTask(4L, 1), MixedTask(6L, 1)))
    println (s"s = $s")
    val w = SeriDeseri.fromJson[List[MixedTask]](s)
    println (s"w = $w")
    val elasticRunTest = ElasticTestRunDao.apply
    elasticRunTest.getTaskMixingQueue(5L) onComplete{
      case Success (x) =>
        log info s"test := ${x.field("queue").getValue.toString}"
        val w2 = SeriDeseri.fromJson[List[Map[String, String]]](x.field("queue").getValue.toString).map{ m =>
          m
        }
        w2.get.foreach(f => println(f))
        println (s"w2 = $w2")
    }
    //QuartzSchedulerExtension()

  }
}

