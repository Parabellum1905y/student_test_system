package elastic

/**
  * Created by luger on 11.03.16.
  */
class ElasticConfRef {
  lazy val filesConfig = if (xitrum.Config.application.hasPath("elastic"))
    Some (xitrum.Config.application.getConfig("elastic"))
  else None
  def config(param:String) = filesConfig match {
    case Some(x)=>x.getString(param)
    case None   =>""
  }

  def sniff       = config("sniff").toBoolean
  def clustername = config("clustername")
  def host        = config("host")
  def port        = config("port").toInt
}

object ElasticConf{
  def apply:ElasticConfRef = new ElasticConfRef
}