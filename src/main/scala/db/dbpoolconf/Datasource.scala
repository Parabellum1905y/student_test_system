package db.dbpoolconf

import java.util
import java.util.Map.Entry

import com.typesafe.config.ConfigValue
import org.apache.commons.dbcp2.BasicDataSource
import slick.jdbc.JdbcBackend.Database
import com.zaxxer.hikari.HikariDataSource
import scala.collection.JavaConversions._

object Conf {
  val poolConfig = if (xitrum.Config.application.hasPath("db"))
    Some (xitrum.Config.application.getConfig("db"))
  else None

  val configList = if (poolConfig.get.hasPath("dblist"))
      poolConfig.get.getConfig("dblist").getStringList("list").toList
  else List()

  val nConf: Map[String, _root_.slick.jdbc.JdbcBackend.DatabaseDef] = configList.map { f =>
    val key = f
    key -> createDS(key)
  }.toMap
  def createDS (key:String) = {
    val nConfig =  if (poolConfig.get.hasPath(key))
      Some (poolConfig.get.getConfig(key))
    else None
    val database = {
      println(" ----------------------------------------- OMGG!!!! ")
      val cpds = new HikariDataSource
      println (nConfig.get)
      cpds.setDriverClassName(nConfig.get.getString("driver"))
      cpds.setJdbcUrl(nConfig.get.getString("url"))
      cpds.setUsername(nConfig.get.getString("user"))
      cpds.setPassword(nConfig.get.getString("pwd"))
      cpds.setMinimumIdle(nConfig.get.getString("min_pool_size").toInt)
      /*If your driver supports JDBC4 we strongly recommend not setting this property*/
      //cpds.setConnectionTestQuery(nConfig.get.getString("check_query"))
      cpds.setMaximumPoolSize(nConfig.get.getString("max_pool_size").toInt)
      cpds.setIdleTimeout(nConfig.get.getString("idleTimeout").toInt)
      cpds.setInitializationFailFast(nConfig.get.getString("failFast").toBoolean)
      cpds.setMaxLifetime(nConfig.get.getString("maxLifetime").toInt)
      //    cpds.setMaxOpenPreparedStatements(nConfig.get.getString("max_statements").toInt)
      Database.forDataSource(cpds)
    }
    database
  }

 /* lazy val nConfig =  if (poolConfig.get.hasPath(name))
    Some (poolConfig.get.getConfig(name))
  else None*/
}
class DatasourceConf(name:String) {
  val database: _root_.slick.jdbc.JdbcBackend.DatabaseDef = Conf.nConf(name)
}

object Datasource{
  def apply (name:String):DatasourceConf = {
    new DatasourceConf (name)
  }  
}