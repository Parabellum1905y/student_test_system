package db
import com.github.tminglei.slickpg._
import org.json4s._
import slick.driver.JdbcProfile
import slick.profile.Capability

trait PostgresDriverPg  extends ExPostgresDriver
  with PgDateSupport
  with PgDate2Support
  with PgArraySupport
  with PgRangeSupport
  with PgJson4sSupport{
  override protected def computeCapabilities: Set[Capability] =
    super.computeCapabilities + JdbcProfile.capabilities.insertOrUpdate
  def pgjson = "jsonb"
  type DOCType = text.Document
  override val jsonMethods = org.json4s.native.JsonMethods
  override val api = MyAPI
  object MyAPI extends API with ArrayImplicits
    with DateTimeImplicits
    with RangeImplicits
    with Json4sJsonImplicits{
    type Range[T] = com.github.tminglei.slickpg.Range[T]

    implicit val json4sJsonArrayTypeMapper =
      new AdvancedArrayJdbcType[JValue](pgjson,
        (s) => utils.SimpleArrayUtils.fromString[JValue](jsonMethods.parse(_))(s).orNull,
        (v) => utils.SimpleArrayUtils.mkString[JValue](j=>jsonMethods.compact(jsonMethods.render(j)))(v)
      ).to(_.toList)
  }
}

object PostgresDriverPgExt extends PostgresDriverPg