package testing.model.classifiers

import slick.lifted.Tag
import slick.jdbc.{GetResult => GR}
import slick.model.ForeignKeyAction
import db.PostgresDriverPgExt.api._

/** Entity class storing rows of table Languages
 *  @param langid Database column langid SqlType(bigserial), AutoInc, PrimaryKey
 *  @param langnameutf Database column langnameutf SqlType(varchar), Length(150,true)
 *  @param langcode Database column langcode SqlType(varchar), Length(6,true) */
case class LanguagesRow(langid: Long, 
                        langnameutf: String, 
                        langcode: String,
                        langlocale:String)

/** Table description of table languages. Objects of this class serve as prototypes for rows in queries. */
class Languages(_tableTag: Tag) extends Table[LanguagesRow](_tableTag, "languages") {
  def * = (langid, langnameutf, langcode, langlocale) <> (LanguagesRow.tupled, LanguagesRow.unapply)
  /** Maps whole row to an option. Useful for outer joins. */
  def ? = (Rep.Some(langid), Rep.Some(langnameutf), Rep.Some(langcode), Rep.Some(langlocale)).
    shaped.<>({r=>import r._; _1.map(_=> LanguagesRow.tupled((_1.get, _2.get, _3.get, _4.get)))},
    (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

  /** GetResult implicit for fetching LanguagesRow objects using plain SQL queries */
  implicit def GetResultLanguagesRow(implicit e0: GR[Long], e1: GR[String]): GR[LanguagesRow] = GR{
    prs => import prs._
    LanguagesRow.tupled((<<[Long], <<[String], <<[String], <<[String]))
  }
  /** Database column langid SqlType(bigserial), AutoInc, PrimaryKey */
  val langid: Rep[Long] = column[Long]("langid", O.AutoInc, O.PrimaryKey)
  /** Database column langnameutf SqlType(varchar), Length(150,true) */
  val langnameutf: Rep[String] = column[String]("langnameutf", O.Length(150,varying=true))
  /** Database column langcode SqlType(varchar), Length(6,true) */
  val langcode: Rep[String] = column[String]("langcode", O.Length(6,varying=true))
  val langlocale: Rep[String] = column[String]("langlocale", O.Length(6,varying=true))

  /** Uniqueness Index over (langcode) (database name languages_langcode_key) */
  val index1 = index("languages_langcode_key", langcode, unique=true)
  /** Uniqueness Index over (langnameutf) (database name languages_langnameutf_key) */
  val index2 = index("languages_langnameutf_key", langnameutf, unique=true)
}

object Languages{
  /** Collection-like TableQuery object for table Languages */
  lazy val Languages = new TableQuery(tag => new Languages(tag))
  def get = Languages
}