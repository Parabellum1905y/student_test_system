package testing.model.classifiers

import slick.lifted.Tag
import slick.jdbc.{GetResult => GR}
import slick.model.ForeignKeyAction
import db.PostgresDriverPgExt.api._

  /** Entity class storing rows of table ClCountries
   *  @param countryid Database column countryid SqlType(bigserial), AutoInc, PrimaryKey
   *  @param code Database column code SqlType(varchar), Length(20,true), Default(None)
   *  @param digcode Database column digcode SqlType(varchar), Length(3,true), Default(None)
   *  @param name Database column name SqlType(varchar), Length(300,true), Default(None)
   *  @param phonecode Database column phonecode SqlType(varchar), Length(20,true), Default(None)
   *  @param status Database column status SqlType(int2), Default(0) */
  case class ClCountriesRow(countryid: Long, 
                            code: Option[String] = None, 
                            digcode: Option[String] = None, 
                            name: Option[String] = None, 
                            phonecode: Option[String] = None, 
                            status: Short = 0)
  
  /** Table description of table cl_countries. Objects of this class serve as prototypes for rows in queries. */
  class ClCountries(_tableTag: Tag) extends Table[ClCountriesRow](_tableTag, "cl_countries") {
    def * = (countryid, code, digcode, name, phonecode, status) <> (ClCountriesRow.tupled, ClCountriesRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(countryid), code, digcode, name, phonecode, Rep.Some(status)).shaped.<>({r=>import r._; _1.map(_=> ClCountriesRow.tupled((_1.get, _2, _3, _4, _5, _6.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))
    /** GetResult implicit for fetching ClCountriesRow objects using plain SQL queries */
    implicit def GetResultClCountriesRow(implicit e0: GR[Long], e1: GR[Option[String]], e2: GR[Short]): GR[ClCountriesRow] = GR{
      prs => import prs._
      ClCountriesRow.tupled((<<[Long], <<?[String], <<?[String], <<?[String], <<?[String], <<[Short]))
    }

    /** Database column countryid SqlType(bigserial), AutoInc, PrimaryKey */
    val countryid: Rep[Long] = column[Long]("countryid", O.AutoInc, O.PrimaryKey)
    /** Database column code SqlType(varchar), Length(20,true), Default(None) */
    val code: Rep[Option[String]] = column[Option[String]]("code", O.Length(20,varying=true), O.Default(None))
    /** Database column digcode SqlType(varchar), Length(3,true), Default(None) */
    val digcode: Rep[Option[String]] = column[Option[String]]("digcode", O.Length(3,varying=true), O.Default(None))
    /** Database column name SqlType(varchar), Length(300,true), Default(None) */
    val name: Rep[Option[String]] = column[Option[String]]("name", O.Length(300,varying=true), O.Default(None))
    /** Database column phonecode SqlType(varchar), Length(20,true), Default(None) */
    val phonecode: Rep[Option[String]] = column[Option[String]]("phonecode", O.Length(20,varying=true), O.Default(None))
    /** Database column status SqlType(int2), Default(0) */
    val status: Rep[Short] = column[Short]("status", O.Default(0))
  }

  object ClCountries{
    /** Collection-like TableQuery object for table ClCountries */
    lazy val ClCountries = new TableQuery(tag => new ClCountries(tag))
    def get = ClCountries
  }

  /** Entity class storing rows of table ClCurrency
   *  @param currid Database column currid SqlType(bigserial), AutoInc, PrimaryKey
   *  @param digcode Database column digcode SqlType(varchar), Length(3,true)
   *  @param charcode Database column charcode SqlType(varchar), Length(3,true)
   *  @param name Database column name SqlType(varchar), Length(100,true) */
  case class ClCurrencyRow(currid: Long, 
                           digcode: String, 
                           charcode: String, 
                           name: String)

  
  /** Table description of table cl_currency. Objects of this class serve as prototypes for rows in queries. */
  class ClCurrency(_tableTag: Tag) extends Table[ClCurrencyRow](_tableTag, "cl_currency") {
    def * = (currid, digcode, charcode, name) <> (ClCurrencyRow.tupled, ClCurrencyRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(currid), Rep.Some(digcode), Rep.Some(charcode), Rep.Some(name)).shaped.<>({r=>import r._; _1.map(_=> ClCurrencyRow.tupled((_1.get, _2.get, _3.get, _4.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))
    /** GetResult implicit for fetching ClCurrencyRow objects using plain SQL queries */
    implicit def GetResultClCurrencyRow(implicit e0: GR[Long], e1: GR[String]): GR[ClCurrencyRow] = GR{
      prs => import prs._
      ClCurrencyRow.tupled((<<[Long], <<[String], <<[String], <<[String]))
    }

    /** Database column currid SqlType(bigserial), AutoInc, PrimaryKey */
    val currid: Rep[Long] = column[Long]("currid", O.AutoInc, O.PrimaryKey)
    /** Database column digcode SqlType(varchar), Length(3,true) */
    val digcode: Rep[String] = column[String]("digcode", O.Length(3,varying=true))
    /** Database column charcode SqlType(varchar), Length(3,true) */
    val charcode: Rep[String] = column[String]("charcode", O.Length(3,varying=true))
    /** Database column name SqlType(varchar), Length(100,true) */
    val name: Rep[String] = column[String]("name", O.Length(100,varying=true))
  }
  
  object ClCurrency {
    /** Collection-like TableQuery object for table ClCurrency */
    lazy val ClCurrency = new TableQuery(tag => new ClCurrency(tag))
    def get = ClCurrency
  }
