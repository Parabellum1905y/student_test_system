package testing.model.tests

import slick.lifted.Tag
import slick.jdbc.{GetResult => GR}
import db.PostgresDriverPgExt.api._
/**
  * Created by luger on 16.05.16.
  */
/** Entity class storing rows of table Tasks
  *  @param taskid Database column taskid SqlType(bigserial), AutoInc, PrimaryKey
  *  @param testid Database column testid SqlType(int8)
  *  @param theme Database column theme SqlType(varchar), Length(512,true), Default(None)
  *  @param title Database column title SqlType(varchar), Length(1024,true), Default(None)
  *  @param task Database column task SqlType(text)
  *  @param jumptaskfine Database column jumptaskfine SqlType(int2), Default(Some(0))
  *  @param taskpoint Database column taskpoint SqlType(int2), Default(0)
  *  @param taskfine Database column taskfine SqlType(int2), Default(0)
  *  @param randomanswers Database column randomanswers SqlType(int2), Default(0)
  *  @param multianswer Database column multianswer SqlType(int2), Default(0)
  *  @param testexectime Database column testexectime SqlType(int4), Default(Some(0)) */
case class TasksRow(taskid: Long,
                    testid: Long,
                    theme: Option[String] = None,
                    title: Option[String] = None,
                    task: String,
                    jumptaskfine: Option[Short] = Some(0),
                    taskpoint: Short = 0,
                    taskfine: Short = 0,
                    randomanswers: Short = 0,
                    multianswer: Short = 0,
                    testexectime: Option[Int] = Some(0))

/** Table description of table tasks. Objects of this class serve as prototypes for rows in queries. */
class Tasks(_tableTag: Tag) extends Table[TasksRow](_tableTag, "tasks") {
  def * = (taskid, testid, theme, title, task,
          jumptaskfine, taskpoint, taskfine,
          randomanswers, multianswer, testexectime) <> (TasksRow.tupled, TasksRow.unapply)
  /** Maps whole row to an option. Useful for outer joins. */
  def ? = (Rep.Some(taskid),
    Rep.Some(testid),
    theme, title,
    Rep.Some(task),
    jumptaskfine,
    Rep.Some(taskpoint),
    Rep.Some(taskfine),
    Rep.Some(randomanswers),
    Rep.Some(multianswer),
    testexectime).shaped.
    <>({r=>import r._; _1.map(_=>
      TasksRow.tupled((_1.get, _2.get, _3, _4, _5.get, _6,
                      _7.get, _8.get, _9.get, _10.get, _11)))},
      (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

  /** GetResult implicit for fetching TasksRow objects using plain SQL queries */
  implicit def GetResultTasksRow(implicit e0: GR[Long],
                                 e1: GR[Option[String]],
                                 e2: GR[String],
                                 e3: GR[Option[Short]],
                                 e4: GR[Short],
                                 e5: GR[Option[Int]]): GR[TasksRow] = GR{
    prs => import prs._
      TasksRow.tupled((<<[Long], <<[Long], <<?[String],
        <<?[String], <<[String], <<?[Short], <<[Short], <<[Short], <<[Short], <<[Short], <<?[Int]))
  }

  /** Database column taskid SqlType(bigserial), AutoInc, PrimaryKey */
  val taskid: Rep[Long] = column[Long]("taskid", O.AutoInc, O.PrimaryKey)
  /** Database column testid SqlType(int8) */
  val testid: Rep[Long] = column[Long]("testid")
  /** Database column theme SqlType(varchar), Length(512,true), Default(None) */
  val theme: Rep[Option[String]] = column[Option[String]]("theme", O.Length(512,varying=true), O.Default(None))
  /** Database column title SqlType(varchar), Length(1024,true), Default(None) */
  val title: Rep[Option[String]] = column[Option[String]]("title", O.Length(1024,varying=true), O.Default(None))
  /** Database column task SqlType(text) */
  val task: Rep[String] = column[String]("task")
  /** Database column jumptaskfine SqlType(int2), Default(Some(0)) */
  val jumptaskfine: Rep[Option[Short]] = column[Option[Short]]("jumptaskfine", O.Default(Some(0)))
  /** Database column taskpoint SqlType(int2), Default(0) */
  val taskpoint: Rep[Short] = column[Short]("taskpoint", O.Default(0))
  /** Database column taskfine SqlType(int2), Default(0) */
  val taskfine: Rep[Short] = column[Short]("taskfine", O.Default(0))
  /** Database column randomanswers SqlType(int2), Default(0) */
  val randomanswers: Rep[Short] = column[Short]("randomanswers", O.Default(0))
  /** Database column multianswer SqlType(int2), Default(0) */
  val multianswer: Rep[Short] = column[Short]("multianswer", O.Default(0))
  /** Database column testexectime SqlType(int4), Default(Some(0)) */
  val testexectime: Rep[Option[Int]] = column[Option[Int]]("testexectime", O.Default(Some(0)))

  /** Foreign key referencing Tests (database name tasks_testid_fkey) */
  lazy val testsFk = foreignKey("tasks_testid_fkey", testid, Tests.get)(
    r => r.testid, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
}

object Tasks {
  /** Collection-like TableQuery object for table Tasks */
  lazy val Tasks = new TableQuery(tag => new Tasks(tag))
  def get = Tasks
}
