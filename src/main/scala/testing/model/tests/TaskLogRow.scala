package testing.model.tests

import java.time.LocalDateTime

import slick.lifted.Tag
import slick.jdbc.{GetResult => GR}
import db.PostgresDriverPgExt.api._
/**
  * Created by luger on 16.05.16.
  */


/** Entity class storing rows of table AnswersLog
  *  @param testlogid Database column testlogid SqlType(int8)
  *  @param taskid Database column taskid SqlType(int8)
  *  */
case class TaskLogRow(tasklogid : Long,
                      taskid: Long,
                      testlogid: Long,
                      seen: Short = 0,
                      seendate:LocalDateTime = LocalDateTime.now,
                      remainingtime:Int = 0)

/** Table description of table answers_log. Objects of this class serve as prototypes for rows in queries. */
class TaskLog(_tableTag: Tag) extends Table[TaskLogRow](_tableTag, "task_log") {
  def * = (tasklogid, taskid, testlogid, seen, seendate, remainingtime) <> (TaskLogRow.tupled, TaskLogRow.unapply)
  /** Maps whole row to an option. Useful for outer joins. */
  def ? = (Rep.Some(tasklogid), Rep.Some(taskid), Rep.Some(testlogid), Rep.Some(seen),
    Rep.Some(seendate),  Rep.Some(remainingtime)).shaped.
    <>({r=>import r._; _1.map(_=> TaskLogRow.tupled((_1.get, _2.get, _3.get, _4.get, _5.get, _6.get)))},
      (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

  /** GetResult implicit for fetching AnswersLogRow objects using plain SQL queries */
  implicit def GetResultAnswersLogRow(implicit e0: GR[Long], e1: GR[String], e2:GR[LocalDateTime]): GR[TaskLogRow] = GR{
    prs => import prs._
      TaskLogRow.tupled((<<[Long], <<[Long], <<[Long], <<[Short], <<[LocalDateTime], <<[Int]))
  }

  /** Database column answerid SqlType(int8) */
  val tasklogid: Rep[Long] = column[Long]("tasklogid", O.AutoInc, O.PrimaryKey)
  /** Database column taskid SqlType(int8) */
  val taskid: Rep[Long] = column[Long]("taskid")
  /** Database column testlogid SqlType(int8) */
  val testlogid: Rep[Long] = column[Long]("testlogid")
  /** Database column answer SqlType(text) */
  val seen: Rep[Short] = column[Short]("seen", O.Default(0))
  val seendate: Rep[LocalDateTime] = column[LocalDateTime]("seendate", O.Default(LocalDateTime.now))
  val remainingtime: Rep[Int] = column[Int]("remainingtime", O.Default(0))

}

object TaskLog {
  /** Collection-like TableQuery object for table AnswersLog */
  lazy val TaskLog = new TableQuery(tag => new TaskLog(tag))
  def get = TaskLog
}


