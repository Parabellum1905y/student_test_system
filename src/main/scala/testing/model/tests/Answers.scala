package testing.model.tests

import slick.lifted.Tag
import slick.jdbc.{GetResult => GR}
import db.PostgresDriverPgExt.api._
/**
  * Created by luger on 16.05.16.
  */
/** Entity class storing rows of table Answers
  *  @param answerid Database column answerid SqlType(bigserial), AutoInc, PrimaryKey
  *  @param taskid Database column taskid SqlType(int8)
  *  @param answer Database column answer SqlType(text)
  *  @param isright Database column isright SqlType(int2), Default(0) */
case class AnswersRow(answerid: Long,
                      taskid: Long,
                      answer: String,
                      isright: Short = 0)

/** Table description of table answers. Objects of this class serve as prototypes for rows in queries. */
class Answers(_tableTag: Tag) extends Table[AnswersRow](_tableTag, "answers") {
  def * = (answerid, taskid, answer, isright) <> (AnswersRow.tupled, AnswersRow.unapply)
  /** Maps whole row to an option. Useful for outer joins. */
  def ? = (Rep.Some(answerid), Rep.Some(taskid), Rep.Some(answer), Rep.Some(isright)).shaped.
            <>({r=>import r._; _1.map(
                _=> AnswersRow.tupled((_1.get, _2.get, _3.get, _4.get)))},
              (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))
  /** GetResult implicit for fetching AnswersRow objects using plain SQL queries */
  implicit def GetResultAnswersRow(implicit e0: GR[Long],
                                   e1: GR[String],
                                   e2: GR[Short]): GR[AnswersRow] = GR{
    prs => import prs._
      AnswersRow.tupled((<<[Long], <<[Long], <<[String], <<[Short]))
  }

  /** Database column answerid SqlType(bigserial), AutoInc, PrimaryKey */
  val answerid: Rep[Long] = column[Long]("answerid", O.AutoInc, O.PrimaryKey)
  /** Database column taskid SqlType(int8) */
  val taskid: Rep[Long] = column[Long]("taskid")
  /** Database column answer SqlType(text) */
  val answer: Rep[String] = column[String]("answer")
  /** Database column isright SqlType(int2), Default(0) */
  val isright: Rep[Short] = column[Short]("isright", O.Default(0))

  /** Foreign key referencing Tasks (database name answers_taskid_fkey) */
  lazy val tasksFk = foreignKey("answers_taskid_fkey", taskid, Tasks.get)(
    r => r.taskid, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
}

object Answers {
  /** Collection-like TableQuery object for table Answers */
  lazy val Answers = new TableQuery(tag => new Answers(tag))
  def get = Answers
}

/** Entity class storing rows of table AnswersLog
  *  @param answerid Database column answerid SqlType(int8)
  *  @param testlogid Database column testlogid SqlType(int8)
  *  @param taskid Database column taskid SqlType(int8)
  *  @param answer Database column answer SqlType(text) */
case class AnswersLogRow(answerid: Long,
                         testlogid: Long,
                         taskid: Long,
                         answer: String)

/** Table description of table answers_log. Objects of this class serve as prototypes for rows in queries. */
class AnswersLog(_tableTag: Tag) extends Table[AnswersLogRow](_tableTag, "answers_log") {
  def * = (answerid, testlogid, taskid, answer) <> (AnswersLogRow.tupled, AnswersLogRow.unapply)
  /** Maps whole row to an option. Useful for outer joins. */
  def ? = (Rep.Some(answerid), Rep.Some(testlogid), Rep.Some(taskid), Rep.Some(answer)).shaped.
            <>({r=>import r._; _1.map(_=> AnswersLogRow.tupled((_1.get, _2.get, _3.get, _4.get)))},
                (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

  /** GetResult implicit for fetching AnswersLogRow objects using plain SQL queries */
  implicit def GetResultAnswersLogRow(implicit e0: GR[Long], e1: GR[String]): GR[AnswersLogRow] = GR{
    prs => import prs._
      AnswersLogRow.tupled((<<[Long], <<[Long], <<[Long], <<[String]))
  }

  /** Database column answerid SqlType(int8) */
  val answerid: Rep[Long] = column[Long]("answerid")
  /** Database column testlogid SqlType(int8) */
  val testlogid: Rep[Long] = column[Long]("testlogid")
  /** Database column taskid SqlType(int8) */
  val taskid: Rep[Long] = column[Long]("taskid")
  /** Database column answer SqlType(text) */
  val answer: Rep[String] = column[String]("answer")

  /** Foreign key referencing Tasks (database name answers_log_taskid_fkey) */
  lazy val tasksFk1 = foreignKey("answers_log_taskid_fkey", taskid, Tasks.get)(
      r => r.taskid, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
  /** Foreign key referencing Tasks (database name answers_log_taskid_fkey1) */
  lazy val tasksFk2 = foreignKey("answers_log_taskid_fkey1", taskid, Tasks.get)(
      r => r.taskid, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
  /** Foreign key referencing TestsLog (database name answers_log_testlogid_fkey) */
  lazy val testsLogFk = foreignKey("answers_log_testlogid_fkey", testlogid, TestsLog.get)(
      r => r.testlogid, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)

  /** Uniqueness Index over (testlogid,taskid,answerid) (database name answer_log_lock) */
  val index1 = index("answer_log_lock", (testlogid, taskid, answerid), unique=true)
}

object AnswersLog {
  /** Collection-like TableQuery object for table AnswersLog */
  lazy val AnswersLog = new TableQuery(tag => new AnswersLog(tag))
  def get = AnswersLog
}
