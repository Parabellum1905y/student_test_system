package testing.model.tests

import slick.lifted.Tag
import slick.jdbc.{GetResult => GR}
import db.PostgresDriverPgExt.api._
import java.time.LocalDateTime
/**
  * Created by luger on 16.05.16.
  */
/** Entity class storing rows of table Tests
  *  @param testid Database column testid SqlType(bigserial), AutoInc, PrimaryKey
  *  @param langid Database column langid SqlType(int8)
  *  @param creatorid Database column creatorid SqlType(int8)
  *  @param testname Database column testname SqlType(varchar), Length(512,true)
  *  @param testdescription Database column testdescription SqlType(text), Default(None)
  *  @param createdate Database column createdate SqlType(timestamp)
  *  @param lastmodifieddate Database column lastmodifieddate SqlType(timestamp)
  *  @param randomtasks Database column randomtasks SqlType(int2), Default(Some(0))
  *  @param randomanswers Database column randomanswers SqlType(int2), Default(Some(0))
  *  @param posibilitytojumptask Database column posibilitytojumptask SqlType(int2), Default(Some(0))
  *  @param jumptaskfine Database column jumptaskfine SqlType(int2), Default(Some(0))
  *  @param testexectime Database column testexectime SqlType(int4), Default(Some(0))
  *  @param maxtimepertask Database column maxtimepertask SqlType(int4), Default(Some(0))
  *  @param usetaskedtime Database column usetaskedtime SqlType(int2), Default(Some(0))
  *  @param allowviewdetailedresults Database column allowviewdetailedresults SqlType(int2), Default(Some(0))
  *  @param allowhideresult Database column allowhideresult SqlType(int2), Default(0)
  *  @param activecopy Database column activecopy SqlType(int2), Default(1) */
case class TestsRow(testid: Long,
                    langid: Long,
                    creatorid: Long,
                    testname: String,
                    testdescription: Option[String] = None,
                    createdate: LocalDateTime = LocalDateTime.now,
                    lastmodifieddate: LocalDateTime = LocalDateTime.now,
                    randomtasks: Option[Short] = Some(0),
                    randomanswers: Option[Short] = Some(0),
                    posibilitytojumptask: Option[Short] = Some(0),
                    jumptaskfine: Option[Short] = Some(0),
                    testexectime: Option[Int] = Some(0),
                    maxtimepertask: Option[Int] = Some(0),
                    usetaskedtime: Option[Short] = Some(0),
                    allowviewdetailedresults: Option[Short] = Some(0),
                    allowhideresult: Short = 0,
                    activecopy: Short = 1)

/** Table description of table tests. Objects of this class serve as prototypes for rows in queries. */
class Tests(_tableTag: Tag) extends Table[TestsRow](_tableTag, "tests") {
  def * = (testid, langid, creatorid,
          testname, testdescription,
          createdate, lastmodifieddate,
          randomtasks, randomanswers,
          posibilitytojumptask, jumptaskfine,
          testexectime, maxtimepertask,
          usetaskedtime, allowviewdetailedresults,
          allowhideresult, activecopy) <> (TestsRow.tupled, TestsRow.unapply)
  /** Maps whole row to an option. Useful for outer joins. */
  def ? = (Rep.Some(testid), Rep.Some(langid),
          Rep.Some(creatorid), Rep.Some(testname),
          testdescription, Rep.Some(createdate),
          Rep.Some(lastmodifieddate), randomtasks,
          randomanswers, posibilitytojumptask,
          jumptaskfine, testexectime,
          maxtimepertask, usetaskedtime,
          allowviewdetailedresults, Rep.Some(allowhideresult),
          Rep.Some(activecopy)).shaped.
          <>({r=>import r._; _1.map(_=>
            TestsRow.tupled((_1.get, _2.get, _3.get, _4.get, _5,
                            _6.get, _7.get, _8, _9, _10,
                            _11, _12, _13, _14, _15,
                            _16.get, _17.get)))},
            (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

  /** GetResult implicit for fetching TestsRow objects using plain SQL queries */
  implicit def GetResultTestsRow(implicit e0: GR[Long],
                                 e1: GR[String],
                                 e2: GR[Option[String]],
                                 e3: GR[LocalDateTime],
                                 e4: GR[Option[Short]],
                                 e5: GR[Option[Int]],
                                 e6: GR[Short]): GR[TestsRow] = GR{
      prs => import prs._
      TestsRow.tupled((<<[Long], <<[Long], <<[Long], <<[String], <<?[String],
                        <<[LocalDateTime], <<[LocalDateTime], <<?[Short],
                        <<?[Short], <<?[Short], <<?[Short], <<?[Int], <<?[Int],
                        <<?[Short], <<?[Short], <<[Short], <<[Short]))
  }

  /** Database column testid SqlType(bigserial), AutoInc, PrimaryKey */
  val testid: Rep[Long] = column[Long]("testid", O.AutoInc, O.PrimaryKey)
  /** Database column langid SqlType(int8) */
  val langid: Rep[Long] = column[Long]("langid")
  /** Database column creatorid SqlType(int8) */
  val creatorid: Rep[Long] = column[Long]("creatorid")
  /** Database column testname SqlType(varchar), Length(512,true) */
  val testname: Rep[String] = column[String]("testname", O.Length(512,varying=true))
  /** Database column testdescription SqlType(text), Default(None) */
  val testdescription: Rep[Option[String]] = column[Option[String]]("testdescription", O.Default(None))
  /** Database column createdate SqlType(timestamp) */
  val createdate: Rep[LocalDateTime] = column[LocalDateTime]("createdate")
  /** Database column lastmodifieddate SqlType(timestamp) */
  val lastmodifieddate: Rep[LocalDateTime] = column[LocalDateTime]("lastmodifieddate")
  /** Database column randomtasks SqlType(int2), Default(Some(0)) */
  val randomtasks: Rep[Option[Short]] = column[Option[Short]]("randomtasks", O.Default(Some(0)))
  /** Database column randomanswers SqlType(int2), Default(Some(0)) */
  val randomanswers: Rep[Option[Short]] = column[Option[Short]]("randomanswers", O.Default(Some(0)))
  /** Database column posibilitytojumptask SqlType(int2), Default(Some(0)) */
  val posibilitytojumptask: Rep[Option[Short]] = column[Option[Short]]("posibilitytojumptask", O.Default(Some(0)))
  /** Database column jumptaskfine SqlType(int2), Default(Some(0)) */
  val jumptaskfine: Rep[Option[Short]] = column[Option[Short]]("jumptaskfine", O.Default(Some(0)))
  /** Database column testexectime SqlType(int4), Default(Some(0)) */
  val testexectime: Rep[Option[Int]] = column[Option[Int]]("testexectime", O.Default(Some(0)))
  /** Database column maxtimepertask SqlType(int4), Default(Some(0)) */
  val maxtimepertask: Rep[Option[Int]] = column[Option[Int]]("maxtimepertask", O.Default(Some(0)))
  /** Database column usetaskedtime SqlType(int2), Default(Some(0)) */
  val usetaskedtime: Rep[Option[Short]] = column[Option[Short]]("usetaskedtime", O.Default(Some(0)))
  /** Database column allowviewdetailedresults SqlType(int2), Default(Some(0)) */
  val allowviewdetailedresults: Rep[Option[Short]] = column[Option[Short]]("allowviewdetailedresults", O.Default(Some(0)))
  /** Database column allowhideresult SqlType(int2), Default(0) */
  val allowhideresult: Rep[Short] = column[Short]("allowhideresult", O.Default(0))
  /** Database column activecopy SqlType(int2), Default(1) */
  val activecopy: Rep[Short] = column[Short]("activecopy", O.Default(1))
}

object Tests {
  /** Collection-like TableQuery object for table Tests */
  lazy val Tests = new TableQuery(tag => new Tests(tag))
  def get = Tests
}

/** Entity class storing rows of table TestsAccess
  *  @param testid Database column testid SqlType(int8)
  *  @param memberid Database column memberid SqlType(int2)
  *  @param subjectid Database column subjectid SqlType(int8)
  *  @param read Database column read SqlType(int2), Default(0)
  *  @param write Database column write SqlType(int2), Default(0)
  *  @param exec Database column exec SqlType(int2), Default(0)
  *  @param execbylink Database column execbylink SqlType(int2), Default(0) */
case class TestsAccessRow(testid: Long,
                          memberid: Short,
                          subjectid: Long,
                          read: Short = 0,
                          write: Short = 0,
                          exec: Short = 0,
                          execbylink: Short = 0)

/** Table description of table tests_access. Objects of this class serve as prototypes for rows in queries. */
class TestsAccess(_tableTag: Tag) extends Table[TestsAccessRow](_tableTag, "tests_access") {
  def * = (testid, memberid, subjectid, read, write, exec, execbylink) <>
            (TestsAccessRow.tupled, TestsAccessRow.unapply)
  /** Maps whole row to an option. Useful for outer joins. */
  def ? = (Rep.Some(testid),
          Rep.Some(memberid),
          Rep.Some(subjectid),
          Rep.Some(read),
          Rep.Some(write),
          Rep.Some(exec),
          Rep.Some(execbylink)).shaped.
        <>({r=>import r._; _1.map(_=>
          TestsAccessRow.tupled((_1.get, _2.get, _3.get, _4.get,
                                  _5.get, _6.get, _7.get)))},
          (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

  /** GetResult implicit for fetching TestsAccessRow objects using plain SQL queries */
  implicit def GetResultTestsAccessRow(implicit e0: GR[Long], e1: GR[Short]): GR[TestsAccessRow] = GR{
    prs => import prs._
      TestsAccessRow.tupled((<<[Long], <<[Short], <<[Long], <<[Short], <<[Short], <<[Short], <<[Short]))
  }

  /** Database column testid SqlType(int8) */
  val testid: Rep[Long] = column[Long]("testid")
  /** Database column memberid SqlType(int2) */
  val memberid: Rep[Short] = column[Short]("memberid")
  /** Database column subjectid SqlType(int8) */
  val subjectid: Rep[Long] = column[Long]("subjectid")
  /** Database column read SqlType(int2), Default(0) */
  val read: Rep[Short] = column[Short]("read", O.Default(0))
  /** Database column write SqlType(int2), Default(0) */
  val write: Rep[Short] = column[Short]("write", O.Default(0))
  /** Database column exec SqlType(int2), Default(0) */
  val exec: Rep[Short] = column[Short]("exec", O.Default(0))
  /** Database column execbylink SqlType(int2), Default(0) */
  val execbylink: Rep[Short] = column[Short]("execbylink", O.Default(0))

  /** Foreign key referencing Tests (database name tests_access_testid_fkey) */
  lazy val testsFk = foreignKey("tests_access_testid_fkey", testid, Tests.get)(
    r => r.testid, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
}

object TestsAccess {
  /** Collection-like TableQuery object for table TestsAccess */
  lazy val TestsAccess = new TableQuery(tag => new TestsAccess(tag))
  def get = TestsAccess
}

/** Entity class storing rows of table TestsLog
  *  @param testlogid Database column testlogid SqlType(bigserial), AutoInc, PrimaryKey
  *  @param testid Database column testid SqlType(int8)
  *  @param startdatetime Database column startdatetime SqlType(timestamp)
  *  @param finishdatetime Database column finishdatetime SqlType(timestamp)
  *  @param userpoint Database column userpoint SqlType(int4), Default(0)
  *  @param fullpoint Database column fullpoint SqlType(int4), Default(0)
  *  @param userid Database column userid SqlType(int8) */
case class TestsLogRow(testlogid: Long, 
                       testid: Long, 
                       startdatetime: LocalDateTime = LocalDateTime.now, 
                       finishdatetime: LocalDateTime = LocalDateTime.now, 
                       userpoint: Int = 0, 
                       fullpoint: Int = 0, 
                       userid: Long,
                       finished:Short = 0)

/** Table description of table tests_log. Objects of this class serve as prototypes for rows in queries. */
class TestsLog(_tableTag: Tag) extends Table[TestsLogRow](_tableTag, "tests_log") {
  def * = (testlogid, testid, startdatetime, finishdatetime, userpoint, fullpoint, userid, finished).
          <> (TestsLogRow.tupled, TestsLogRow.unapply)
  /** Maps whole row to an option. Useful for outer joins. */
  def ? = (Rep.Some(testlogid),
          Rep.Some(testid),
          Rep.Some(startdatetime),
          Rep.Some(finishdatetime),
          Rep.Some(userpoint),
          Rep.Some(fullpoint),
          Rep.Some(userid),
          Rep.Some(finished)).shaped.
    <>({r=>import r._; _1.map(_=>
      TestsLogRow.tupled((_1.get, _2.get, _3.get, _4.get, _5.get, _6.get, _7.get, _8.get)))},
      (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

  /** GetResult implicit for fetching TestsLogRow objects using plain SQL queries */
  implicit def GetResultTestsLogRow(implicit e0: GR[Long], e1: GR[LocalDateTime], e2: GR[Int]): GR[TestsLogRow] = GR{
    prs => import prs._
      TestsLogRow.tupled((<<[Long], <<[Long], <<[LocalDateTime], <<[LocalDateTime], <<[Int], <<[Int], <<[Long], <<[Short]))
  }

  /** Database column testlogid SqlType(bigserial), AutoInc, PrimaryKey */
  val testlogid: Rep[Long] = column[Long]("testlogid", O.AutoInc, O.PrimaryKey)
  /** Database column testid SqlType(int8) */
  val testid: Rep[Long] = column[Long]("testid")
  /** Database column startdatetime SqlType(timestamp) */
  val startdatetime: Rep[LocalDateTime] = column[LocalDateTime]("startdatetime")
  /** Database column finishdatetime SqlType(timestamp) */
  val finishdatetime: Rep[LocalDateTime] = column[LocalDateTime]("finishdatetime")
  /** Database column userpoint SqlType(int4), Default(0) */
  val userpoint: Rep[Int] = column[Int]("userpoint", O.Default(0))
  /** Database column fullpoint SqlType(int4), Default(0) */
  val fullpoint: Rep[Int] = column[Int]("fullpoint", O.Default(0))
  /** Database column userid SqlType(int8) */
  val userid: Rep[Long] = column[Long]("userid")
  val finished: Rep[Short] = column[Short]("finished", O.Default(0))
}

object TestsLog {
  /** Collection-like TableQuery object for table TestsLog */
  lazy val TestsLog = new TableQuery(tag => new TestsLog(tag))
  def get = TestsLog
}
