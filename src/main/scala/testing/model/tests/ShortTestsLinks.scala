package testing.model.tests

import slick.lifted.Tag
import slick.jdbc.{GetResult => GR}
import db.PostgresDriverPgExt.api._
/**
  * Created by luger on 16.05.16.
  */
/** Entity class storing rows of table ShortTestsLinks
  *  @param testid Database column testid SqlType(int8)
  *  @param link Database column link SqlType(varchar), PrimaryKey, Length(200,true) */
case class ShortTestsLinksRow(testid: Long, link: String)

/** Table description of table short_tests_links. Objects of this class serve as prototypes for rows in queries. */
class ShortTestsLinks(_tableTag: Tag) extends Table[ShortTestsLinksRow](_tableTag, "short_tests_links") {
  def * = (testid, link) <> (ShortTestsLinksRow.tupled, ShortTestsLinksRow.unapply)
  /** Maps whole row to an option. Useful for outer joins. */
  def ? = (Rep.Some(testid), Rep.Some(link)).shaped.
    <>({r=>import r._; _1.map(_=> ShortTestsLinksRow.tupled((_1.get, _2.get)))},
      (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

  /** GetResult implicit for fetching ShortTestsLinksRow objects using plain SQL queries */
  implicit def GetResultShortTestsLinksRow(implicit e0: GR[Long], e1: GR[String]): GR[ShortTestsLinksRow] = GR{
    prs => import prs._
      ShortTestsLinksRow.tupled((<<[Long], <<[String]))
  }

  /** Database column testid SqlType(int8) */
  val testid: Rep[Long] = column[Long]("testid")
  /** Database column link SqlType(varchar), PrimaryKey, Length(200,true) */
  val link: Rep[String] = column[String]("link", O.PrimaryKey, O.Length(200,varying=true))

  /** Foreign key referencing Tests (database name short_tests_links_testid_fkey) */
  lazy val testsFk = foreignKey("short_tests_links_testid_fkey", testid, Tests.get)(
    r => r.testid, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
}

object ShortTestsLinks {
  /** Collection-like TableQuery object for table ShortTestsLinks */
  lazy val ShortTestsLinks = new TableQuery(tag => new ShortTestsLinks(tag))

  def get = ShortTestsLinks
}
