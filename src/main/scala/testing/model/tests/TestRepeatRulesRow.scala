package testing.model.tests

import slick.lifted.Tag
import slick.jdbc.{GetResult => GR}
import db.PostgresDriverPgExt.api._
/**
  * Created by luger on 16.05.16.
  */
/** Entity class storing rows of table TestRepeatRules
  *  @param testid Database column testid SqlType(int8)
  *  @param memberid Database column memberid SqlType(int2)
  *  @param subjectid Database column subjectid SqlType(int8)
  *  @param period Database column period SqlType(int8), Default(0) */
case class TestRepeatRulesRow(testid: Long,
                              memberid: Short,
                              subjectid: Long,
                              period: Long = 0L)

/** Table description of table test_repeat_rules. Objects of this class serve as prototypes for rows in queries. */
class TestRepeatRules(_tableTag: Tag) extends Table[TestRepeatRulesRow](_tableTag, "test_repeat_rules") {
  def * = (testid, memberid, subjectid, period) <> (TestRepeatRulesRow.tupled, TestRepeatRulesRow.unapply)
  /** Maps whole row to an option. Useful for outer joins. */
  def ? = (Rep.Some(testid), Rep.Some(memberid), Rep.Some(subjectid), Rep.Some(period)).shaped.
    <>({r=>import r._; _1.map(_=> TestRepeatRulesRow.tupled((_1.get, _2.get, _3.get, _4.get)))},
      (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))
  /** GetResult implicit for fetching TestRepeatRulesRow objects using plain SQL queries */
  implicit def GetResultTestRepeatRulesRow(implicit e0: GR[Long], e1: GR[Short]): GR[TestRepeatRulesRow] = GR{
    prs => import prs._
      TestRepeatRulesRow.tupled((<<[Long], <<[Short], <<[Long], <<[Long]))
  }

  /** Database column testid SqlType(int8) */
  val testid: Rep[Long] = column[Long]("testid")
  /** Database column memberid SqlType(int2) */
  val memberid: Rep[Short] = column[Short]("memberid")
  /** Database column subjectid SqlType(int8) */
  val subjectid: Rep[Long] = column[Long]("subjectid")
  /** Database column period SqlType(int8), Default(0) */
  val period: Rep[Long] = column[Long]("period", O.Default(0L))

  /** Foreign key referencing Tests (database name test_repeat_rules_testid_fkey) */
  lazy val testsFk = foreignKey("test_repeat_rules_testid_fkey", testid, Tests.get)(
    r => r.testid, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
}

object TestRepeatRules {
  /** Collection-like TableQuery object for table TestRepeatRules */
  lazy val TestRepeatRules = new TableQuery(tag => new TestRepeatRules(tag))

  def get = TestRepeatRules
}
