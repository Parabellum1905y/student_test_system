package testing.model.users

import slick.lifted.Tag
import slick.jdbc.{GetResult => GR}
import slick.model.ForeignKeyAction
import db.PostgresDriverPgExt.api._
import java.time.LocalDateTime

/** Entity class storing rows of table MemberCategory
 *  @param catId Database column cat_id SqlType(int2), PrimaryKey
 *  @param catName Database column cat_name SqlType(varchar), Length(100,true)
 *  @param enabled Database column enabled SqlType(int2), Default(1) */
case class MemberCategoryRow(catId: Short, 
                             catName: String, 
                             enabled: Short = 1)

/** Table description of table member_category. Objects of this class serve as prototypes for rows in queries. */
class MemberCategory(_tableTag: Tag) extends Table[MemberCategoryRow](_tableTag, "member_category") {
  def * = (catId, catName, enabled) <> (MemberCategoryRow.tupled, MemberCategoryRow.unapply)
  /** Maps whole row to an option. Useful for outer joins. */
  def ? = (Rep.Some(catId), Rep.Some(catName), Rep.Some(enabled)).shaped.<>({r=>import r._; _1.map(_=> MemberCategoryRow.tupled((_1.get, _2.get, _3.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))
  /** GetResult implicit for fetching MemberCategoryRow objects using plain SQL queries */
  implicit def GetResultMemberCategoryRow(implicit e0: GR[Short], e1: GR[String]): GR[MemberCategoryRow] = GR{
    prs => import prs._
    MemberCategoryRow.tupled((<<[Short], <<[String], <<[Short]))
  }

  /** Database column cat_id SqlType(int2), PrimaryKey */
  val catId: Rep[Short] = column[Short]("cat_id", O.PrimaryKey)
  /** Database column cat_name SqlType(varchar), Length(100,true) */
  val catName: Rep[String] = column[String]("cat_name", O.Length(100,varying=true))
  /** Database column enabled SqlType(int2), Default(1) */
  val enabled: Rep[Short] = column[Short]("enabled", O.Default(1))
}

object MemberCategory{
  /** Collection-like TableQuery object for table MemberCategory */
  lazy val MemberCategory = new TableQuery(tag => new MemberCategory(tag))
  def get = MemberCategory
}


/** Entity class storing rows of table GroupRel
 *  @param grouprelid Database column grouprelid SqlType(bigserial), AutoInc, PrimaryKey
 *  @param groupid Database column groupid SqlType(int8)
 *  @param typeid Database column typeid SqlType(int2), Default(1)
 *  @param objectid Database column objectid SqlType(int8)
 *  @param enabled Database column enabled SqlType(int2), Default(1) */
case class GroupRelRow(grouprelid: Long, 
                       groupid: Long, 
                       typeid: Short = 1, 
                       objectid: Long, 
                       enabled: Short = 1,
                       isOwner: Short = 0)

/** Table description of table group_rel. Objects of this class serve as prototypes for rows in queries. */
class GroupRel(_tableTag: Tag) extends Table[GroupRelRow](_tableTag, "group_rel") {
  def * = (grouprelid, groupid, typeid, objectid, enabled, isowner) <> (GroupRelRow.tupled, GroupRelRow.unapply)
  /** Maps whole row to an option. Useful for outer joins. */
  def ? = (Rep.Some(grouprelid), Rep.Some(groupid), Rep.Some(typeid),
          Rep.Some(objectid), Rep.Some(enabled), Rep.Some(isowner)).shaped.
    <>({r=>import r._; _1.map(_=> GroupRelRow.tupled((_1.get, _2.get, _3.get, _4.get, _5.get, _6.get)))},
      (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))
  /** GetResult implicit for fetching GroupRelRow objects using plain SQL queries */
  implicit def GetResultGroupRelRow(implicit e0: GR[Long], e1: GR[Short]): GR[GroupRelRow] = GR{
    prs => import prs._
    GroupRelRow.tupled((<<[Long], <<[Long], <<[Short], <<[Long], <<[Short], <<[Short]))
  }

  /** Database column grouprelid SqlType(bigserial), AutoInc, PrimaryKey */
  val grouprelid: Rep[Long] = column[Long]("grouprelid", O.AutoInc, O.PrimaryKey)
  /** Database column groupid SqlType(int8) */
  val groupid: Rep[Long] = column[Long]("groupid")
  /** Database column typeid SqlType(int2), Default(1) */
  val typeid: Rep[Short] = column[Short]("typeid", O.Default(1))
  /** Database column objectid SqlType(int8) */
  val objectid: Rep[Long] = column[Long]("objectid")
  /** Database column enabled SqlType(int2), Default(1) */
  val enabled: Rep[Short] = column[Short]("enabled", O.Default(1))
  /** Database column isowner SqlType(int2), Default(1) */
  val isowner: Rep[Short] = column[Short]("isowner", O.Default(1))
  /** Foreign key referencing MemberCategory (database name group_rel_typeid_fkey) */
  lazy val memberCategoryFk = foreignKey("group_rel_typeid_fkey", typeid, MemberCategory.MemberCategory)(
    r => r.catId, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
  /** Foreign key referencing UserGroups (database name group_rel_groupid_fkey) */
  lazy val userGroupsFk = foreignKey("group_rel_groupid_fkey", groupid, UserGroups.UserGroups)(
    r => r.groupid, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
}

object GroupRel{
  /** Collection-like TableQuery object for table GroupRel */
  lazy val GroupRel = new TableQuery(tag => new GroupRel(tag))
  def get = GroupRel
}

  /** Entity class storing rows of table UserGroups
 *  @param groupid Database column groupid SqlType(bigserial), AutoInc, PrimaryKey
 *  @param groupname Database column groupname SqlType(varchar), Length(100,true), Default(None)
 *  @param groupdescr Database column groupdescr SqlType(varchar), Length(100,true), Default(None)
 *  @param grouputfname Database column grouputfname SqlType(varchar), Length(100,true), Default(None)
 *  @param enabled Database column enabled SqlType(int2), Default(1)
 *  @param hidden Database column hidden SqlType(int2), Default(0) */
case class UserGroupsRow(groupid: Long, 
                         groupname: Option[String] = None, 
                         groupdescr: Option[String] = None, 
                         grouputfname: Option[String] = None, 
                         enabled: Short = 1, 
                         hidden: Short = 0)
                         
/** Table description of table user_groups. Objects of this class serve as prototypes for rows in queries. */
class UserGroups(_tableTag: Tag) extends Table[UserGroupsRow](_tableTag, "user_groups") {
  def * = (groupid, groupname, groupdescr, grouputfname, enabled, hidden) <> (UserGroupsRow.tupled, UserGroupsRow.unapply)
  /** Maps whole row to an option. Useful for outer joins. */
  def ? = (Rep.Some(groupid), groupname, groupdescr, grouputfname, Rep.Some(enabled), Rep.Some(hidden)).shaped.<>({r=>import r._; _1.map(_=> UserGroupsRow.tupled((_1.get, _2, _3, _4, _5.get, _6.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))
  /** GetResult implicit for fetching UserGroupsRow objects using plain SQL queries */
  implicit def GetResultUserGroupsRow(implicit e0: GR[Long], e1: GR[Option[String]], e2: GR[Short]): GR[UserGroupsRow] = GR{
    prs => import prs._
    UserGroupsRow.tupled((<<[Long], <<?[String], <<?[String], <<?[String], <<[Short], <<[Short]))
  }

  /** Database column groupid SqlType(bigserial), AutoInc, PrimaryKey */
  val groupid: Rep[Long] = column[Long]("groupid", O.AutoInc, O.PrimaryKey)
  /** Database column groupname SqlType(varchar), Length(100,true), Default(None) */
  val groupname: Rep[Option[String]] = column[Option[String]]("groupname", O.Length(100,varying=true), O.Default(None))
  /** Database column groupdescr SqlType(varchar), Length(100,true), Default(None) */
  val groupdescr: Rep[Option[String]] = column[Option[String]]("groupdescr", O.Length(100,varying=true), O.Default(None))
  /** Database column grouputfname SqlType(varchar), Length(100,true), Default(None) */
  val grouputfname: Rep[Option[String]] = column[Option[String]]("grouputfname", O.Length(100,varying=true), O.Default(None))
  /** Database column enabled SqlType(int2), Default(1) */
  val enabled: Rep[Short] = column[Short]("enabled", O.Default(1))
  /** Database column hidden SqlType(int2), Default(0) */
  val hidden: Rep[Short] = column[Short]("hidden", O.Default(0))
}

object UserGroups{
  /** Collection-like TableQuery object for table UserGroups */
  lazy val UserGroups = new TableQuery(tag => new UserGroups(tag))
  def get = UserGroups
}
