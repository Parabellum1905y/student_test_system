package testing.model.tags
import slick.lifted.Tag
import slick.jdbc.{GetResult => GR}
import db.PostgresDriverPgExt.api._

 /** Entity class storing rows of table Tags
   *  @param tagid Database column tagid SqlType(bigserial), AutoInc, PrimaryKey
   *  @param tagtext Database column tagtext SqlType(varchar), Length(200,true) */
  case class TagsRow(tagid: Long, tagtext: String)

  /** Table description of table tags. Objects of this class serve as prototypes for rows in queries. */
  class Tags(_tableTag: Tag) extends Table[TagsRow](_tableTag, "tags") {
    def * = (tagid, tagtext) <> (TagsRow.tupled, TagsRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(tagid), Rep.Some(tagtext)).shaped.<>({r=>import r._; _1.map(_=> TagsRow.tupled((_1.get, _2.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))
    /** GetResult implicit for fetching TagsRow objects using plain SQL queries */
    implicit def GetResultTagsRow(implicit e0: GR[Long], e1: GR[String]): GR[TagsRow] = GR{
      prs => import prs._
      TagsRow.tupled((<<[Long], <<[String]))
    }

    /** Database column tagid SqlType(bigserial), AutoInc, PrimaryKey */
    val tagid: Rep[Long] = column[Long]("tagid", O.AutoInc, O.PrimaryKey)
    /** Database column tagtext SqlType(varchar), Length(200,true) */
    val tagtext: Rep[String] = column[String]("tagtext", O.Length(200,varying=true))

    /** Index over (tagtext) (database name tags_adid_date_index) */
    val index1 = index("tags_adid_date_index", tagtext)
    /** Uniqueness Index over (tagtext) (database name tags_tagtext_key) */
    val index2 = index("tags_tagtext_key", tagtext, unique=true)

}

object Tags{
    /** Collection-like TableQuery object for table Tags */
    lazy val Tags = new TableQuery(tag => new Tags(tag))
    def get = Tags
}
