package testing.controllers

import java.time.LocalDateTime

import quickstart.action.BeforeRespond
import testing.dao.tests.{ElasticTestDao, TestsDao}
import testing.model.tests.TestsRow
import xitrum.annotation.{GET, POST}

import scala.util.{Failure, Success}

/**
  * Created by luger on 02.06.16.
  */
trait TestsController extends BeforeRespond with xitrum.Log{
  lazy val testDao = TestsDao
  lazy val currentUserId = sessiono[Long]("userId") getOrElse (-1L)
}

trait TestManipulation extends TestsController {
  lazy val langId = sessiono[Long]("langId").getOrElse(-1L)
  lazy val creatorId = currentUserId
  lazy val testName = paramo[String]("testname").getOrElse("")
  lazy val testDescription = paramo[String]("testdescription").getOrElse("")
  lazy val randomTasks = paramo[Short]("randomtasks").getOrElse(0.toShort)
  lazy val randomAnswers = paramo[Short]("randomanswers").getOrElse(0.toShort)
  lazy val posibilityToJumpTask = paramo[Short]("posibilitytojumptask").getOrElse(0.toShort)
  lazy val jumpTaskFine = paramo[Short]("jumptaskfine").getOrElse(0.toShort)
  lazy val testExecTime = paramo[Int]("testexectime").getOrElse(0)
  lazy val maxTimePerTask = paramo[Int]("maxtimepertask").getOrElse(0)
  lazy val useTaskedTime = paramo[Short]("usetaskedtime").getOrElse(0.toShort)
  lazy val allowViewDetailedResults = paramo[Short]("allowviewdetailedresults").getOrElse(0.toShort)
  lazy val allowHideResult = paramo[Short]("allowhideresult").getOrElse(0.toShort)

  beforeFilter {
    testName match {
      case "" =>
        respondJson(Map ("status"->"error", "data"->"INVALID_FORM_DATA"))
        false
      case _ => true
    }
  }

  def newId:Long

  object Test extends TestsRow(newId,
    langId,
    creatorId, testName,
    Option(testDescription), randomtasks = Option(randomTasks),
    randomanswers = Option(randomAnswers),
    posibilitytojumptask = Option(posibilityToJumpTask),
    jumptaskfine = Option(jumpTaskFine), testexectime = Option(testExecTime),
    maxtimepertask = Option(maxTimePerTask), usetaskedtime = Option(useTaskedTime),
    allowviewdetailedresults = Option(allowViewDetailedResults),
    allowhideresult = allowHideResult)
}

@POST ("tests")
class NewTest extends TestManipulation{
  def newId = -1L

  def execute =
    testDao createTest Test onComplete{
      case Success(row) =>
        respondJson(Map("status"->"success", "data"->row))
      case Failure(err) =>
        log error s"$err"
        respondJson("status"->"error", "data"->"TEST_DOESNT_SAVED")
    }
}

@GET("t-dropdown")
class TestsAutoComplete extends TestsController{
  val elastic = ElasticTestDao.apply

  def execute =
    paramo[String]("pattern") match {
      case _@None        => respondJson(Map("status"->"error", "data"->"NAME_NON_EMPTY"))
      case Some(pattern) if !pattern.isEmpty =>
        elastic.searchMatchTest(pattern) onComplete {
          case Success(x) =>
            val suggests = x.hits.map{hit =>
              log info s"hits:${hit.sourceAsMap.get("suggest")}"
              log info s"hits another :${hit.fields}"
              Map("id"->hit.getId, "test" ->
                Map(
                  "name"->hit.sourceAsMap.getOrElse("name", ""),
                  "description"->hit.sourceAsMap.getOrElse("description", "")
                ))
            }
            respondJson(Map ("status"->"success", "data"->suggests))
          case Failure(err) =>
            log error s"$err"
            respondJson(Map ("status"->"success", "data"->""))
        }
    }
}

@GET ("my-test/:testid<[0-9]+>")
class GetMyTest extends TestsController {
  lazy val testId = paramo[Long]("testid").filter(_ > 0).getOrElse(-1L)

  beforeFilter {
    testId match {
      case -1L =>
        respondJson(Map ("status"->"error", "data"->"INVALID_FORM_DATA"))
        false
      case _ => true
    }
  }

  def execute =
    testDao getMine (testId, currentUserId) onComplete {
      case Success(test) =>
        respondJson(Map ("status"->"success", "data"->test))
      case Failure(err) =>
        log error s"$err"
        respondJson(Map ("status"->"error", "data"->None))
    }
}

@GET ("test-preview/:testid<[0-9]+>")
class GetTest extends TestsController {
  lazy val testId = paramo[Long]("testid").filter(_ > 0).getOrElse(-1L)

  beforeFilter {
    testId match {
      case -1L =>
        respondJson(Map ("status"->"error", "data"->"INVALID_FORM_DATA"))
        false
      case _ => true
    }
  }

  def execute =
    testDao get testId onComplete {
      case Success(None) =>
        respondJson(Map ("status"->"success", "data"->"EMPTY"))
      case Success(test) =>
        respondJson(Map ("status"->"success", "data"->test))
      case Failure(err) =>
        log error s"$err"
        respondJson(Map ("status"->"error", "data"->None))
    }
}

@POST ("e-tests")
class EditTest extends TestManipulation{
  lazy val testId = paramo[Long]("testid").filter(_ > 0).getOrElse(-1L)

  beforeFilter {
    testId match {
      case -1L =>
        respondJson(Map ("status"->"error", "data"->"INVALID_FORM_DATA"))
        false
      case _ => true
    }
  }

  def newId = testId

  def execute =
    testDao updateTest Test onComplete{
      case Success(row) =>
        respondJson(Map("status"->"success", "data"->row))
      case Failure(err) =>
        log error s"$err"
        respondJson("status"->"error", "data"->"TEST_DOESNT_SAVED")
    }
}

@POST ("del-tests")
class DeleteTest extends TestsController{
  lazy val testId = paramo[Long]("testid").filter(_ > 0).getOrElse(-1L)

  beforeFilter {
    testId match {
      case -1L =>
        respondJson(Map ("status"->"error", "data"->"INVALID_FORM_DATA"))
        false
      case _ => true
    }
  }

  def newId = testId

  def execute =
    testDao deleteTest (testId, currentUserId) onComplete{
      case Success(count) =>
        respondJson(Map("status"->"success", "data"->count))
      case Failure(err) =>
        log error s"$err"
        respondJson("status"->"error", "data"->"TEST_DOESNT_DELETED")
    }
}

@GET ("test-accessed/:testid<[0-9]+>")
class LoadTestAccessForUser extends TestsController{
  lazy val testId = paramo[Long]("testid").filter(_ > 0).getOrElse(-1L)

  beforeFilter {
    testId match {
      case -1L =>
        respondJson(Map ("status"->"error", "data"->"INVALID_FORM_DATA"))
        false
      case _ => true
    }
  }

  def newId = testId

  def execute =
    testDao getAccessedTestForUser (currentUserId, testId) onComplete{
      case Success(None) =>
        respondJson(Map("status"->"success", "data"->"EMPTY"))
      case Success(testAccessed) =>
            respondJson(Map("status"->"success", "data"->testAccessed.get))
      case Failure(err) =>
        log error s"$err"
        respondJson(Map("status"->"error", "data"->"EMPTY"))
    }
}

@GET ("test-accesses/:testid<[0-9]+>")
class LoadTestAccesses extends TestsController{
  lazy val testId = paramo[Long]("testid").filter(_ > 0).getOrElse(-1L)

  beforeFilter {
    testId match {
      case -1L =>
        respondJson(Map ("status"->"error", "data"->"INVALID_FORM_DATA"))
        false
      case _ => true
    }
  }

  def newId = testId

  /*override def respondJson (ref: AnyRef): ChannelFuture = {
    val json = SeriDeseri.toJson(ref)
    respondText(json, "application/json")
  }*/
  def execute =
    testDao getAccessesFor testId onComplete{
      case Success(testAccesses) =>
        testAccesses match {
          case (Nil, Nil) => respondJson(Map("status"->"success", "data"->"EMPTY_ACCESS"))
          case _ =>
            val grAcc = testAccesses._2.map {gr =>
              Map("testacc"->gr._1, "group"->gr._2)
            }
            val usAcc = testAccesses._1.map {gr =>
              Map("testacc"->gr._1, "user"->gr._2.copy(userPass = None, registerDate = LocalDateTime.now))
            }
            respondJson(Map("status"->"success", "data"->
              Map("gr-acc"->grAcc,
                "us-acc"->usAcc)
            ))
        }
      case Failure(err) =>
        log error s"$err"
        respondJson("status"->"error", "data"->"TEST_DOESNT_DELETED")
    }
}

trait NewTestAccesses extends TestsController {
  lazy val testId = paramo[Long]("testid").filter(_ > 0).getOrElse(-1L)
  lazy val userId = paramo[Long]("userid").filter(_ > 0).getOrElse(-1L)
  lazy val groupId = paramo[Long]("groupid").filter(_ > 0).getOrElse(-1L)

  beforeFilter {
    (testId, userId, groupId) match {
      case (-1L, _, _) | (_, -1L, -1L) =>
        respondJson(Map("status" -> "error", "data" -> "INVALID_FORM_DATA"))
        false
      case _ => true
    }
  }
}

@POST ("test-gr-accesses")
class NewTestGroupAccesses extends NewTestAccesses{
  beforeFilter {
    groupId match {
      case -1L =>
        respondJson(Map ("status"->"error", "data"->"INVALID_FORM_DATA"))
        false
      case _ => true
    }
  }

 /* override def respondJson (ref: AnyRef): ChannelFuture = {
    val json = SeriDeseri.toJson(ref)
    respondText(json, "application/json")
  }*/
  def execute =
    testDao setObjectAccessForTest (testId, groupId, 2, currentUserId, 0, 0, 1) onComplete{
      case Success(testAccesses) =>
        testAccesses match {
          case None => respondJson(Map("status"->"success", "data"->"EMPTY_ACCESS"))
          case testAccess =>
            respondJson(Map("status"->"success", "data"->testAccess))
        }
      case Failure(err) =>
        log error s"$err"
        respondJson("status"->"error", "data"->"TEST_DOESNT_DELETED")
    }
}


@POST ("test-us-accesses")
class NewTestUserAccesses extends NewTestAccesses{
  beforeFilter {
    userId match {
      case -1L =>
        respondJson(Map ("status"->"error", "data"->"INVALID_FORM_DATA"))
        false
      case _ => true
    }
  }

  /* override def respondJson (ref: AnyRef): ChannelFuture = {
     val json = SeriDeseri.toJson(ref)
     respondText(json, "application/json")
   }*/
  def execute =
    testDao setObjectAccessForTest (testId, userId, 1, currentUserId, 0, 0, 1) onComplete{
      case Success(testAccesses) =>
        testAccesses match {
          case None => respondJson(Map("status"->"success", "data"->"EMPTY_ACCESS"))
          case testAccess =>
            respondJson(Map("status"->"success", "data"->testAccess))
        }
      case Failure(err) =>
        log error s"$err"
        respondJson("status"->"error", "data"->"TEST_DOESNT_DELETED")
    }
}

@POST ("del-test-accesses")
class DelTestAccesses extends NewTestAccesses{
  lazy val memberType = paramo[Int]("membertype").filter(_ > 0).getOrElse(-1)
  beforeFilter {
    (groupId, memberType, userId) match {
      case (-1L, -1, _)|(_, -1, -1L) =>
        respondJson(Map ("status"->"error", "data"->"INVALID_FORM_DATA"))
        false
      case _ => true
    }
  }

  def memberId = memberType match {
    case 1 => userId
    case _ => groupId
  }
  /* override def respondJson (ref: AnyRef): ChannelFuture = {
     val json = SeriDeseri.toJson(ref)
     respondText(json, "application/json")
   }*/
  def execute =
    testDao delUserAccessForTest (testId, memberId, memberType, currentUserId) onComplete{
      case Success(testAccesses) =>
        testAccesses match {
          case 0 => respondJson(Map("status"->"success", "data"->"EMPTY_ACCESS"))
          case testAccess =>
            respondJson(Map("status"->"success", "data"->testAccess))
        }
      case Failure(err) =>
        log error s"$err"
        respondJson("status"->"error", "data"->"TEST_DOESNT_DELETED")
    }
}
