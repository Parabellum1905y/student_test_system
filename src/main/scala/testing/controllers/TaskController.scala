package testing.controllers

import quickstart.action.BeforeRespond
import testing.dao.tests.{TasksDao, TestsDao}
import testing.model.tests.TasksRow
import xitrum.annotation.{GET, POST}
import testing.dao.tests.TaskHelper._

import scala.util.{Failure, Success}

/**
  * Created by luger on 09.06.16.
  */
trait TaskController extends BeforeRespond with xitrum.Log{
  lazy val testDao = TestsDao
  lazy implicit val taskDao = TasksDao.apply
  lazy val currentUserId = sessiono[Long]("userId").filter(_ > 0).getOrElse (-1L)
  lazy val langId = sessiono[Long]("langId").filter(_ > 0).getOrElse(-1L)
  lazy val creatorId = currentUserId
  lazy val testId = paramo[Long]("testid").filter(_ > 0).getOrElse(-1L)
}

trait TaskManipulation extends TaskController {
  def taskId:Long

  beforeFilter {
    testId match {
      case -1L =>
        respondJson(Map ("status"->"error", "data"->"INVALID_FORM_DATA"))
        false
      case _ => true
    }
  }
}

@GET("tasks/:testid<[0-9]+>")
class TasksGet extends TaskManipulation {
  def taskId = -1L

  def execute =
    taskDao getForTest (testId, currentUserId) onComplete {
      case Success(taskList) =>
        respondJson(Map("status"->"success", "data"->taskList.sortBy(_.taskid)))
      case Failure(err) =>
        log error s"$err"
    }
}

@GET("task/:testid<[0-9]+>/:taskid<[0-9]+>")
class TaskGet extends TaskManipulation {
  lazy val taskEditId = paramo[Long]("taskid").filter(_ > 0).getOrElse(-1L)
  def taskId = taskEditId

  def execute =
    taskDao getForTest (taskId, testId, currentUserId) onComplete {
/*
      case Success(None) =>
        respondJson(Map("status"->"success", "data"->"EMPTY_TASK"))
*/
      case Success(taskList) =>
        respondJson(Map("status"->"success", "data"->taskList.head))
      case Failure(err) =>
        log error s"$err"
    }
}

trait TaskPut extends TaskManipulation {
  lazy val theme = paramo[String]("theme")
  lazy val title = paramo[String]("title")
  lazy val task = paramo[String]("task").getOrElse("")
  lazy val jumptaskfine = paramo[Short]("jumptaskfine").getOrElse(0.toShort)
  lazy val taskpoint = paramo[Short]("taskpoint").filter(_ > 0).getOrElse(0.toShort)
  lazy val taskfine = paramo[Short]("taskfine").getOrElse(0.toShort)
  lazy val randomanswers = paramo[Short]("randomanswers").getOrElse(0.toShort)
  lazy val multianswer = paramo[Short]("multianswer").getOrElse(0.toShort)
  lazy val testexectime = paramo[Int]("testexectime").getOrElse(0)

  lazy implicit val ownerId = currentUserId

}

@POST("task")
class NewTask extends TaskPut {
  def taskId = -1L

  beforeFilter {
    (task, taskpoint) match {
      case ("", _)| (_, 0)=>
        respondJson(Map ("status"->"error", "data"->"INVALID_FORM_DATA"))
        false
      case _ => true
    }
  }

  def newTask = TasksRow(taskId, testId, theme, title, task, Option(jumptaskfine),
    taskpoint, taskfine, randomanswers, multianswer, Option(testexectime))
  def execute =
    newTask.create onComplete{
      case Success(None) =>
        respondJson(Map("status"->"error", "data"->"TASK_DOESNT_CREATE"))
      case Success(x) =>
        respondJson(Map("status"->"success", "data"->x.get))
      case Failure(err)=>
        log error s"$err"
        respondJson(Map("status"->"error", "data"->"TASK_DOESNT_CREATE"))
    }
}

@POST("etask")
class EditTask extends TaskPut {
  lazy val taskid = paramo[Long]("taskid").filter(_ > 0).getOrElse(-1L)
  def taskId = taskid

  beforeFilter {
    /*val taskEditId = paramo[Long]("taskid").filter(_ > 0).getOrElse(-1L)
    handlerEnv.bodyTextParams.get("taskid").get.headOption*/
    (task, taskpoint, taskId) match {
      case ("", _, _)| (_, 0, _)| (_, _, -1L)=>
        respondJson(Map ("status"->"error", "data"->"INVALID_FORM_DATA"))
        false
      case _ => true
    }
  }

  def newTask = TasksRow(taskId, testId, theme, title, task, Option(jumptaskfine),
    taskpoint, taskfine, randomanswers, multianswer, Option(testexectime))
  def execute =
    newTask.update onComplete{
      case Success(x) =>
        respondJson(Map("status"->"success", "data"->x))
      case Failure(err)=>
        log error s"$err"
        respondJson(Map("status"->"error", "data"->"TASK_DOESNT_UPDATE"))
    }
}

@POST("dtask")
class DeleteTask extends TaskPut {
  lazy val taskid = paramo[Long]("taskid").filter(_ > 0).getOrElse(-1L)
  def taskId = taskid

  beforeFilter {
    /*val taskEditId = paramo[Long]("taskid").filter(_ > 0).getOrElse(-1L)
    handlerEnv.bodyTextParams.get("taskid").get.headOption*/
    (testId, taskId) match {
      case (-1L, _)| (_, -1L)=>
        respondJson(Map ("status"->"error", "data"->"INVALID_FORM_DATA"))
        false
      case _ => true
    }
  }

  def execute =
    taskDao.delete(taskId, testId, ownerId) onComplete{
      case Success(x) =>
        respondJson(Map("status"->"success", "data"->Map("testid"->testId, "count"->x)))
      case Failure(err)=>
        log error s"$err"
        respondJson(Map("status"->"error", "data"->"TASK_DOESNT_UPDATE"))
    }
}

