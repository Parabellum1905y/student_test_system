package testing.controllers



import java.time.LocalDateTime

import akka.actor.{Actor, ActorRef, ActorSystem, Props, Terminated}
import glokka.Registry
import testing.model.tests.{TestsLogRow, TestsRow}
import xitrum.{Config, Log, SockJsAction, SockJsText, WebSocketAction, WebSocketBinary, WebSocketPing, WebSocketPong, WebSocketText}
import xitrum.annotation.{GET, SOCKJS, WEBSOCKET}

/**
  * Created by luger on 11.06.16.
  */

@SOCKJS("sockJsRunTest")
class SockJsChatActor extends SockJsAction with LookupOrCreateTesting {
  def execute() {
    runTest()
  }

  def onRunTest(runTest: ActorRef) = {
    case SockJsText(msg) =>
      //runTest ! RunTests.Join(msg)

    case ChatRoom.Msg(body) =>
      respondSockJsText(body)
  }
}

@WEBSOCKET("websocketRunTest")
class WebSocketChatActor extends WebSocketAction with LookupOrCreateChatRoom {
  def execute() {
    joinChatRoom()
  }

  def onJoinChatRoom(chatRoom: ActorRef) = {
    case WebSocketText(msg) =>
      chatRoom ! ChatRoom.Msg(msg)

    case ChatRoom.Msg(body) =>
      respondWebSocketText(body)

    case WebSocketBinary(bytes) =>
    case WebSocketPing =>
    case WebSocketPong =>
  }
}

//------------------------------------------------------------------------------
// See https://github.com/xitrum-framework/glokka

object ChatRoom {
  val MAX_MSGS   = 20
  val ROOM_NAME  = "chatRoom"
  val PROXY_NAME = "chatRoomProxy"

  // Subscribers:
  // * To join a chat room, send Join
  // * When there's a chat message, receive Msg
  case object Join
  case class  Msg(body: String)
  //case object Tick

  // Registry is used for looking up chat room actor by name.
  // For simplicity, this demo uses only one chat room (lobby chat room).
  // If you want many chat rooms, create more chat rooms with different names.
  val registry = Registry.start(Config.actorSystem, PROXY_NAME)
}

/** Subclasses should implement onJoinChatRoom and call joinChatRoom. */
trait LookupOrCreateChatRoom {
  this: Actor =>

  import ChatRoom._

  def onJoinChatRoom(chatRoom: ActorRef): Actor.Receive

  def joinChatRoom() {
    registry ! Registry.Register(ROOM_NAME, Props[ChatRoom])
    context.become(waitForRegisterResult)
  }

  private def waitForRegisterResult(): Actor.Receive = {
    case msg: Registry.FoundOrCreated =>
      val chatRoom = msg.ref
      chatRoom ! Join
      context.become(onJoinChatRoom(chatRoom))
  }
}

class ChatRoom extends Actor with Log {
  import ChatRoom._

  private var subscribers = Seq.empty[ActorRef]
  private var msgs        = Seq.empty[String]

  def receive = {
    case Join =>
      val subscriber = sender
      subscribers = subscribers :+ sender
      context.watch(subscriber)
      msgs foreach (subscriber ! Msg(_))
      log.debug("Joined chat room: " + subscriber)
    case m @ Msg(body) =>
      msgs = msgs :+ body
      if (msgs.length > MAX_MSGS) msgs = msgs.drop(1)
      subscribers foreach (_ ! m)

    case Terminated(subscriber) =>
      subscribers = subscribers.filterNot(_ == subscriber)
      log.debug("Left chat room: " + subscriber)
    case _ =>
      log info s"WTF!!!??${LocalDateTime.now}"
  }
}

case class JoinedTest (testsRow: TestsRow, testsLogRow: TestsLogRow)

object RunTests {
  val ROOM_NAME  = "testing"
  val PROXY_NAME = "testingProxy"

  // Subscribers:
  // * To create your test send Join
  // * When there's a chat message, receive Msg
  case class Join(joinedTest: JoinedTest)
  case class clientFinishTest(testsLogRowId:Long)
  case class sheduledFinishTest(testsLogRowId:Long)
  case class clientFinishTask(taskId:Long)
  case class sheduledFinishTask(taskId:Long)
  case class Tick(msg :String)

  // Registry is used for looking up chat room actor by name.
  // For simplicity, this demo uses only one chat room (lobby chat room).
  // If you want many chat rooms, create more chat rooms with different names.
  val registry = Registry.start(Config.actorSystem, PROXY_NAME)
}

trait LookupOrCreateTesting {
  this: Actor =>

  import RunTests._

  def onRunTest(test: ActorRef): Actor.Receive

  def runTest() {
    registry ! Registry.Register(ROOM_NAME, Props[RunTests])
    context.become(waitForRegisterResult)
  }

  private def waitForRegisterResult(): Actor.Receive = {
    case msg: Registry.FoundOrCreated =>
      val tests = msg.ref
      tests ! Join
      context.become(onRunTest(tests))
  }
}

class RunTests extends Actor with Log {
  import RunTests._

  private var subscribers = Seq.empty[(ActorRef, JoinedTest)]
  //private var testsList   = Seq.empty[JoinedTest]

  def receive = {
    case join @ Join(body) =>
      val subscriber = sender
      subscribers = subscribers :+ (sender, body)
      context.watch(subscriber)
      subscriber ! body.testsLogRow.testlogid
        //testsList foreach (subscriber ! JoinedTest(_))
      log.debug("Joined chat room: " + subscriber)
    case tick @ Tick(body) =>
      log info s"tic tac"
      /*subscribers.filter { testing =>
        val subscriber = testing._1
        val test = testing._2.testsRow
        val testLog = testing._2.testsLogRow

      }*/
/*    case m @ Msg(body) =>
      msgs = msgs :+ body
      if (msgs.length > MAX_MSGS) msgs = msgs.drop(1)
      subscribers foreach (_ ! m)*/

    case Terminated(subscriber) =>
      subscribers = subscribers.filterNot(_ == subscriber)
      log.debug("Left chat room: " + subscriber)
    case _ =>
      log info s"WTF!!!??${LocalDateTime.now}"
  }
}

