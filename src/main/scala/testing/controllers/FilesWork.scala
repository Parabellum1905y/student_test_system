package testing.controllers

import java.io.File

import files.FileConf
import io.netty.handler.codec.http.multipart.FileUpload
import quickstart.action.BeforeRespond
import testing.dao.files.FilesDao
import testing.model.files.FilesRow
import util.AcceptableMimeTypes
import xitrum.FutureAction
import xitrum.annotation.{GET, POST}

import scala.util.{Failure, Success}

/**
  * Created by luger on 10.03.16.
  */

trait FilesWork extends BeforeRespond with xitrum.Log{
  lazy val files = FilesDao
  lazy val path = FileConf.apply.rootPath

  def checkFileName (root:String, name:String, index:Int):String = index match {
    case 0 =>
      if (new File (root+"/"+name).exists) checkFileName(root, name, index+1)
      else name
    case _ =>
      if (new File (root+"/"+name+"-"+index).exists) checkFileName(root, name, index+1)
      else name+"-"+index
  }
}

@POST("newfile")
class NewFile extends FilesWork{
  override def execute = {
    val myFile = param[FileUpload]("file")
    val realName = myFile.getFilename
    val sessionUserId = sessiono[Long]("userId").get
    val contentType = myFile.getContentType
    log info s"${contentType}"
    /**
      * check mime-type
      */
    val acceptedType = AcceptableMimeTypes(contentType)
    /**
     * create or check root dir
     */
    val rootUserDir = new File(path+"/"+sessionUserId)
    if (!rootUserDir.exists)rootUserDir.mkdir
    val rootUserPath = rootUserDir.getAbsolutePath
    /**
      * check name in server FS
      */
    val name = checkFileName(rootUserPath, realName, 0)
    // TODO: create small and medium png for image type
    acceptedType match {
      case Some(atype) =>
        val fileRow = FilesRow (userId = sessionUserId, name = Some(name), realname = Some(sessionUserId+"_"+name),
                                size = Some(myFile.length), mediaformat = Some(atype.index))
        files create fileRow onComplete {
          case Success(0)  => respondJson(Map("status"->"error", "data"->"UNKNOWN_ERROR"))
          case Success(x)  =>
            myFile.renameTo(new File(rootUserPath+"/"+name))
            respondJson(Map("status"->"success", "data"->x))
          case Failure(ex) =>
            log error s"$ex"
            respond500Page()
        }
      case None => respondJson(Map("status"->"error", "data"->"MIME_TYPE_INVALID"))
    }
  }
}

@GET("file/:id<[0-9]+>")
class DownloadFile extends FilesWork{
  def execute = {
    val pid = paramo[Long]("id").getOrElse(-1L)
    pid match {
      case id if id<=0 =>respondJson(Map("status"->"error", "data"->"INVALID_FILE_ID"))
      case id =>
        files getFileById(id) onComplete {
          case Success (None) => respond404Page
          case Success (Some(x)) =>
            val filePath = path+"/"+x.userId+"/"+x.name.get
            log info s"$filePath"
            respondFile(filePath)
          case Failure(ex) =>
            log error s"$ex"
            respond500Page
        }
    }
  }
}

@GET("image/:id<[0-9]+>", "image/:id<[0-9]+>/:size<(small|medium|orig)>")
class DownloadImage extends FilesWork{
  def execute = {
    val pid = paramo[Long]("id").getOrElse(-1L)
    val size = paramo[String]("size").getOrElse("orig")
    pid match {
      case id if id<=0 =>respondJson(Map("status"->"error", "data"->"INVALID_FILE_ID"))
      case id =>
        files getFileById id onComplete {
          case Success (None) => respond404Page
          case Success (Some(x)) =>
            val filePath = path+"/"+x.userId+"/"+x.name.get

            if(AcceptableMimeTypes(x.mediaformat.get).get.name.startsWith("image")) size match {
              case "small"=>respondFile(filePath+"._small")
              case "medium"=>respondFile(filePath+"._medium")
              case _ =>respondFile(filePath)
            }
            else respondJson(Map("status"->"error", "data"->"INVALID_REQUEST_TYPE"))
          case Failure(ex) =>
            log error s"$ex"
            respond500Page
        }
    }
  }
}
/* -------------------------------------------------------
    val contentType: String = request headers() get Names.CONTENT_TYPE
    log info s"$contentType"
    if (contentType.startsWith("multipart/form-data")) {
      // Old browser
      val myFile = param[FileUpload]("file")
      val path = FileConf.apply.rootPath
      val fileBody = request content()
      log info s"$myFile"
      /*myFile.renameTo(new File(path+"/"+myFile.getFilename))
      Files.write(Paths get(myFile.getFilename), myFile.get(), StandardOpenOption.CREATE)*/
      /*val file = new File(path+"/"+myFile.getFilename)
      if (!file.exists())file.createNewFile
      val output = new FileOutputStream(file)
      val outputChannel:WritableByteChannel = Channels.newChannel(output)
      val nioBuffer = fileBody nioBuffer()
      outputChannel.write(nioBuffer)
      outputChannel.close()*/
      myFile.renameTo(new File(path+"/"+myFile.getFilename))
    } else if (contentType.startsWith("application/octet-stream")) {
      // New browser
      val fileName = param("file")
      val fileBody = request content()
      val path = FileConf.apply.rootPath
      log info s"$fileName"
      //Files.
      val file = new File(path+"/"+fileName)
      if (!file.exists())file.createNewFile
      val output = new FileOutputStream(file)
      val outputChannel:WritableByteChannel = Channels.newChannel(output)
      val nioBuffer = fileBody nioBuffer()
      outputChannel.write(nioBuffer)
      outputChannel.close()
    }
    respondJson(Map("status"->"success"))

 */