package testing.controllers

import quickstart.action.BeforeRespond
import testing.dao.usersDAO.{ElasticGroupDao, GroupsDao, UserDao}
import testing.model.users.GroupRelRow
import xitrum.annotation.{GET, POST}

import scala.util.{Failure, Success}

/**
  * Created by luger on 19.05.16.
  */
trait GroupsLoad extends BeforeRespond with xitrum.Log{
  lazy val userman = UserDao
  lazy val groups = GroupsDao

  def failIdAnswer (format:Option[String]) = format match {
    case Some("html")|Some("HTML") =>
      respond500Page()
    case _ =>
      respondJson(Map("status" -> "error", "data" -> "INVALID_ID"))
  }
}

@GET("gr-users/:id<[0-9]+>", "gr-users/:id<[0-9]+>/:format<(html)>")
class GroupsUsers extends GroupsLoad{
  def createRespond (groupId:Long, format:Option[String]) = {
    val currentUserId = sessiono[Long]("userId") getOrElse (-1L)
    groups.searchUsersInGroups(groupId) onComplete {
      case Success(useq) =>
        format match {
          case Some("html")|Some("HTML") =>
            respondHtml(
              newComponent[UsersLists]().render(groupId, useq, currentUserId))
          case _ =>
            respondJson(Map("status" -> "success",
                            "data" -> userman.safeUsersSeq (useq)))
        }
      case Failure(er)=>
        log error s"$er"
        failIdAnswer(format)
    }
  }

  def execute = {
    val id = paramo[Long]("id")
    val format = paramo[String]("format")
    id match {
      case _@None    => failIdAnswer(format)
      case Some(gid) => createRespond (gid, format)
    }
  }
}

trait GroupsUserCrud extends GroupsLoad {
  lazy val groupId = paramo[Long]("groupId").getOrElse(-1L)
  lazy val userId = paramo[Long]("userId").getOrElse(-1L)
  lazy val currentUserId = sessiono[Long]("userId") getOrElse (-1L)

  beforeFilter {
    (groupId, userId) match {
      case (-1L, -1L) |  (-1L, _) | (_, -1L) =>
        respondJson(Map ("status"->"error", "data"->"INVALID_FORM_DATA"))
        false
      case _ => true
    }
  }
}

@POST("gr-users")
class NewGroup extends GroupsLoad {
  lazy val groupName = paramo[String]("groupName").getOrElse("")
  lazy val groupDescription = paramo[String]("groupDescription").getOrElse("")
  lazy val currentUserId = sessiono[Long]("userId") getOrElse (-1L)

  beforeFilter {
    (groupName, groupDescription) match {
      case ("", "") |  ("", _) | (_, "") =>
        respondJson(Map ("status"->"error", "data"->"INVALID_FORM_DATA"))
        false
      case _ => true
    }
  }

  def execute =
    groups.createGroupWithOwner(currentUserId,
                                Option(groupName),
                                Option(groupDescription),
                                1.toShort,
                                0.toShort) onComplete {
      case Success(GroupRelRow(-1L, _, _, _, _, _)) =>
        respondJson(Map ("status"->"error", "data"->"GROUP_HAVENT_CREATED"))
      case Success (x) =>
        respondJson(Map ("status"->"success", "data"->Map("groupId"->x.groupid, "userId"->x.objectid)))
      case Failure(err) =>
        log error s"$err"
        respondJson(Map ("status"->"error", "data"->"GROUP_HAVENT_CREATED"))
    }
}

@POST("gr-del")
class DelGroup extends GroupsLoad {
  lazy val groupId = paramo[Long]("groupid").filter(_ > 0).getOrElse (-1L)
  lazy val currentUserId = sessiono[Long]("userId") getOrElse (-1L)

  beforeFilter {
    groupId match {
      case -1L =>
        respondJson(Map ("status"->"error", "data"->"INVALID_FORM_DATA"))
        false
      case _ => true
    }
  }

  def execute =
    groups.delGroup(groupId, currentUserId) onComplete {
      case Success(0) =>
        respondJson(Map ("status"->"error", "data"->"GROUP_HAVENT_DELETED"))
      case Success (x) =>
        respondJson(Map ("status"->"success", "data"->x))
      case Failure(err) =>
        log error s"$err"
        respondJson(Map ("status"->"error", "data"->"GROUP_HAVENT_DELETED"))
    }
}


@POST("gr-newuser")
class NewUserInGroup extends GroupsUserCrud {
  def execute =
    groups.addUserToGroup(groupId, userId, currentUserId) onComplete {
      case Success(GroupRelRow(-1L, _, _, _, _, _)) =>
        respondJson(Map ("status"->"error", "data"->"USER_DOESNT_ADD"))
      case Success (_) =>
        respondJson(Map ("status"->"success", "data"->Map("groupId"->groupId, "userId"->userId)))
      case Failure(err) =>
        log error s"$err"
        respondJson(Map ("status"->"error", "data"->"USER_DOESNT_ADD"))
    }
}

@POST("gr-deluser")
class DelUserFromGroup extends GroupsUserCrud {
  def execute = {
    if (userId != currentUserId)
      groups.delUserFromGroup(groupId, userId, currentUserId) onComplete {
        case Success(0) =>
          respondJson(Map("status" -> "error", "data" -> "USER_NOT_DELETED"))
        case Success(_) =>
          respondJson(Map("status" -> "success", "data" -> Map("groupId" -> groupId, "userId" -> userId)))
        case Failure(err) =>
          log error s"$err"
          respondJson(Map("status" -> "error", "data" -> "USER_NOT_DELETED"))
      }
    else respondJson(Map("status" -> "error", "data" -> "USER_NOT_DELETED"))
  }
}

@GET("gr-dropdown")
class GroupsAutoComplete extends Users{
  val elastic = ElasticGroupDao.apply

  def execute =
    paramo[String]("pattern") match {
      case _@None        => respondJson(Map("status"->"error", "data"->"NAME_NON_EMPTY"))
      case Some(pattern) if !pattern.isEmpty =>
        elastic.searchMatchGroup(pattern) onComplete {
          case Success(x) =>
            val suggests = x.hits.map{hit =>
              log info s"hits:${hit.sourceAsMap}"
              log info s"hits another :${hit.fields}"
              Map("id"->hit.getId, "suggest"->hit.sourceAsMap.getOrElse("groupname", ""))
            }
            respondJson(Map ("status"->"success", "data"->suggests))
          case Failure(err) =>
            log error s"$err"
            respondJson(Map ("status"->"success", "data"->""))
        }
    }
}