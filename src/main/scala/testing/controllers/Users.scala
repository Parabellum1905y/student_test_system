package testing.controllers

//import quickstart.action.AnotherProfileBlock
import com.sksamuel.elastic4s.RichSearchResponse
import quickstart.action.{AnotherProfileBlock, BeforeRespond}
import testing.dao.Db
import testing.dao.usersDAO.{ElasticUserDao, UserDao}
import xitrum.FutureAction
import xitrum.annotation.{GET, POST, Swagger}

import scala.util.{Failure, Success}
import testing.model.users.{NoneUser, User}

import scala.concurrent.Future

trait Users extends BeforeRespond with xitrum.Log

//class UsersRegistration extends Users{
//  
//}

@GET("profile/:id<[0-9]+>", "profile/:id<[0-9]+>.:path")
class UsersProfile extends Users{
  def execute {
    val id = paramo[Long]("id").getOrElse(-1L)
    val path = paramo[String]("path").getOrElse("json")
    val user = UserDao getUserByIdSafe id
    user.onComplete { 
      case Success(x) =>
        val n = NoneUser
        val us = x.getOrElse(NoneUser)
        log debug s"user:${us}" 
        if (path == "json") respondJson(Map("user"->us))
        else if (path == "html"){
          at("user") = us
          respondView[AnotherProfileBlock] ()
        }
        else respond500Page()
      case Failure(err) =>
        log info ("error ", err)
        respond404Page()
    }
  }
}

@GET("dropdown")
class UsersAutoComplete extends Users{
  val elastic = ElasticUserDao.apply

  def execute =
    paramo[String]("pattern") match {
      case _@None        => respondJson(Map("status"->"error", "data"->"NAME_NON_EMPTY"))
      case Some(pattern) if !pattern.isEmpty =>
        elastic.searchMatchUser(pattern) onComplete {
          case Success(x) =>
            val suggests = x.hits.map{hit =>
              log info s"hits:${hit.sourceAsMap.get("suggest")}"
              log info s"hits another :${hit.fields}"
              Map("id"->hit.getId, "suggest"->hit.sourceAsMap.getOrElse("suggest", ""))
            }
            respondJson(Map ("status"->"success", "data"->suggests))
          case Failure(err) =>
            log error s"$err"
            respondJson(Map ("status"->"success", "data"->""))
        }
    }
}

@POST("profile/update")
class UsersProfileUpdate extends Users{
  def execute {
    val isLoggedOn = session isDefinedAt "userId"
    if (isLoggedOn) {
      val sessionUserId = sessiono[Long]("userId").get
      val avatarFileId = paramo[Long]("avatarFileId").getOrElse(-1L)
      val name = paramo[String]("name").getOrElse("")
      val surname = paramo[String]("surname").getOrElse("")
      val aboutMe = paramo[String]("aboutMe").getOrElse("")
      val newNick  = paramo[String]("nick").getOrElse("")
      val birthDate = paramo[String]("birthdate").getOrElse("")
      if (name.isEmpty)respondJson(Map("status"->"error", "data"->"NAME_NON_EMPTY"))
      else
        UserDao.update {
          User("", Some(""), nick = newNick,
            userId = sessionUserId, avatarFileId = Some(avatarFileId),
            name = Some(name), surname = Some(surname), aboutMe = Some(aboutMe))
        } onComplete{
          case Success (None) =>
            respondJson(Map("status"->"error", "data"->"USER_NOT_UPDATED"))
          case Success(Some(user))  =>
            session.clear()
            session ("userId") = user.userId
            session ("authLevel") = 0
            session ("user") = user
            respondJson(Map("status"->"success"))
          case Failure(ex) =>
            log error s"$ex"
            respondJson(Map("status"->"error", "data"->"USER_NOT_UPDATED"))
        }
    } else respondJson(Map("status"->"error", "data"->"USER_NOT_LOGGED"))
  }
}

@POST("profile/security")
class UsersSecurityUpdate extends Users{
  def execute {
    val sessionUserId = sessiono[Long]("userId").get
    val oldPwd = paramo[String]("oldPwd").getOrElse("")
    val newPwd = paramo[String]("newPwd").getOrElse("")
    val confirmPwd = paramo[String]("confirmPwd").getOrElse("")
    (oldPwd, newPwd, confirmPwd) match {
      case ("", _, _) => respondJson(Map("status" -> "error", "data" -> "INVALID_PWD"))
      case (_, x, y) if x != y => respondJson(Map("status" -> "error", "data" -> "NOT_EQ_PWD_CONFIRM"))
      case _ => UserDao getUserById sessionUserId onComplete {
        case Success(None)=>respondJson(Map("status"->"error", "data"->"USER_NOT_FOUND"))
        case Success(Some(x))=>x match {
          case user if user.userPass.contains(Db.crypt.sha1(oldPwd))=>
            UserDao.fullUpdate {
              user.copy(userPass = Option(newPwd))
            } onComplete{
              case Success (None) =>
                respondJson(Map("status"->"error", "data"->"USER_NOT_UPDATED"))
              case Success(user)  =>
                session.clear()
                session ("userId") = user.get.userId
                session ("authLevel") = 0
                session ("user") = user.get
                respondJson(Map("status"->"success"))
              case Failure(ex) =>
                log error s"$ex"
                respondJson(Map("status"->"error", "data"->"USER_NOT_UPDATED"))
            }
          case _ =>respondJson(Map("status"->"error", "data"->"INVALID_PWD"))
        }
        case Failure(ex)=>
          log error s"$ex"
          respondJson(Map("status"->"error", "data"->"USER_NOT_FOUND"))
      }

    }
  }
}