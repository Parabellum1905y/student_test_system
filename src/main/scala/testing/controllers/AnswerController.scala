package testing.controllers

import quickstart.action.BeforeRespond
import testing.dao.tests.{AnswersDao, TasksDao, TestsDao}
import testing.model.tests.AnswersRow
import xitrum.annotation.{GET, POST}
import testing.dao.tests.AnswerHelper._

import scala.util.{Failure, Random, Success}

/**
  * Created by luger on 10.06.16.
  */
trait AnswerController  extends BeforeRespond with xitrum.Log{
  lazy val testDao = TestsDao
  lazy implicit val taskDao = TasksDao.apply
  lazy implicit val answerDao = AnswersDao.apply
  lazy val currentUserId = sessiono[Long]("userId").filter(_ > 0).getOrElse (-1L)
  lazy val langId = sessiono[Long]("langId").filter(_ > 0).getOrElse(-1L)
  lazy val creatorId = currentUserId
}


@GET("ans-list/:taskid<[0-9]+>")
class AnswersListLoad extends AnswerController{

  lazy val taskId = paramo[Long]("taskid").filter(_ > 0).getOrElse(-1L)


  def execute =
    answerDao getForTask taskId onComplete {
      case Success(aseq) =>
        respondHtml(
          newComponent[AnswersLists]().render(taskId, aseq))
      case Failure(er) =>
        log error s"$er"
        respondJson(Map("status" -> "error",
          "data" -> "EMPTY"))
    }

}

@GET("run-ans-list/:taskid<[0-9]+>/:multi<[0-9]+>")
class RunAnswersListLoad extends AnswerController{

  lazy val taskId = paramo[Long]("taskid").filter(_ > 0).getOrElse(-1L)
  lazy val multi = paramo[Short]("multi").filter(_ > 0).getOrElse(0.toShort)

  def execute =
    (for {
      aseq <- answerDao.getForTask (taskId)
      lseq <- answerDao.getLogForTask(taskId)
    }yield (aseq, lseq)) onComplete {
      case Success(seq) =>
        respondHtml(
          newComponent[RunAnswersLists]().render(taskId, Random.shuffle(seq._1), seq._2, multi))
      case Failure(er) =>
        log error s"$er"
        respondJson(Map("status" -> "error",
          "data" -> "EMPTY"))
    }

}

trait AnswerPut extends AnswerController {
  lazy val taskId = paramo[Long]("taskid").filter(_ > 0).getOrElse(-1L)
  lazy val isRight = paramo[Short]("isright").filter(_ >= 0).getOrElse(0.toShort)
  lazy val answer = paramo[String]("answer").getOrElse("")
  lazy implicit val ownerId = currentUserId

}

@GET("answer/:taskid<[0-9]+>/:answerid<[0-9]+>")
class AnswerGet extends AnswerController {
  lazy val answerid = paramo[Long]("answerid").filter(_ > 0).getOrElse(-1L)
  lazy val taskId = paramo[Long]("taskid").filter(_ > 0).getOrElse(-1L)

  def execute =
    answerDao get answerid onComplete {
      /*
            case Success(None) =>
              respondJson(Map("status"->"success", "data"->"EMPTY_TASK"))
      */
      case Success(answer) =>
        respondJson(Map("status"->"success", "data"->answer))
      case Failure(err) =>
        log error s"$err"
    }
}

@POST("answers")
class NewAnswer extends AnswerPut {
  beforeFilter {
    (answer, taskId) match {
      case ("", _)|(_, -1L)=>
        respondJson(Map ("status"->"error", "data"->"INVALID_FORM_DATA"))
        false
      case _ => true
    }
  }

  def newAnswer = AnswersRow(-1L, taskId, answer, isRight)
  def execute =
    newAnswer.create onComplete{
      case Success(None) =>
        respondJson(Map("status"->"error", "data"->"ANSWER_DOESNT_CREATE"))
      case Success(x) =>
        respondJson(Map("status"->"success", "data"->x.get))
      case Failure(err)=>
        log error s"$err"
        respondJson(Map("status"->"error", "data"->"ANSWER_DOESNT_CREATE"))
    }
}

@POST("eanswers")
class EditAnswer extends AnswerPut {
  lazy val answerid = paramo[Long]("answerid").filter(_ > 0).getOrElse(-1L)

  beforeFilter {
    (answer, answerid) match {
      case ("", _)|(_, -1L)=>
        respondJson(Map ("status"->"error", "data"->"INVALID_FORM_DATA"))
        false
      case _ => true
    }
  }

  def newAnswer = AnswersRow(answerid, taskId, answer, isRight)
  def execute =
    newAnswer.update onComplete{
      case Success(None) =>
        respondJson(Map("status"->"error", "data"->"ANSWER_DOESNT_UPDATE"))
      case Success(x) =>
        respondJson(Map("status"->"success", "data"->x.get))
      case Failure(err)=>
        log error s"$err"
        respondJson(Map("status"->"error", "data"->"ANSWER_DOESNT_UPDATE"))
    }
}

@POST("delanswers")
class DelAnswer extends AnswerController {
  lazy val answerid = paramo[Long]("answerid").filter(_ > 0).getOrElse(-1L)
  lazy val taskId = paramo[Long]("taskid").filter(_ > 0).getOrElse(-1L)
  lazy implicit val ownerId = currentUserId

  beforeFilter {
    (taskId, answerid) match {
      case (-1L, _)|(_, -1L)=>
        respondJson(Map ("status"->"error", "data"->"INVALID_FORM_DATA"))
        false
      case _ => true
    }
  }

  def execute =
    answerDao.delete(answerid, taskId, ownerId) onComplete{
      case Success(x) =>
        respondJson(Map("status"->"success", "data"->Map("taskid"->taskId, "count"->x)))
      case Failure(err)=>
        log error s"$err"
        respondJson(Map("status"->"error", "data"->"ANSWER_DOESNT_DELETE"))
    }
}