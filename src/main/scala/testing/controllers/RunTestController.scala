package testing.controllers

import java.nio.charset.StandardCharsets
import javax.mail.internet.{MimeBodyPart, InternetAddress => iaddr}

import com.sksamuel.elastic4s.RichGetResponse
import courier.{Envelope, Mailer, Multipart}
import mailer.MailerConf
import quickstart.action.{SuccessMailReport, TestRun, TestRunServ, ViewLoggedTest}
import testing.dao.tests.{ElasticTestRunDao, MixedTask}
import testing.dao.usersDAO.UserDao
import testing.model.tests.{AnswersLogRow, TaskLogRow, TestsLogRow, TestsRow}
import xitrum.annotation.{GET, POST}

import scala.collection.JavaConversions._
import scala.concurrent.Future
import scala.util.{Failure, Success}

/**
  * Created by luger on 11.06.16.
  */
class RunTestController extends TestsController{
  lazy val testId = paramo[Long]("testid").filter(_ > 0).getOrElse(-1L)

  beforeFilter {
    testId match {
      case -1L =>
        respondJson(Map ("status"->"error", "data"->"INVALID_FORM_DATA"))
        false
      case _ => true
    }
  }

  def newId = testId

  def execute =
    testDao getAccessedTestForUser (currentUserId, testId) onComplete{
      case Success(None) =>
        respondJson("status"->"success", "data"->"EMPTY")
      case Success(testAccessed) =>
        respondJson(Map("status"->"success", "data"->testAccessed.get))
      case Failure(err) =>
        log error s"$err"
        respondJson("status"->"error", "data"->"EMPTY")
    }
}

@POST ("start-test")
class StartTest extends TestsController{
  lazy val testId = paramo[Long]("testid").filter(_ > 0).getOrElse(-1L)
  lazy val elasticRunTest = ElasticTestRunDao.apply

  beforeFilter {
    testId match {
      case -1L =>
        respondJson(Map ("status"->"error", "data"->"INVALID_FORM_DATA"))
        false
      case _ => true
    }
  }

  def newId = testId

  def execute =
    (for {
      lastTaskIdResp <- elasticRunTest.getLastTaskId(currentUserId)
      testAccessed   <- {
        if (lastTaskIdResp.isExists) Future {Option.empty[TestsLogRow]}
        else testDao fullSartTest (testId, currentUserId)
      }
    } yield (testAccessed, lastTaskIdResp)) onComplete{
      case Success((None, x)) =>
        if (x.isExists) {
          val logid = x.field("testlogid").getValue.asInstanceOf[Long]
          val tasknum = x.field("lasttasknum").getValue.asInstanceOf[Int]
          respondJson(Map ("status"->"error",
                          "data"-> "EMPTY", "accessed" -> true,
                                      "uri" -> url[TestRun]("testlogid"->logid, "tasknum"->tasknum))
                          )
        }else {
          respondJson(Map ("status"->"error",
            "data"-> "EMPTY", "accessed" -> false))
        }
      case Success(testAccessed) =>
        elasticRunTest.indexLastTaskId(currentUserId, testAccessed._1.get.testlogid, -1L, 1)
        respondJson(Map("status"->"success", "data"->url[TestRun]("testlogid"->testAccessed._1.get.testlogid, "tasknum"->1)))
      case Failure(err) =>
        log error s"$err"
        respondJson(Map ("status"->"error", "data"->"EMPTY", "accessed" -> false))
    }
}


@POST ("answertask")
class AnswerTask extends TestsController{
  lazy val taskId = paramo[Long]("taskid").filter(_ > 0).getOrElse(-1L)
  lazy val logId = paramo[Long]("logid").filter(_ > 0).getOrElse(-1L)
  lazy val answer = params[Long] ("answer[]")
  lazy val elasticRunTest = ElasticTestRunDao.apply

  beforeFilter {
    (taskId, logId) match {
      case (-1L, _)|(_, -1L) =>
        respondJson(Map ("status"->"error", "data"->"INVALID_FORM_DATA"))
        false
      case _ => true
    }
  }

  def newId = -1L

  def execute = {
    log info s"answer := ${answer}"
    val t: Future[(RichGetResponse, (Seq[AnswersLogRow], Option[TaskLogRow]))] = for {
      lastTaskIdResp: RichGetResponse <- elasticRunTest.getLastTaskId(currentUserId)
      answersLog : (Seq[AnswersLogRow], Option[TaskLogRow]) <-
        if (lastTaskIdResp.isExists) {
          testDao setAnswer(answer.toList, logId, taskId)
        }else {
          Future {
            (Seq.empty[AnswersLogRow], None)
          }
        }
    } yield (lastTaskIdResp, answersLog)
    t.onComplete{
      case Success(x) =>
        if (x._1.isExists) {
          testDao.getNotAnswered(logId) onComplete {
            case Success(Nil) =>
              redirectTo[TestFinish]("testlogid"->logId)
            case Success(ans) =>
              (for {
                test <- testDao.get(ans.head.testid)
                queue<- TestRunServ.getQueue(currentUserId, test, logId)
              } yield queue) onComplete{
                case Success(queue) =>
                  val newQ = queue.get.filter(t => ans.map(_.taskid).contains(t.taskId))
                  val afterSeq = newQ.dropWhile(_.taskId != taskId)
                  val tasknum = if (afterSeq.isEmpty || afterSeq.size == 1) {
                    newQ.filter(_.taskId != taskId).lastOption.getOrElse(MixedTask(-1L, 1)).orderNum
                  }else{
                    afterSeq(1).orderNum
                  }
                  respondJson(Map ("status"->"error",
                    "data"-> "EMPTY", "accessed" -> true,
                    "uri" -> url[TestRun]("testlogid"->{
                      if (x._2._2.isDefined) x._2._2.get.testlogid else -1L
                    }, "tasknum"->tasknum))
                  )
                case Failure(err) =>
                  log error s"$err"
                  respondJson(Map ("status"->"error", "data"->"EMPTY", "accessed" -> false))
              }
            case Failure(err) =>
              log error s"$err"
              respondJson(Map ("status"->"error", "data"->"EMPTY", "accessed" -> false))
          }
        }else {
          respondJson(Map ("status"->"error",
            "data"-> "EMPTY", "accessed" -> false))
        }
      case Failure(err) =>
        log error s"$err"
        respondJson(Map ("status"->"error", "data"->"EMPTY", "accessed" -> false))
    }
  }
}

@POST ("nexttask")
class NextTask extends TestsController{
  lazy val taskId = paramo[Long]("taskid").filter(_ > 0).getOrElse(-1L)
  lazy val logId = paramo[Long]("logid").filter(_ > 0).getOrElse(-1L)
  lazy val answer = params[Long] ("answer[]")
  lazy val elasticRunTest = ElasticTestRunDao.apply

  beforeFilter {
    (taskId, logId) match {
      case (-1L, _)|(_, -1L) =>
        respondJson(Map ("status"->"error", "data"->"INVALID_FORM_DATA"))
        false
      case _ => true
    }
  }

  def newId = -1L

  def execute = {
    log info s"answer := ${answer}"
    val t: Future[(RichGetResponse, (Seq[AnswersLogRow], Option[TaskLogRow]))] = for {
      lastTaskIdResp: RichGetResponse <- elasticRunTest.getLastTaskId(currentUserId)
      answersLog : (Seq[AnswersLogRow], Option[TaskLogRow]) <-
      if (lastTaskIdResp.isExists) {
        testDao skipAnswer(logId, taskId)
      }else {
        Future {
          (Seq.empty[AnswersLogRow], None)
        }
      }
    } yield (lastTaskIdResp, answersLog)
    t.onComplete{
      case Success(x) =>
        if (x._1.isExists) {
          testDao.getNotAnswered(logId) onComplete {
            case Success(Nil) =>
              redirectTo[TestFinish]("testlogid"->logId)
            case Success(ans) =>
              (for {
                test <- testDao.get(ans.head.testid)
                queue<- TestRunServ.getQueue(currentUserId, test, logId)
              } yield queue) onComplete{
                case Success(queue) =>
                  val newQ = queue.get.filter(t => ans.map(_.taskid).contains(t.taskId))
                  val afterSeq = newQ.dropWhile(_.taskId != taskId)
                  val tasknum = if (afterSeq.isEmpty || afterSeq.size == 1) {
                    newQ.filter(_.taskId != taskId).lastOption.getOrElse(MixedTask(-1L, 1)).orderNum
                  }else{
                    afterSeq(1).orderNum
                  }
                  respondJson(Map ("status"->"error",
                    "data"-> "EMPTY", "accessed" -> true,
                    "uri" -> url[TestRun]("testlogid"->{
                      if (x._2._2.isDefined) x._2._2.get.testlogid else -1L
                    }, "tasknum"->tasknum))
                  )
                case Failure(err) =>
                  log error s"$err"
                  respondJson(Map ("status"->"error", "data"->"EMPTY", "accessed" -> false))
              }
            case Failure(err) =>
              log error s"$err"
              respondJson(Map ("status"->"error", "data"->"EMPTY", "accessed" -> false))
          }
        }else {
          respondJson(Map ("status"->"error",
            "data"-> "EMPTY", "accessed" -> false))
        }
      case Failure(err) =>
        log error s"$err"
        respondJson(Map ("status"->"error", "data"->"EMPTY", "accessed" -> false))
    }
  }
}


@POST ("fintest")
class TestFinish extends TestsController{
  //lazy val testId = paramo[Long]("testid").filter(_ > 0).getOrElse(-1L)
  lazy val logId = paramo[Long]("logid").filter(_ > 0).getOrElse(-1L)
  lazy val elasticRunTest = ElasticTestRunDao.apply

  beforeFilter {
    logId match {
      case -1L =>
        respondJson(Map ("status"->"error", "data"->"INVALID_FORM_DATA"))
        false
      case _ => true
    }
  }

  def newId = -1L

  def sendConfirmMail (mailTo:String, userName:String, testLogId:Long, testName:String) = {
    val conf = MailerConf.apply
    val mailer = Mailer(conf.host, conf.port.toInt)
      .auth(conf.auth.toBoolean)
      .as(conf.userName, conf.userPassword)
      .startTtls(true)()
    val future = mailer(Envelope.from(new iaddr(conf.userName, conf.from, "UTF-8"))
      .to(new iaddr(mailTo, userName, "UTF-8"))
      .subject(t("Note about successfully finishing your test!"))
      .content(Multipart()
        .add({
          val part = new MimeBodyPart
          part.setText(newComponent[SuccessMailReport]().render(testLogId, userName, testName),
            StandardCharsets.UTF_8.name, "html")
          part
        })))
    /*.onSuccess {
      case x => log info s"delivered report:$x"
    }*/
    future
  }

  def execute = {
    val finTest:Future[(Int, Option[TestsRow])] = for {
      lastTaskIdResp <- elasticRunTest.getLastTaskId(currentUserId)
      testClosed   <- {
        if (lastTaskIdResp.isExists) testDao.finishTest(logId, currentUserId)
        else Future {0}
      }
      test: Option[TestsRow] <- testDao.getForLogId(logId)
    } yield (testClosed, test)
    finTest.onComplete{
      case Success((0, _)) =>
        respondJson(Map ("status"->"error", "data"->"EMPTY", "accessed" -> false))
      case Success((c, x)) =>

        /**
          * drop in elastic search last Task id
          */
        elasticRunTest.deleteLastTaskId(currentUserId)

        /**
          * get user
          */
        UserDao.getUserById(x.get.creatorid).onComplete {
          case Success(Some(user)) =>
            sendConfirmMail(user.userLogin, user.nick, logId, x.get.testname).onComplete{
              case Success(_) =>
                log info s"mail to ${user.userLogin} sent"
              case Failure(err)=>
                log error s"$err"
            }
            respondJson(Map("status"->"success", "data"->url[ViewLoggedTest]("testlogid"->logId)))
          case Success(None)=>
            respondJson(Map ("status"->"error", "data"->"EMPTY", "accessed" -> false))
          case Failure(err) =>
            log error s"$err"
            respondJson(Map ("status"->"error", "data"->"EMPTY", "accessed" -> false))
        }
      case Failure(err) =>
        log error s"$err"
        respondJson(Map ("status"->"error", "data"->"EMPTY", "accessed" -> false))
    }
  }
}

