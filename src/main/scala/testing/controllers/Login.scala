package testing.controllers

import java.nio.charset.StandardCharsets
import javax.mail.internet.{MimeBodyPart, InternetAddress => iaddr}

import courier._
import mailer.MailerConf
import quickstart.action.{MailConfirm, SiteIndex}

import scala.concurrent.{Await, Future}
import scala.concurrent.duration._
import scala.util.{Failure, Success}
import testing.dao.usersDAO.{ElasticUserDao, GroupsDao, UserDao}
import testing.model.users.{GroupRelRow, User}
import xitrum.FutureAction
import xitrum.annotation.{GET, POST}

/**
  * Created by luger on 28.02.16.
  */
trait Login extends FutureAction with xitrum.Log{
  lazy val userman = UserDao
  lazy val mailPattern = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$".r
  lazy val mailPatternReg = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"
}

@POST("login")
class LogUser extends Login{
  override def execute {
    val l = paramo[String]("login")
    val p = paramo[String]("password")
    val r = paramo[Int]("rememberme")//TODO
    log info s"${session isDefinedAt "userId"}"
    (l, p) match {
      case (None, _)=>respondJson(Map("status"->"error", "data"->"INVALID_LOGIN"))
      case (Some(x), _) if !x.matches(mailPatternReg) =>respondJson(Map("status"->"error", "data"->"INVALID_LOGIN"))
      case (_, None)=>respondJson(Map("status"->"error", "data"->"INVALID_PWD"))
      case (Some(login), Some(pwd))=> userman.getUserByLogin(login, pwd).onComplete{
        case Success(None)=>respondJson(Map("status"->"error", "data"->"USER_NOT_FOUND"))
        case Success(Some(x))=>x match {
          case user if user.enabled==1=>
            session.clear()
            session ("userId") = user.userId
            session ("authLevel") = 0
            session ("user") = user
            respondJson(Map("status"->"success", "data"->"OK"))
          case _ =>respondJson(Map("status"->"error", "data"->"USER_NOT_ENABLED"))
        }
        case Failure(ex)=>
          log error s"$ex"
          respondJson(Map("status"->"error", "data"->"USER_NOT_FOUND"))
      }
    }
  }
}

@POST("logout")
class LogoutUser extends Login{
  override def execute {
    if (session isDefinedAt "userId"){
      session remove "userId"
      session remove "authLevel"
      session remove "user"
    }
    session.clear()
    respondJson(Map("status"->"success", "data"->"OK"))
  }
}

@POST("signin")
class RegUser extends Login{
  def getOrCreateUser (login:String, pwd:String, name:String, surname:String):Future[(Long, String)] = for {
    existingUser: Option[User] <- userman.getUserByLogin(login)
    result<- existingUser.fold(
      userman createAndLog User(login, Some(pwd), name,
                                userMail = Some(login),
                                name = Option(name),
                                surname = Option(surname),
                                enabled = 0)){ _=>
      Future{(-1L, "")}}
  } yield result

  def sendConfirmMail (login:String, hash:String) = {
    val conf = MailerConf.apply
    val mailer = Mailer(conf.host, conf.port.toInt)
      .auth(conf.auth.toBoolean)
      .as(conf.userName, conf.userPassword)
      .startTtls(true)()
    val future = mailer(Envelope.from(new iaddr(conf.userName, conf.from, "UTF-8"))
      .to(new iaddr(login, login, "UTF-8"))
      .subject("Подтверждение пароля")
      .content(Multipart()
          .add({
            val part = new MimeBodyPart
            part.setText(newComponent[MailConfirm]().render(Array("code"->hash)),
                          StandardCharsets.UTF_8.name, "html")
            part
          })))
    /*.onSuccess {
      case x => log info s"delivered report:$x"
    }*/
    Await.ready(future, conf.timeout.toInt.seconds)
  }

  override def execute {
    val name = paramo[String]("name")
    val surname = paramo[String]("surname")
    val agreement = paramo[Int]("agreement").getOrElse(0)
    val l = paramo[String]("login")
    val p = paramo[String]("password")
    val ch = paramo[String]("passwordconfirm")
    (name, surname, agreement) match {
      case (_, _, 0) => respondJson(Map("status" -> "error", "data" -> "AGREEMENT_NOT_CONFIRMED"))
      case (None, _, 1) => respondJson(Map("status" -> "error", "data" -> "INVALID_NAME"))
      case (_, None, 1) => respondJson(Map("status" -> "error", "data" -> "INVALID_SURNAME"))
      case _ => (l, p, ch) match {
          case (None, _, _) => respondJson(Map("status" -> "error", "data" -> "INVALID_LOGIN"))
          case (_, None, _) => respondJson(Map("status" -> "error", "data" -> "INVALID_PWD"))
          case (_, _, None) => respondJson(Map("status" -> "error", "data" -> "INVALID_PWD_CONFIRM"))
          case (_, Some(x), Some(y)) if x != y => respondJson(Map("status" -> "error", "data" -> "NOT_EQ_PWD_CONFIRM"))
          case (Some(login), Some(pwd), Some(check)) =>
            getOrCreateUser(login, pwd, name.get, surname.get).onComplete {
              case Success((-1, "")) => respondJson(Map("status" -> "error", "data" -> "ALREADY_FOUND"))
              case Success(x) =>
                sendConfirmMail(login, x._2)
                respondJson(Map("status" -> "success", "data" -> "OK"))
              case Failure(ex) =>
                log error s"$ex"
                respondJson(Map("status" -> "error", "data" -> "USER_NOT_CREATED"))
            }
        }
    }
  }
}

@GET("confirm")
class ConfirmRegUser extends Login{
  def execute = paramo[String]("code") match {
    case None      => redirectTo[SiteIndex]()
    case Some(code)=> userman activateUser code onComplete {
      case Success(None) =>
        redirectTo[SiteIndex]("msg"->"failed_activation")
      case Success(user) =>
        // add to ALL group with owner of ADMIN argrg!!
        GroupsDao addUserToGroup (2L, user.get.userId, 3L) onComplete {
          case Success(GroupRelRow(-1L, _, _, _, _, _)) =>
            redirectTo[SiteIndex]("msg"->"failed_activation")
          case Success (_) =>
            redirectTo[SiteIndex]("msg"->"finish_activation")
          case Failure(err) =>
            log error s"$err"
            redirectTo[SiteIndex]("msg"->"failed_activation")
        }
      case Failure(ex) =>
        log error s"$ex"
        redirectTo[SiteIndex]("msg"->"failed_activation")
    }
  }
}
