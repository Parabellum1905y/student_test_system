package testing.controllers

import testing.model.users.User
import xitrum.Component

/**
  * Created by luger on 13.05.16.
  */
class UsersLists extends Component {
  def render(groupId:Long, useq:Seq[User], currentUserId:Long) = {
    at("users_seq") = useq
    at("gid") = groupId
    at("currentUserId") = currentUserId
    renderView()
  }
}

