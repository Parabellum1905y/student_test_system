package testing.controllers

import testing.model.tests.{AnswersLogRow, AnswersRow}
import xitrum.Component

import scala.util.Random

/**
  * Created by luger on 13.05.16.
  */
class AnswersLists extends Component {
  def render(taskId:Long, aseq:Seq[AnswersRow]) = {
    at("ans_seq") = aseq.sortBy(_.answerid)
    at("tid") = taskId
    renderView()
  }
}

class RunAnswersLists extends Component {
  def render(taskId:Long, aseq:Seq[AnswersRow], answerlog:Seq[AnswersLogRow], multiAnswer:Short) = {
    at("ans_seq") = Random.shuffle(aseq)
    at("answerlog") = answerlog
    at("tid") = taskId
    at("multianswer") = multiAnswer
    renderView()
  }
}

