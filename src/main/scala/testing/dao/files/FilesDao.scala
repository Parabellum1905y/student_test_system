package testing.dao.files

/**
  * Created by luger on 10.03.16.
  */

import testing.dao.Db
import db.PostgresDriverPgExt.api._
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global
import java.time.LocalDateTime
import testing.model.files.{Files, FilesRow, Fileaccesses, FileaccessesRow}

/**
  * managing files in files database
  */
object FilesDao extends Db with xitrum.Log {
  val files = Files.get
  val accesses = Fileaccesses.get

  def getFileById (id:Long):Future[Option[FilesRow]]= {
    try db.run(files.filter ({ x => x.fileid===id }).result.headOption)
    catch {
      case ex:Exception => {
        log error ("ex:", ex)
        Future[Option[FilesRow]] {
          None
        }
      }
    }
    //    finally db.close()
  }

  def update (file:FilesRow) = {
  }

  def create (file:FilesRow):Future[Long] = {
    val q = (files returning files.map ( _.fileid )) += file
    db.run(q)
  }

  def createRet (file:FilesRow):Future[Option[FilesRow]] = {
    val q = (files returning files.map ( _.fileid )) += file
    for {
      id:Long    <- db.run(q)
      createdFile<- Future {
        if (id>0) Some(file.copy(fileid = id)) else None
      }
    }yield createdFile
  }

  /*
  def createRegistryLog (file:FilesRow):Future[Long] = {
    val regLog = new UserRegistrationLogRow(crypt.sha1(file.userLogin)+uuid, LocalDateTime.now.plusHours(24), file.userId)
    val query = (registrations returning registrations.map(_.regid)) += regLog
    db run query
  }

  def createRegistryLogHashRet (file:FilesRow):Future[String] = {
    val hash = crypt.sha1(file.userLogin)+uuid
    val regLog = new UserRegistrationLogRow(hash, LocalDateTime.now.plusHours(24), file.userId)
    val query = (registrations returning registrations.map(_.regid)) += regLog
    for {
      regid <- db run query
      h     <- Future {if (regid>0) hash else ""}
    } yield h
  }

  /**
    * create file, create log row for file registration, return new FilesRow object and reg hash
    *
    * @param file file to create
    * @return
    */
  def createRetAndLog (file:FilesRow):Future[(Option[FilesRow], String)] = {
    createRet(file).flatMap {
      case userOpt@None => Future {
        (userOpt, "")
      }
      case some => createRegistryLogHashRet(some.get).flatMap({
        hash=>Future{(some, hash)}
      })
    }
  }

  /**
    * create file, create log row for file registration, return new FilesRow Id and reg hash
    *
    * @param file file to create
    * @return
    */
  def createAndLog (file:FilesRow):Future[(Long, String)] = {
    /*createRet(file).flatMap(opt=>opt.fold(Future{(-1L, "")}) {
      u=>createRegistryLogHashRet(u).flatMap {
        hash => Future {
          (u.userId, hash)
        }
      }
    })*///valid CODE , but i want to try for-comprehensions
    for {
      retUser <- createRet(file)
      ret     <- retUser.fold(Future{(-1L, "")}) {u=>
        createRegistryLogHashRet(u).flatMap {
          hash => Future {(u.userId, hash)}
        }
      }
    } yield (ret)
  }

  /**
    * full activate file by hash in reg log
    *
    * @param regHash
    */
  def activateUser (regHash:String):Future[Int] = {
    val filteredReg = registrations filter(_.hashtag === regHash)
    val userIdQuery = filteredReg  sortBy(p=>p.expiredate.desc) map (reg => reg.userid)
    val allUpd = (for {
      _          <- filteredReg map (reg => reg.enabled) update (1)
      result:Int <- files.filter(_.fileid in userIdQuery) map (us => (us.registerDate, us.enabled)) update (LocalDateTime.now, 1)
    } yield result).transactionally
    db.run(allUpd)
  }
  */
}
