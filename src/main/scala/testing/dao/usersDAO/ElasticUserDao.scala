package testing.dao.usersDAO

import elastic.ElasticConf
import testing.model.users.User
import com.sksamuel.elastic4s._
import com.sksamuel.elastic4s.ElasticDsl._
import org.elasticsearch.action.update.UpdateResponse
import org.elasticsearch.index.query.MatchQueryBuilder

import scala.concurrent.Future

/**
  * Created by luger on 21.05.16.
  */
trait IElasticUserDao extends xitrum.Log{
  val conf = ElasticConf.apply
  val client = ElasticClient.transport(ElasticsearchClientUri("elasticsearch://"+conf.host+":"+conf.port))

  def createIndex (user:User): Future[IndexResult]
  def updateIndex (user:User): Future[UpdateResponse]
  def getById (id:Long) : Future[RichGetResponse]
  def searchSuggestionsUser (pattern:String): Future[RichSearchResponse]
  def searchUser (pattern:String): Future[RichSearchResponse]
  def searchMatchUser (pattern:String): Future[RichSearchResponse]
}

class ElasticUserDao extends IElasticUserDao{
  def createIndex (user:User): Future[IndexResult] =
    client.execute {
      index into "testing" / "users" id user.userId fields (
        ("suggest", user.userLogin+", "+user.nick+" "+user.surname.getOrElse("")+" "+user.name.getOrElse(""))
        )
    }


  def updateIndex (user:User): Future[UpdateResponse] =
    client.execute {
      update (user.userId) in "testing" / "users" docAsUpsert (
        ("suggest", user.userLogin+", "+user.nick+" "+" "+user.surname.getOrElse("")+" "+user.name.getOrElse(""))
        )
    }

  def getById (id:Long) =
    client execute {
      get id id from "testing" / "users"
    }

  def searchSuggestionsUser (pattern:String): Future[RichSearchResponse] = {
    val mysugg = termSuggestion("mysugg").field("suggest").text(pattern)
    client execute {
      search in "testing/users" suggestions {
        mysugg
      }
    }
  }

  def searchUser (pattern:String): Future[RichSearchResponse] =
    client execute {
      search in "testing/users" query wildcardQuery("suggest", "*"+pattern+"*")
    }

  def searchMatchUser (pattern:String): Future[RichSearchResponse] =
    client execute {
      search in "testing/users" query {
        matchQuery("suggest", pattern).operator(MatchQueryBuilder.Operator.OR)
      }
    }
}

object ElasticUserDao {
  def apply: ElasticUserDao = new ElasticUserDao()
}