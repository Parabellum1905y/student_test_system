package testing.dao.usersDAO

import elastic.ElasticConf
import com.sksamuel.elastic4s._
import com.sksamuel.elastic4s.ElasticDsl._
import org.elasticsearch.action.update.UpdateResponse
import org.elasticsearch.index.query.MatchQueryBuilder
import testing.model.users.UserGroupsRow

import scala.concurrent.Future

/**
  * Created by luger on 21.05.16.
  */
trait IElasticGroupDao extends xitrum.Log{
  val conf = ElasticConf.apply
  val client = ElasticClient.transport(ElasticsearchClientUri("elasticsearch://"+conf.host+":"+conf.port))

  def createIndex (group:UserGroupsRow): Future[IndexResult]
  def updateIndex (group:UserGroupsRow): Future[UpdateResponse]
  def getById (id:Long) : Future[RichGetResponse]
  def searchSuggestionsGroup (pattern:String): Future[RichSearchResponse]
  def searchGroup (pattern:String): Future[RichSearchResponse]
  def searchMatchGroup (pattern:String): Future[RichSearchResponse]
}

class ElasticGroupDao extends IElasticGroupDao{
  def createIndex (group:UserGroupsRow): Future[IndexResult] =
    client.execute {
      index into "testing" / "groups" id group.groupid fields (
        ("groupname", group.groupname.getOrElse("")),
        ("groupdescr", group.groupdescr.getOrElse(""))
        )
    }


  def updateIndex (group:UserGroupsRow): Future[UpdateResponse] =
    client.execute {
      update (group.groupid) in "testing" / "groups" docAsUpsert (
        ("groupname", group.groupname.getOrElse("")),
        ("groupdescr", group.groupdescr.getOrElse(""))
        )
    }

  def getById (id:Long) =
    client execute {
      get id id from "testing" / "groups"
    }

  def searchSuggestionsGroup (pattern:String): Future[RichSearchResponse] = {
    val mysugg = termSuggestion("mysugg").field("groupname").text(pattern)
    client execute {
      search in "testing/groups" suggestions {
        mysugg
      }
    }
  }

  def searchGroup (pattern:String): Future[RichSearchResponse] =
    client execute {
      search in "testing/groups" query wildcardQuery("groupname", "*"+pattern+"*")
    }

  def searchMatchGroup (pattern:String): Future[RichSearchResponse] =
    client execute {
      search in "testing/groups" query {
        multiMatchQuery(pattern).fields("groupname", "groupdescr") operator MatchQueryBuilder.Operator.OR
      }
    }
}

object ElasticGroupDao {
  def apply: ElasticGroupDao = new ElasticGroupDao()
}