package testing.dao.usersDAO

import testing.dao.Db
import testing.model.users._
import db.PostgresDriverPgExt.api._

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global


class InvalidEntityException extends Throwable
/**
  * Created by luger on 16.05.16.
  */
trait GroupsDao extends Db with xitrum.Log {
  lazy val groups = UserGroups.get
  lazy val groupsRel = GroupRel.get
  lazy val users = UsersObj.get
  lazy val elasticGroup = ElasticGroupDao.apply

  def createGroup (groupname: Option[String] = None,
                   groupdescr: Option[String] = None,
                   enabled: Short = 1,
                   hidden: Short = 0):Future[UserGroupsRow]
  def createGroup (group:UserGroupsRow):Future[UserGroupsRow]
  def createGroupWithOwner (userId:Long,
                            groupname: Option[String],
                            groupdescr: Option[String],
                            enabled: Short = 1,
                            hidden: Short = 0):Future[GroupRelRow]
  def createGroupWithOwner (group:UserGroupsRow, user:User):Future[GroupRelRow]
  def createGroupWithOwner (group:UserGroupsRow, userId:Long):Future[GroupRelRow]
  def get (id:Long*):Future[Seq[UserGroupsRow]]
  def get (id:Long):Future[Option[UserGroupsRow]]
  def listAll :Future[Seq[UserGroupsRow]]
  def searchUsersGroups (userId:Long):Future[Seq[(UserGroupsRow, GroupRelRow)]]
  def searchUsersInGroups (groupId:Long):Future[Seq[User]]
  def addUserToGroup (groupId: Long, userId : Long, ownerId:Long):Future[GroupRelRow]
  def delGroup (groupId: Long, ownerId:Long): Future[Int]
  def delUserFromGroup (groupId: Long, userId : Long, ownerId:Long):Future[Int]
}

object GroupsDao extends GroupsDao{
  def createGroup (groupname: Option[String] = None,
                   groupdescr: Option[String] = None,
                   enabled: Short = 1,
                   hidden: Short = 0):Future[UserGroupsRow] =
    createGroup(UserGroupsRow(-1L, groupname, groupdescr, None, enabled, hidden))

  def createGroupQuery (group:UserGroupsRow) = (groups returning groups.map(_.groupid)
    into ((group,id) => group.copy (groupid = id))) += group

  def createGroup (group:UserGroupsRow): Future[UserGroupsRow] =
    db.run(createGroupQuery (group)).map{gr =>
      elasticGroup.updateIndex(gr)
      gr
    }

  def createGroupWithOwner (userId:Long,
                            groupname: Option[String],
                            groupdescr: Option[String],
                            enabled: Short = 1,
                            hidden: Short = 0) =
    createGroupWithOwner(UserGroupsRow(-1L, groupname, groupdescr, None, enabled, hidden), userId)

  def createGroupWithOwner (group:UserGroupsRow, user:User) =
    createGroupWithOwner (group, user.userId)

  def createGroupWithOwner (group:UserGroupsRow, userId:Long) = {
    val q = (for {
      create: UserGroupsRow <- createGroupQuery (group)
      addUserToGroup        <- create match {
        case UserGroupsRow(-1L, _, _, _, _, _) => DBIO.failed(new InvalidEntityException)
        case xGroup =>
          // create elastic index
          elasticGroup.updateIndex(xGroup)
          // check existing row
          val exists = existsGroupRel (xGroup.groupid, userId)
          for {
            posibility: Seq[Int] <- groupsRel.filter(_ => !exists).map(_ => 1).take(1).result
            action     <- posibility match {
              case Nil => DBIO.failed(new InvalidEntityException)
              case _ =>
                val grel = GroupRelRow(-1L, xGroup.groupid, 1.toShort, userId, 1.toShort, 1.toShort)
                (groupsRel returning groupsRel.map(_.grouprelid)
                  into ((grel, id) => grel.copy (grouprelid = id))) += grel
            }
          }yield action
      }
    } yield addUserToGroup).transactionally
    db.run(q)
  }
  //TODO true code but bugged
  /*
groupsRel.forceInsertQuery {
          // check existing row
          val exists = existsGroupRel (xGroup.groupid, user.userId)
          /* val insert = groupsRel returning (groupsRel map (_.grouprelid)) +=
             GroupRelRow(-1L, groupId, 1, userId, 1, 0)*/
          val insert = (-1L, xGroup.groupid, 1.toShort, user.userId, 1.toShort, 0.toShort).
            <>(GroupRelRow.apply _ tupled, GroupRelRow.unapply)
          for (u <- Query(insert) if !exists) yield u
        }
  * */

  def get (id:Long*) =
    db.run(groups.filter(_.groupid inSet id).result)

  def get (id:Long) = get(Seq(id):_*).map(_.headOption)

  def recursiveGroupsForUser (userId:Long) =
    sql"""WITH RECURSIVE categories_stack (groupid, level,objectid, typeid) as (
          select groupid, 0, objectid, typeid
          from group_rel
          where (typeid = 1 and objectid = $userId)
          union all
          select cat.groupid, cat2.level+1, cat2.objectid, cat2.typeid
          from group_rel cat, categories_stack cat2
          where (cat.objectid=cat2.groupid and cat.typeid=2)
          )
          select * from categories_stack
          where level>=0
          order by level, groupid""".as[(Long, Int, Long, Int)].map(_.toSeq)

  def recursiveUsersInGroup (groupId:Long) =
    sql"""select distinct(gr.objectid) from group_rel gr
      join (
          WITH RECURSIVE categories_stack (groupid, level,objectid, typeid) as
          (
            (select groupid, 0, objectid, typeid
            from group_rel
            where groupid = $groupId)
            union all
            select cat.objectid, cat2.level+1, cat2.groupid, cat2.typeid
            from group_rel cat, categories_stack cat2
            where (cat.groupid=cat2.objectid and cat.typeid=2 and cat2.typeid=2)
          )
          select * from categories_stack
          where level>=0
          order by level, groupid) rec on gr.groupid=rec.groupid and gr.typeid=1
      and exists(select 1 from user_groups where groupid=gr.groupid and hidden=0)""".
        as[(Long)].map(_.toSeq)

  def searchUsersGroups (userId:Long) = {
    val groupsIdQuery = recursiveGroupsForUser (userId) map(_.map(_._1))
    val query = for {
      groupsId <- groupsIdQuery
      action <- (groups filter (g => g.groupid inSet groupsId)).distinct.
        join(groupsRel).on{(x, y) =>
          x.groupid === y.groupid
      }.result
    }yield action
    try
      db.run (query)
    catch {
      case ex: Exception =>
        log error("ex:", ex)
        Future[Seq[(UserGroupsRow, GroupRelRow)]] {
          Nil
        }
    }
  }

  def searchUsersInGroups (groupId:Long):Future[Seq[User]] = {
    val usersIdQuery = recursiveUsersInGroup (groupId)
    val query = for {
      usersId <- usersIdQuery
      action  <- (users filter (_.userId inSet usersId)).distinctOn(_.userId).result
    }yield action
    //query.map(d => log info s"usersId: ${d._1}")
    try
      db.run (query)
    catch {
      case ex: Exception =>
        log error("ex:", ex)
        Future[Seq[User]] {
          Nil
        }
    }
  }

  def isGroupOwner (groupId: Long, ownerId:Long) = groupsRel.filter(x =>
    x.typeid === 1.toShort && x.objectid === ownerId &&
    x.isowner === 1.toShort && x.groupid === groupId).map(_ => 1).exists

  def existsGroupRel (groupId: Long, userId : Long) = groupsRel.filter(x =>
    x.typeid === 1.toShort && x.objectid === userId &&
    x.groupid === groupId).map(_ => 1).exists

  def addUserToGroup (groupId: Long, userId : Long, ownerId:Long) = {
    // check owner before
    val isOwner = isGroupOwner (groupId, ownerId)
    // check existing row
    val exists = existsGroupRel (groupId, userId)
    val query = for {
      posibility: Seq[Int] <- groupsRel.filter(_ => !exists && isOwner).map(_ => 1).take(1).result
      action     <- posibility match {
        case Nil => DBIO.failed(new InvalidEntityException)
        case _ =>
          val grel = GroupRelRow(-1L, groupId, 1, userId, 1, 0)
          (groupsRel returning groupsRel.map(_.grouprelid)
            into ((grel, id) => grel.copy (grouprelid = id))) += grel
      }
    }yield action
    db.run(query.transactionally)
  }

  def delGroup (groupId: Long, ownerId:Long): Future[Int] = {
    // check owner before
    val isOwner = isGroupOwner (groupId, ownerId)
    val query = for {
      posibility: Seq[Int] <- groupsRel.filter(_ => isOwner).map(_ => 1).take(1).result
      action     <- posibility match {
        case Nil => DBIO.failed(new InvalidEntityException)
        case _ =>
          groupsRel.filter(_.groupid === groupId).delete.andThen(groups.filter(_.groupid === groupId).delete)
      }
    }yield action
    db.run(query.transactionally)
  }
  //TODO true method but with bugs

  /*def addUserToGroup (groupId: Long, userId : Long, ownerId:Long) = db.run{
      groupsRel.forceInsertQuery {
        // check owner before
        val isOwner = isGroupOwner (groupId, ownerId)
        // check existing row
        val exists = existsGroupRel (groupId, userId)
        /* val insert = groupsRel returning (groupsRel map (_.grouprelid)) +=
           GroupRelRow(-1L, groupId, 1, userId, 1, 0)*/
        val insert = ((-1L).bind, groupId.bind, 1.toShort.bind, userId.bind, 1.toShort.bind, 0.toShort.bind).
                      <>(GroupRelRow.apply _ tupled, GroupRelRow.unapply)
        /*groupsRel.*/
        for (u <- Query(insert) if !exists && isOwner) yield u
      }
  }*/

  def delUserFromGroup (groupId: Long, userId : Long, ownerId:Long) = {
    // check owner before
    val isOwner = isGroupOwner (groupId, ownerId)
    db.run(groupsRel.
      filter (d => d.objectid === userId && d.typeid === 1.toShort && isOwner).
      delete)
  }
/*
    val query = (for {
      isOwner: Seq[Short] <- (groupsRel filter (x => x.typeid === 1.toShort && x.objectid === ownerId &&
        x.isowner === 1.toShort && x.groupid === groupId) map(_.isowner)).result
      action <- if (isOwner.contains(1))
        groupsRel returning(groupsRel map(_.grouprelid)) += GroupRelRow(-1L, groupId, 1, userId, 1, 0)
      else Future{None}
      //if (isOwner == 1)
    }yield action).transactionally
    db.run(query)
  }
*/
  def listAll :Future[Seq[UserGroupsRow]] =
    db.run(groups.result)
}
