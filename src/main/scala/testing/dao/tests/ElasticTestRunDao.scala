package testing.dao.tests

import elastic.ElasticConf
import com.sksamuel.elastic4s._
import com.sksamuel.elastic4s.ElasticDsl._
import org.elasticsearch.action.update.UpdateResponse
import org.elasticsearch.index.query.MatchQueryBuilder
import testing.model.tests.TestsRow
import xitrum.util.SeriDeseri

import scala.concurrent.Future

/**
  * Created by luger on 21.05.16.
  */
trait IElasticTestRunDao extends xitrum.Log{
  val conf = ElasticConf.apply
  val client = ElasticClient.transport(ElasticsearchClientUri("elasticsearch://"+conf.host+":"+conf.port))

  def indexTaskMixingQueue[A<:Seq[MixedTask]] (userId:Long, testLogId:Long, mixTask:A): Future[IndexResult]
  def updateTaskMixingQueue[A<:Seq[MixedTask]] (userId:Long, testLogId:Long, mixTask:A): Future[UpdateResponse]
  def deleteTaskMixingQueue (testLogId:Long)
  def getTaskMixingQueue (testLogId:Long) : Future[RichGetResponse]

  def indexLastTaskId (userId:Long, testLogId:Long, lastTaskId:Long, lastTaskNum:Int): Future[IndexResult]
  def updateLastTaskId (userId:Long, testLogId:Long, lastTaskId:Long, lastTaskNum:Int): Future[UpdateResponse]
  def deleteLastTaskId (userId:Long)
  def getLastTaskId (userId:Long) : Future[RichGetResponse]
}

class ElasticTestRunDao extends IElasticTestRunDao{
  def indexTaskMixingQueue [A<:Seq[MixedTask]] (userId:Long, testLogId:Long, mixTask:A): Future[IndexResult] =
    client.execute {
      log info s"mixTask : ${mixTask}"
      index into "testing" / "runtaskqueue" id testLogId fields (
        ("testlogid", testLogId),
        ("userid", userId),
        ("queue", SeriDeseri.toJson(mixTask))
        )
    }

  def updateTaskMixingQueue[A<:Seq[MixedTask]] (userId:Long, testLogId:Long, mixTask:A): Future[UpdateResponse] =
    client.execute {
      log info s"mixTask : ${mixTask}"
      update (testLogId) in "testing" / "runtaskqueue" docAsUpsert (
        ("testlogid", testLogId),
        ("userid", userId),
        ("queue", SeriDeseri.toJson(mixTask))
        )
    }

  def deleteTaskMixingQueue (testLogId:Long) =
    client.execute {
      delete (testLogId) from "testing" / "runtaskqueue"
    }

  def getTaskMixingQueue (testLogId:Long) =
    client.execute {
      get(testLogId) from "testing" / "runtaskqueue" fields("userid", "testlogid", "queue")
    }

  override def indexLastTaskId(userId: Long,
                               testLogId: Long,
                               lastTaskId: Long,
                               lastTaskNum: Int): Future[IndexResult] =
    client.execute {
      index into "testing" / "runtasklast" id userId fields (
        ("userid", userId),
        ("testlogid", testLogId),
        ("lasttaskid", lastTaskId),
        ("lasttasknum", lastTaskNum)
        )
    }
  override def deleteLastTaskId(userId: Long): Unit =
    client.execute {
      delete (userId) from "testing" / "runtasklast"
    }

  override def updateLastTaskId(userId: Long,
                                testLogId: Long,
                                lastTaskId: Long,
                                lastTaskNum: Int): Future[UpdateResponse] =
    client.execute {
      update (userId) in "testing" / "runtasklast" docAsUpsert (
        ("userid", userId),
        ("testlogid", testLogId),
        ("lasttaskid", lastTaskId),
        ("lasttasknum", lastTaskNum)
        )
    }

  override def getLastTaskId(userId: Long): Future[RichGetResponse] =
    client.execute {
      get(userId) from "testing" / "runtasklast" fields("userid", "testlogid", "lasttaskid", "lasttasknum")
    }
}

object ElasticTestRunDao {
  def apply: IElasticTestRunDao = new ElasticTestRunDao()
}