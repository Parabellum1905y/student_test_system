package testing.dao.tests

import java.time.LocalDateTime

import testing.dao.Db
import testing.model.tests._

import scala.concurrent.Future
import db.PostgresDriverPgExt.api._
import slick.dbio.DBIOAction
import testing.dao.usersDAO.GroupsDao

import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.Random

case class MixedTask(taskId:Long, orderNum:Int)

/**
  * Created by luger on 09.06.16.
  */
trait ITasksDao extends Db with xitrum.Log {
  lazy val tasks = Tasks.get
  lazy val tests = Tests.get
  lazy val testsLog = TestsLog.get
  lazy val taskLog = TaskLog.get
  lazy val answerLog = AnswersLog.get
  lazy val testDao = TestsDao
  lazy val groupsDao = GroupsDao

  def create (task:TasksRow, ownerId:Long):Future[Option[TasksRow]]
  def update (task:TasksRow, ownerId:Long):Future[Option[TasksRow]]
  def delete (taskId:Long, testId:Long, ownerId:Long):Future[Int]
  def listAll:Future[Seq[TasksRow]]
  def get (id:Long*):Future[Seq[TasksRow]]
  def get (id:Long):Future[TasksRow]
  def getById (id:Long):Future[TasksRow]
  def getForTest (testId:Long):Future[Seq[TasksRow]]
  def getForTest (testId:Long, userId:Long):Future[Seq[TasksRow]]
  def getForTest (taskId:Long, testId:Long, userId:Long):Future[Seq[TasksRow]]

  /**
    * return Task by order num. Order numbers generates
    * after user selected test and stored in elastic search
    * @param testId
    * @param num
    * @return
    */
  def getByNum (testId:Long, num:Int):Future[Option[TasksRow]]
  def isAllCompleted (testLogId:Long):Future[Boolean]
  def mixedArrayOfTasks(testId:Long, shuffle:Boolean):Future[List[MixedTask]]
}

class TasksDao extends ITasksDao {
  def createTaskQuery (task: TasksRow) =
    (tasks returning tasks.map(_.taskid)
      into ((task,id) => task.copy (taskid = id))) += task

  override def create(task: TasksRow, ownerId:Long): Future[Option[TasksRow]] =
    db.run {
      //val testIdQuery =
      (for {
        found <- tests.filter(t => t.creatorid === ownerId && t.testid === task.testid).exists.result
        action <- found match {
          case false => DBIO.successful(Option.empty[TasksRow])
          case _ =>
            createTaskQuery(task).map(Option(_))
        }
      }yield action).transactionally
    }

  override def update (task:TasksRow, ownerId:Long):Future[Option[TasksRow]] =
    db.run {
      for {
        qupdate <- {
          val found = tests.filter(t => t.creatorid === ownerId && t.testid === task.testid).exists
          tasks.filter(t => t.taskid === task.taskid && found).
            map(t=>(
              t.theme,
              t.title,
              t.task,
              t.jumptaskfine,
              t.taskpoint,
              t.taskfine,
              t.randomanswers,
              t.multianswer,
              t.testexectime)).
            update((
              task.theme,
              task.title,
              task.task,
              task.jumptaskfine,
              task.taskpoint,
              task.taskfine,
              task.randomanswers,
              task.multianswer,
              task.testexectime
              ))
        }
        qselect <- qupdate match {
          case 0 => DBIO.successful(Option.empty[TasksRow])
          case _ => tasks.filter(_.taskid === task.taskid).result.map(_.headOption)
        }
      } yield qselect
    }

  override def delete (taskId:Long, testId:Long, ownerId:Long):Future[Int] =
    db.run {
      val found = tests.filter(t => t.creatorid === ownerId && t.testid === testId).exists
      tasks.filter(t => t.taskid === taskId && found).delete
    }


  override def get(id: Long*): Future[Seq[TasksRow]] =
    db.run{
      tasks.filter (_.taskid inSet id).result
    }

  override def get(id: Long): Future[TasksRow] =
    get (Seq(id):_*).map(_.head)

  def getById (id:Long):Future[TasksRow] = get(id)

  override def listAll: Future[Seq[TasksRow]] =
    db.run{
      tasks.result
    }

  override def getForTest(testId: Long): Future[Seq[TasksRow]] =
    db.run {
      tasks.filter(_.testid === testId).result
    }

  override def getForTest(testId: Long, userId: Long): Future[Seq[TasksRow]] =
    db.run {
      val userGroupsQuery = groupsDao.recursiveGroupsForUser(userId) map (_.map(_._1))
      val query = for {
        groupsId <- userGroupsQuery
        retTests <- {
          val access = testDao.searchAccessQuery(userId, testId, groupsId)
          val testQuery = tests.filter(t => (t.testid === testId && (t.testid in access))||
                          (t.testid === testId && t.creatorid === userId)).map(_.testid)
          tasks.filter(_.testid in testQuery).result
        }
      } yield retTests
      query
    }

  def blaTest(userId:Long) =
    db.run {
      groupsDao.recursiveGroupsForUser(userId) map (_.map(_._1))
    }

  def blaTest2(testId:Long, userId:Long) =
    db.run {
      val userGroupsQuery = List(1L,2l,3l,4l,5l,6l,7l,8l,9l,10l)
      testDao.searchAccessQuery(userId, testId, userGroupsQuery).result
    }

  def blaTest3(taskId:Long, testId:Long, userId:Long) =
    db.run {
      val userGroupsQuery = List(1L,2l,3l,4l,5l,6l,7l,8l,9l,10l)
      tasks.filter(t => t.taskid === taskId && (t.testid inSet userGroupsQuery)).result
    }

  override def getForTest (taskId:Long, testId:Long, userId:Long):Future[Seq[TasksRow]] =
    db.run {
      val userGroupsQuery = groupsDao.recursiveGroupsForUser(userId) map (_.map(_._1))
      val query = for {
        groupsId <- userGroupsQuery
        retTests <- {
          val access = testDao.searchAccessQuery(userId, testId, groupsId)
          val testQuery = tests.filter(t => (t.testid in access)||
            (t.testid === testId && t.creatorid === userId)).map(_.testid)
          tasks.filter(t => t.taskid === taskId && (t.testid in testQuery)).result
        }
      } yield retTests
      query
    }


  override def getByNum (testId:Long, num:Int):Future[Option[TasksRow]] = {
    val q = tasks.filter(_.testid === testId).sortBy(_.taskid).drop(num-1).take(1)
    db.run (q.result).map(_.headOption)
  }
  def isAllCompleted (testLogId:Long):Future[Boolean] =
    db.run {
      /*val existsAnswers = answerLog.
        filter(a => a.testlogid === testLogId).map(_.taskid)*/
      val haveSeen = taskLog.
        filter(t=> t.testlogid === testLogId && t.seen >= 1.toShort).
        map(_.taskid)
      val testId = testsLog.filter(_.testlogid === testLogId).map(_.testid)
      tasks.filter(t=> /*!(t.taskid in existsAnswers) &&*/ !(t.taskid in haveSeen) && (t.testid in testId)).
        exists.result.map(!_)
    }

  def mixedArrayOfTasks(testId:Long, shuffle:Boolean):Future[List[MixedTask]] =
    db.run {
      tasks.filter(_.testid === testId).map(_.taskid).result
    }.map {seq =>
      def mixUp (ln:List[MixedTask], l:List[Long], n:Int): List[MixedTask] = l match {
        case Nil =>
          ln
        case head :: tail =>
          mixUp (ln ::: List(MixedTask (head, n+1)), tail, n+1)
        }
      if (shuffle)
        mixUp(Nil, Random.shuffle(seq).toList, 0)
      else
        mixUp(Nil, seq.toList.sorted, 0)
    }

}

object TasksDao{
  def apply: TasksDao = new TasksDao()
}

object TaskHelper {
  implicit class TaskExtended[A<:ITasksDao](task:TasksRow)(implicit dao:A, ownerId:Long) {
    def create = dao.create(task, ownerId)
    def update = dao.update(task, ownerId)
  }
}

object Test {
  def test = {
    import TaskHelper._
    implicit val dao = TasksDao.apply
    implicit val ownerId = 8L
    TasksRow(-1L, 1L, Option(""), Option(""), "", Option(0)).create
  }
}

