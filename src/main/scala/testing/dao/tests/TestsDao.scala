package testing.dao.tests

import java.time.LocalDateTime

import testing.dao.Db
import testing.model.tests._
import testing.model.users._
import db.PostgresDriverPgExt.api._
import testing.dao.usersDAO.GroupsDao

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

/**
  * Created by luger on 31.05.16.
  */
case class UserPoint(fineForSkip: Short,
                     skippedSumFine: Int,
                     emptyTasksFine: Int,
                     failedTasksFine: Int,
                     points: Int,
                     fullPoint : Int )

trait ITestsDao extends Db with xitrum.Log {
  lazy val tests = Tests.get
  lazy val testAccess = TestsAccess.get
  lazy val taskLogs = TaskLog.get
  lazy val answerLog = AnswersLog.get
  lazy val answers = Answers.get
  lazy val users = UsersObj.get
  lazy val groups = UserGroups.get
  lazy val groupsDao = GroupsDao
  lazy val testsLog = TestsLog.get
  lazy val tasks = Tasks.get

  val elasticTest = ElasticTestDao.apply
  /*lazy val groups = UserGroups.get
  lazy val groupsRel = GroupRel.get
  lazy val users = UsersObj.get*/

  /**
    * create test row
    *
    * @param testsRow test row for create
    * @return
    */
  def createTest (testsRow: TestsRow):Future[TestsRow]

  /**
    * create test row
    *
    * @param testsRow test row for update
    * @return
    */
  def updateTest (testsRow: TestsRow):Future[Option[TestsRow]]

  /**
    * delete test row
    *
    * @return
    */
  def deleteTest (testId:Long, creatorId:Long):Future[Int]

  /**
    * get test for ID sequence
    *
    * @param id
    * @return
    */
  def get (id:Long*):Future[Seq[TestsRow]]

  /**
    * get test for ID
    *
    * @param id
    * @return
    */
  def get (id:Long):Future[Option[TestsRow]]

  def listAll :Future[Seq[TestsRow]]
  /**
    * get test for ID sequence
    *
    * @param id
    * @return
    */
  def getMine (ownerId:Long, id:Long*):Future[Seq[TestsRow]]

  /**
    * get test for ID
    *
    * @param id
    * @return
    */
  def getMine (id:Long, ownerId:Long):Future[Option[TestsRow]]

  /**
    * search tests created by user(UserId)
    *
    * @param userId
    * @return
    */
  def searchUsersTests (userId:Long):Future[Seq[TestsRow]]

  /**
    * search tests reated by user(UserId)
    *
    * @param userId
    * @return
    */
  def searchAccessedTestsForUser (userId:Long):Future[Seq[TestsRow]]

  /**
    * search tests reated by user(UserId)
    *
    * @param userId
    * @return
    */
  def getAccessedTestForUser (userId:Long, testId:Long):Future[Option[TestsRow]]

  /**
    * get tests accessed for user
    * searhing in groups and users
    *
    * @param testId
    * @return
    */
  def searchUsersAccessedForTest (testId:Long):Future[Seq[User]]
  /**
    * get tests accessed for user
    * searhing in groups and users
    *
    * @param testId
    * @return
    */
  def searchGroupsAccessedForTest (testId:Long):Future[Seq[UserGroupsRow]]
  /**
    * set tests accessed for user/group
    * searhing in groups and users
    *
    * @param objectId
    * @return
    */
  def setObjectAccessForTest (testId: Long, objectId : Long, memberType:Int, ownerId:Long,
                            read:Int, write:Int, exec:Int):Future[Option[TestsAccessRow]]
  def delUserAccessForTest (testId: Long, objectId : Long, memberType:Int, ownerId:Long):Future[Int]

  /**
    * get all accesses for test
    *
    * @param testId
    * @return
    */
  def getAccessesFor (testId:Long):Future[(Seq[(TestsAccessRow, User)],
                                           Seq[(TestsAccessRow, UserGroupsRow)])]

  def startTest (testId:Long, userId:Long, fullPoint:Int, maxTimeToExec:Int):Future[TestsLogRow]
  def finishTest (testLogId:Long, userId:Long):Future[Int]
  def setAnswer (answerList:List[Long], testlogid:Long, taskId:Long):Future[(Seq[AnswersLogRow], Option[TaskLogRow])]
  def skipAnswer (testlogid:Long, taskId:Long):Future[(Seq[AnswersLogRow], Option[TaskLogRow])]
  def analyzeUserPoint (test: Seq[TestsRow],
                        tasks: Seq[TasksRow],
                        rans: Seq[AnswersRow],
                        failed: Seq[AnswersLogRow],
                        empty: Seq[TasksRow],
                        skipped: Seq[TaskLogRow]):UserPoint
  def selectAnalyzeUserPoint(testLogId:Long, userId:Long):Future[UserPoint]
  def getNotAnswered (testlogid:Long): Future[Seq[TasksRow]]
  def sumFullPoint (testId:Long):Future[Option[Short]]
  def sumTime (testId:Long): Future[Int]
  def getForLogId (logId:Long):Future[Option[TestsRow]]
  def getRunnedForUser (userId:Long): Future[Seq[(TestsLogRow, TestsRow, User)]]
  def getForReport (testlogid:Long, userId:Long): Future[Seq[(TestsLogRow, TestsRow, User, User)]]
  def loadLeaderShip: Future[Seq[((Long, Int, Int), User)]]
}



object TestsDao extends ITestsDao with TestDaoHelper{

  def createTestQuery (testsRow: TestsRow) =
    (tests returning tests.map(_.testid)
      into ((testsRow,id) => testsRow.copy (testid = id))) += testsRow
  /**
    * create test row
    *
    * @param testsRow
    * @return
    */
  override def createTest(testsRow: TestsRow): Future[TestsRow] =
    db.run(createTestQuery(testsRow).map{test =>
      elasticTest.createIndex(test)
      test
    })

  /**
    * get test for ID sequence
    *
    * @param id
    * @return
    */
  override def get(id: Long*): Future[Seq[TestsRow]] =
    db.run (tests.filter(t => (t.testid inSet id) && t.activecopy === 1.toShort).result)

  /**
    * get test for ID
    *
    * @param id
    * @return
    */
  override def get(id: Long): Future[Option[TestsRow]] = get (Seq(id):_*).map(_.headOption)

  /**
    * get test for ID sequence
    *
    * @param id
    * @return
    */
  override def getMine(ownerId:Long, id: Long*): Future[Seq[TestsRow]] =
    db.run (tests.filter(t => (t.testid inSet id) && t.activecopy === 1.toShort && t.creatorid === ownerId).result)

  /**
    * get test for ID
    *
    * @param id
    * @return
    */
  override def getMine(id: Long, ownerId:Long): Future[Option[TestsRow]] = getMine (ownerId, Seq(id):_*).map(_.headOption)
  /**
    * search tests reated by user(UserId)
    *
    * @param userId
    * @return
    */
  override def searchUsersTests(userId: Long): Future[Seq[TestsRow]] =
    db.run (tests.filter(t => t.creatorid === userId && t.activecopy === 1.toShort).result)

  /**
    * get tests accessed for user
    * searhing in groups and users
    *
    * @param testId
    * @return
    */
  override def searchGroupsAccessedForTest(testId: Long): Future[Seq[UserGroupsRow]] = ???

  /**
    * get tests accessed for user
    * searhing in groups and users
    *
    * @param testId
    * @return
    */
  override def searchUsersAccessedForTest(testId: Long): Future[Seq[User]] = ???

  /**
    * set tests accessed for user/group
    * searhing in groups and users
    *
    * @param objectId
    * @return
    */
  override def setObjectAccessForTest(testId: Long,
                                      objectId: Long,
                                      memberType: Int,
                                      ownerId: Long,
                                      read: Int,
                                      write: Int,
                                      exec: Int): Future[Option[TestsAccessRow]] =
    db.run {
      val query = for {
        isOwner: Boolean <- tests.filter(t => t.creatorid === ownerId && t.testid === testId).map(_ => 1).exists.result
        isFound: Boolean <- testAccess.
          filter(t => t.testid === testId && t.write === write.toShort &&
                t.memberid === memberType.toShort && t.subjectid === objectId).map(_ => 1).exists.result
        /*action <- testAccess
        posibility: Seq[Int] <- groupsRel.filter(_ => !exists).map(_ => 1).take(1).result*/
        action <- (isOwner, isFound) match {
          case (false, _)|(_, true) => DBIO.successful(Option.empty[TestsAccessRow])
          case _ =>
            val tar = TestsAccessRow(testId, memberType.toShort, objectId,
              read.toShort, write.toShort, exec.toShort, 0.toShort)
            (testAccess returning testAccess.map(_.testid)
              into ((tar, id) => Option(tar.copy(testid = id)))) += tar
        }
      }yield action
      query.transactionally
    }

  override def delUserAccessForTest(testId: Long,
                                    objectId: Long,
                                    memberType: Int,
                                    ownerId: Long): Future[Int] =
    db.run {
      val checkOwner = tests.filter(t => t.creatorid === ownerId && t.testid === testId).map(_.testid).take(1)
      val query =
        testAccess.filter(t =>
          t.memberid === memberType.toShort && t.subjectid === objectId && (t.testid in checkOwner))
      query.delete
    }

  def searchAccessQuery [A<:Seq[Long]](userId: Long, testId:Long, groupsId:A) =
    testAccess.filter { acc =>
      acc.testid === testId && acc.memberid === 1.toShort && acc.subjectid === userId || (
        acc.memberid === 2.toShort && acc.subjectid.inSet(groupsId))
    }.map(_.testid)

  def searchAccessQuery [A<:Seq[Long]](userId: Long, groupsId:A) =
    testAccess.filter { acc =>
      acc.memberid === 1.toShort && acc.subjectid === userId || (
        acc.memberid === 2.toShort && acc.subjectid.inSet(groupsId))
    }.map(_.testid)
  /**
    * search tests reated by user(UserId)
    *
    * @param userId
    * @return
    */
  override def searchAccessedTestsForUser(userId: Long): Future[Seq[TestsRow]] =
    db.run {
      val userGroupsQuery = groupsDao.recursiveGroupsForUser(userId) map (_.map(_._1))
      val query = for {
        groupsId <- userGroupsQuery
        retTests <- {
          val access = searchAccessQuery(userId, groupsId)
          tests.filter(_.testid in access).result
        }
      } yield retTests
      query.transactionally
    }

  override def getAccessedTestForUser (userId:Long, testId:Long):Future[Option[TestsRow]] =
    db.run {
      val userGroupsQuery = groupsDao.recursiveGroupsForUser(userId) map (_.map(_._1))
      val query = for {
        groupsId <- userGroupsQuery
        retTests <- {
          val access = searchAccessQuery(userId, testId, groupsId)
          tests.filter(t => t.testid === testId && (t.testid in access)).result
        }
      } yield retTests
      query.transactionally
    }.map(_.headOption)
  /**
    * create test row
    *
    * @param testsRow test row for update
    * @return
    */
  override def updateTest(testsRow: TestsRow): Future[Option[TestsRow]] =
    db.run {
      val query = for {
        qupdate <-tests.filter(t => t.testid === testsRow.testid && t.creatorid === testsRow.creatorid).
          map(t=>(
            t.testname,
            t.testdescription,
            t.lastmodifieddate,
            t.randomtasks,
            t.randomanswers,
            t.posibilitytojumptask,
            t.jumptaskfine,
            t.testexectime,
            t.maxtimepertask,
            t.usetaskedtime,
            t.allowviewdetailedresults,
            t.allowhideresult)).
          update((
            testsRow.testname,
            testsRow.testdescription,
            LocalDateTime.now,
            testsRow.randomtasks,
            testsRow.randomanswers,
            testsRow.posibilitytojumptask,
            testsRow.jumptaskfine,
            testsRow.testexectime,
            testsRow.maxtimepertask,
            testsRow.usetaskedtime,
            testsRow.allowviewdetailedresults,
            testsRow.allowhideresult
            ))
        qselect <- qupdate match {
          case 0 => DBIO.successful(Option.empty[TestsRow])
          case _ => tests.filter(_.testid === testsRow.testid).result.map(_.headOption)
        }
      } yield qselect
      query.map{test =>
        if (test.isDefined) elasticTest.updateIndex(test.get)
        test
      }.transactionally
    }

  override def listAll: Future[Seq[TestsRow]] = db.run(tests.result)

  /**
    * delete test row
    *
    * @return
    */
  override def deleteTest(testId: Long, creatorId: Long): Future[Int] =
    db.run {
      tests.filter(t=>t.creatorid === creatorId && t.testid === testId).delete
    }.map{x =>
      if (x > 0) elasticTest.deleteIndex(testId)
      x
    }

  override def getAccessesFor(testId: Long):Future[(Seq[(TestsAccessRow, User)],
    Seq[(TestsAccessRow, UserGroupsRow)])] =
    db.run {
      val queryUsers = testAccess.filter(_.testid === testId).join(users).on {(x, y) =>
        x.memberid === 1.toShort && x.subjectid === y.userId
      }
      val queryGroups = testAccess.filter(_.testid === testId).join(groups).on {(x, y) =>
        x.memberid === 2.toShort && x.subjectid === y.groupid
      }
      val query = for {
        usersAccess <- queryUsers.result
        groupsAccess<- queryGroups.result
      }yield {
        (usersAccess, groupsAccess)
      }
      query
    }

  def sumFullPoint (testId:Long):Future[Option[Short]] = {
    val q = tasks.filter(_.testid === testId).map(_.taskpoint).sum
    db.run(q.result)
  }

  def sumTime (testId:Long): Future[Int] = {
    val q1 = tasks.filter(_.testid === testId).map(_.testexectime).sum
    val q2 = tests.filter(_.testid === testId).map(_.testexectime)
    val q = for {
      time1: Option[Int] <- q1.result
      time2: Option[Int] <- q2.result.head
    } yield {
      (time1, time2) match {
        case (None, None) => 0
        case (None, _) => time2.get
        case (_, None) => time1.get
        case (Some(t1), Some(t2)) =>
          if (t1>t2)
            t1
          else
            t2
      }
    }
    db.run(q)
  }

  def startTest (testId:Long, userId:Long, fullPoint:Int, maxTimeToExec:Int):Future[TestsLogRow] =
    db.run{
      val testLog = TestsLogRow(-1L, testId, LocalDateTime.now, LocalDateTime.now, 0, fullPoint, userId)
      (testsLog returning testsLog.map(_.testlogid)
      into ((testLog,id) => testLog.copy (testlogid = id))) += testLog
    }

  def analyzeUserPoint (test: Seq[TestsRow],
                        tasks: Seq[TasksRow],
                        rans: Seq[AnswersRow],
                        failed: Seq[AnswersLogRow],
                        empty: Seq[TasksRow],
                        skipped: Seq[TaskLogRow]):UserPoint = {
    val t = test.head
    val fineForSkip: Short = t.jumptaskfine.getOrElse(0)
    log info s"fineForSkip:$fineForSkip"
    val skippedSumFine: Int = skipped.size * fineForSkip
    log info s"skippedSumFine:$skippedSumFine"
    val emptyTasksFine: Int = empty.map(_.taskfine.toInt).sum
    log info s"emptyTasksFine:$emptyTasksFine"
    log info s"rans:$rans"
    log info s"failed:$failed"
    val failedAnswers = rans.filter(r => failed.map(_.answerid).contains(r.answerid))
    log info s"failedAnswers:$failedAnswers"
    val failedTasksFine: Int = tasks.filter(t => failed.map(_.taskid).contains(t.taskid)).
      map(_.taskfine.toInt).sum
    log info s"failedTasksFine:$failedTasksFine"
    log info s"$empty"
    val points: Int = tasks.filter(t => !empty.map(_.taskid).contains(t.taskid)).
      filter(t => !failed.map(_.taskid).contains(t.taskid)).map(_.taskpoint.toInt).sum
    log info s"points:$points"
    val fullPoint : Int = tasks.map(_.taskpoint.toInt).sum
      //rans.filter(r => empty.map(_.taskid).contains())
    UserPoint(fineForSkip, skippedSumFine, emptyTasksFine, failedTasksFine, points, fullPoint )
  }

  def selectAnalyzeUserPoint(testLogId:Long, userId:Long):Future[UserPoint] =
    db.run {
      val q = testsLog.filter(_.testlogid === testLogId)
      val getTest = tests.filter(_.testid in q.map(_.testid))
      val testTasks = tasks.filter(_.testid in getTest.map(_.testid))
      val rightAnswers = answers.filter(a => a.isright === 1.toShort && (a.taskid in testTasks.map(_.taskid)))
      /**
        * invalid answers
        */
      val userFailedTasks = answerLog.
        filter(al => al.testlogid === testLogId && (al.taskid in testTasks.map(_.taskid)) &&
          !(al.answerid in rightAnswers.map(_.answerid)))
      /**
        * there's not answer
        */
      val userEmptyAnswers = testTasks.
        filter(t => !(t.taskid in taskLogs.filter(_.testlogid === testLogId).map(_.taskid)))
      /**
        * fine for skipping question
        */
      val jumpedFines = taskLogs.filter(tl => tl.testlogid === testLogId && tl.seen > 1.toShort)
      (for {
        test: Seq[TestsRow] <- getTest.result
        tasks: Seq[TasksRow] <- testTasks.result
        rans: Seq[AnswersRow] <- rightAnswers.result
        failed: Seq[AnswersLogRow] <- userFailedTasks.result
        empty: Seq[TasksRow] <- userEmptyAnswers.result
        skipped: Seq[TaskLogRow] <- jumpedFines.result
        userPoint: UserPoint <- DBIO.successful {
          analyzeUserPoint(test, tasks, rans, failed, empty, skipped)
        }
      } yield userPoint).transactionally
    }

  def finishTest (testLogId:Long, userId:Long):Future[Int] =
    for {
      userPoint <- selectAnalyzeUserPoint(testLogId, userId)
      upd <- db.run {
        val q = testsLog.filter(_.testlogid === testLogId)
        q.map(t => (t.finishdatetime, t.finished, t.userpoint)).
          update((LocalDateTime.now, 1.toShort, userPoint.points))
      }
    } yield upd

  def getNotAnswered (testlogid:Long): Future[Seq[TasksRow]]  =
    db.run {
      val jump = tests.filter(t => t.posibilitytojumptask === 1.toShort &&
        (t.testid in testsLog.filter(_.testlogid === testlogid).map(_.testid))).
        map(_.posibilitytojumptask).exists
      val tnotanswered = tasks.filter ( t =>
        /*(t.taskid in taskLogs.
          filter(
            t => t.testlogid === testlogid && t.seen > 1.toShort && jump).map(_.taskid)) ||*/
          !(
          t.taskid in taskLogs.
            filter(t => t.testlogid === testlogid).map(_.taskid)
          )
      )
      tnotanswered.result
    }

  def setAnswer (answerList:List[Long], testlogid:Long, taskId:Long):Future[(Seq[AnswersLogRow], Option[TaskLogRow])] =
    db.run {
      for {
        tlogseq: Seq[TaskLogRow] <- getTasksLogSeqQuery(testlogid, taskId).result

        jump: Option[Short] <- checkAbilityToSkipTaskQuery(testlogid).result.map(_.map(_.getOrElse(0.toShort)).headOption)
        tlog:Option[TaskLogRow] <- tlogseq match {
          case Nil =>
            val tl = TaskLogRow (-1L , taskId, testlogid, seen = 1.toShort, seendate = LocalDateTime.now)
            (taskLogs returning taskLogs.map(_.tasklogid)
              into ((tl,id) => Option(tl.copy (tasklogid = id)))) += tl
          case _   =>
            val head = tlogseq.head
            if (jump.contains(1.toShort)){
              for {
                qupd: Int <- taskLogs.filter(_.tasklogid === head.tasklogid).map{
                  t=> (t.seen, t.seendate)
                }.update(
                  ((head.seen +1).toShort, LocalDateTime.now))
                qsel:Option[TaskLogRow] <- qupd match {
                  case 0 => DBIO.successful(Option(head))
                  case _ => taskLogs.filter(_.tasklogid === head.tasklogid).result.headOption
                }
              }yield qsel
            }else {
              DBIO.successful(Option(head))
            }
        }
        foundAnswers: Seq[AnswersLogRow] <- answerLog.
          filter(a => a.taskid === taskId && a.testlogid === testlogid).result
        newAnswers  <- newAnswersSave (testlogid, taskId, tlog, jump, foundAnswers, answerList)
      }yield (newAnswers, tlog)

    }

  def skipAnswer (testlogid:Long, taskId:Long):Future[(Seq[AnswersLogRow], Option[TaskLogRow])] =
    db.run {
      for {
        tlogseq: Seq[TaskLogRow] <- getTasksLogSeqQuery(testlogid, taskId).result

        jump: Option[Short] <- checkAbilityToSkipTaskQuery(testlogid).result.map(_.map(_.getOrElse(0.toShort)).headOption)
        tlog:Option[TaskLogRow] <- tlogseq match {
          case Nil =>
            val tl = TaskLogRow (-1L , taskId, testlogid, seen = 1.toShort, seendate = LocalDateTime.now)
            (taskLogs returning taskLogs.map(_.tasklogid)
              into ((tl,id) => Option(tl.copy (tasklogid = id)))) += tl
          case _   =>
            val head = tlogseq.head
            if (jump.contains(1.toShort)){
              for {
                qupd: Int <- taskLogs.filter(_.tasklogid === head.tasklogid).map{
                  t=> (t.seen, t.seendate)
                }.update(
                  ((head.seen +1).toShort, LocalDateTime.now))
                qsel:Option[TaskLogRow] <- qupd match {
                  case 0 => DBIO.successful(Option(head))
                  case _ => taskLogs.filter(_.tasklogid === head.tasklogid).result.headOption
                }
              }yield qsel
            }else {
              DBIO.successful(Option(head))
            }
        }
        foundAnswers: Seq[AnswersLogRow] <- answerLog.
          filter(a => a.taskid === taskId && a.testlogid === testlogid).result
      }yield (foundAnswers, tlog)

    }

  def fullSartTest(testId:Long, userId:Long): Future[Option[TestsLogRow]] = {
    val seq = for {
      fullPoint: Option[Short] <- sumFullPoint(testId)
      fullTime  <- sumTime(testId)
      access    <- getAccessedTestForUser(userId, testId)
      result    <- {
        access match {
          case None => Future{
            Option.empty[TestsLogRow]
          }
          case _    => startTest(testId, userId, fullPoint.getOrElse(0.toShort).toInt, fullTime).map(Option(_))
        }
      }
    } yield result
    seq
  }
  //def finishTest (testLogId:Long, userId:Long) =
  def getForLogId (logId:Long):Future[Option[TestsRow]] = {
    val q = testsLog.filter(_.testlogid === logId).map(_.testid)
    val q2 = tests.filter(_.testid in q)
    db.run(q2.result)
  }.map(_.headOption)

  def getRunnedForUser (userId:Long): Future[Seq[(TestsLogRow, TestsRow, User)]] = {
    val q = testsLog.filter(_.userid === userId).join(tests).on{(x, y) =>
      x.testid === y.testid
    }.join (users).on {(x, y) =>
      x._2.creatorid === y.userId
    }.map(x => (x._1._1,x._1._2, x._2))
    db.run(q.result)
  }

  def getForReport (testlogid:Long, userId:Long): Future[Seq[(TestsLogRow, TestsRow, User, User)]] = {
    val q = testsLog.filter(_.userid === userId).join(tests).on{(x, y) =>
      x.testid === y.testid
    }.join (users).on {(x, y) =>
      x._2.creatorid === y.userId
    }.join (users).on {(x, y) =>
      x._1._1.userid === y.userId
    }.map(x => (x._1._1._1,x._1._1._2, /*creator*/x._1._2, x._2))
    db.run(q.result)
  }

  def loadLeaderShip: Future[Seq[((Long, Int, Int), User)]] =
    db.run {
      testsLog.groupBy(x => x.userid).map {
        case (userid, log) =>
          (userid, log.map (_.userpoint).sum.getOrElse(0), log.map (_.fullpoint).sum.getOrElse(0))
      }.join (users).on{(x, y) =>
        x._1 === y.userId
      }.result
    }
}

trait TestDaoHelper {
  this:ITestsDao=>

  def getTasksLogSeqQuery(testLogIg:Long, taskId:Long) = taskLogs.
    filter(t => t.testlogid === testLogIg && t.taskid === taskId)

  def checkAbilityToSkipTaskQuery (testLogIg:Long) = tests.
    filter(_.testid in testsLog.filter(_.testlogid === testLogIg).map(_.testid)).
    map(_.posibilitytojumptask)

  def newAnswersSave(testLogIg:Long,
                     taskId:Long,
                     tlog:Option[TaskLogRow],
                     jump: Option[Short],
                     foundAnswers: Seq[AnswersLogRow],
                     answerList:List[Long]) = {
    val seen: Int = if (tlog.isDefined) tlog.get.seen else 0
    val skipped: Int = jump.getOrElse(0.toShort).toInt
    (seen, skipped) match {
      case (xx, 0) if xx > 1 => DBIO.successful(foundAnswers)
      case (xx, 1)  if xx > 1 =>
        for {
          xdel <- answerLog.
            filter(a => (a.answerid inSet foundAnswers.map (_.answerid)) && a.testlogid === testLogIg).
            delete
          xins <- answerLog ++= answerList.map (fa => AnswersLogRow(fa, testLogIg, taskId, answer = ""))
          xsel: Seq[AnswersLogRow] <- answerLog.
            filter(a => a.taskid === taskId && a.testlogid === testLogIg).result
        }yield xsel
      case (1, 0) | (1, 1)=>
        for {
          xins <- answerLog ++= answerList.map (fa => AnswersLogRow(fa, testLogIg, taskId, answer = ""))
          xsel: Seq[AnswersLogRow] <- answerLog.
            filter(a => a.taskid === taskId && a.testlogid === testLogIg).result
        }yield xsel

    }
  }
}
