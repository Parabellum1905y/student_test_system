package testing.dao.tests

import elastic.ElasticConf
import com.sksamuel.elastic4s._
import com.sksamuel.elastic4s.ElasticDsl._
import org.elasticsearch.action.update.UpdateResponse
import org.elasticsearch.index.query.MatchQueryBuilder
import testing.model.tests.TestsRow

import scala.concurrent.Future

/**
  * Created by luger on 21.05.16.
  */
trait IElasticTestDao extends xitrum.Log{
  val conf = ElasticConf.apply
  val client = ElasticClient.transport(ElasticsearchClientUri("elasticsearch://"+conf.host+":"+conf.port))

  def createIndex (test:TestsRow): Future[IndexResult]
  def updateIndex (test:TestsRow): Future[UpdateResponse]
  def deleteIndex (testId:Long)
  def getById (id:Long) : Future[RichGetResponse]
  def searchSuggestionsTest (pattern:String): Future[RichSearchResponse]
  def searchTest (pattern:String): Future[RichSearchResponse]
  def searchMatchTest (pattern:String): Future[RichSearchResponse]
}

class ElasticTestDao extends IElasticTestDao{
  def createIndex (test:TestsRow): Future[IndexResult] =
    client.execute {
      index into "testing" / "tests" id test.testid fields (
        ("name", test.testname),
        ("description", test.testdescription.getOrElse(""))
        )
    }

  def updateIndex (test:TestsRow): Future[UpdateResponse] =
    client.execute {
      update (test.testid) in "testing" / "tests" docAsUpsert (
        ("name", test.testname),
        ("description", test.testdescription.getOrElse(""))
        )
    }

  def deleteIndex (testId:Long) =
    client.execute {
      delete (testId) from "testing" / "tests"
    }

  def getById (id:Long) =
    client execute {
      get id id from "testing" / "tests"
    }

  def searchSuggestionsTest (pattern:String): Future[RichSearchResponse] = {
    val mysugg = termSuggestion("mysugg").field("name").text(pattern)
    client execute {
      search in "testing/tests" suggestions {
        mysugg
      }
    }
  }

  def searchTest (pattern:String): Future[RichSearchResponse] =
    client execute {
      search in "testing/tests" query wildcardQuery("name", "*"+pattern+"*")
    }

  def searchMatchTest (pattern:String): Future[RichSearchResponse] =
    client execute {
      search in "testing/tests" query {
        multiMatchQuery(pattern).fields("name", "description") operator MatchQueryBuilder.Operator.OR
      }
    }
}

object ElasticTestDao {
  def apply: ElasticTestDao = new ElasticTestDao()
}