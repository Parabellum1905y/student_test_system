package testing.dao.tests

import testing.dao.Db
import testing.model.tests._

import scala.concurrent.Future
import db.PostgresDriverPgExt.api._
import slick.dbio.DBIOAction
import testing.dao.usersDAO.GroupsDao

import scala.concurrent.ExecutionContext.Implicits.global


/**
  * Created by luger on 09.06.16.
  */
trait IAnswerDao extends Db with xitrum.Log {
  lazy val tasks = Tasks.get
  lazy val tests = Tests.get
  lazy val testDao = TestsDao
  lazy val taskDao = TasksDao.apply
  lazy val answers = Answers.get
  lazy val answersLog = AnswersLog.get

  def create (answer:AnswersRow):Future[Option[AnswersRow]]
  def update (answer:AnswersRow):Future[Option[AnswersRow]]
  //TODO add checking for owner
  def delete (answerId:Long, taskId:Long, ownerId:Long):Future[Int]
  def listAll:Future[Seq[AnswersRow]]
  def get (id:Long*):Future[Seq[AnswersRow]]
  def get (id:Long):Future[AnswersRow]
  def getForTask (taskId:Long):Future[Seq[AnswersRow]]
  def getForTasks (taskId:Long*):Future[Seq[AnswersRow]]
  def getLogForTask (logId:Long, taskId:Long):Future[Seq[AnswersLogRow]]
  def getLogForTask(logId:Long, taskId: Long*):Future[Seq[AnswersLogRow]]
}

class AnswersDao extends IAnswerDao {
  def createTaskQuery (answer:AnswersRow) =
    (answers returning answers.map(_.answerid)
      into ((answer,id) => answer.copy (answerid = id))) += answer


  override def create(answer: AnswersRow): Future[Option[AnswersRow]] =
    db.run {
      (for {
        found <- answers.filter(a => a.answerid === answer.answerid).exists.result
        action <- found match {
          case true => DBIO.successful(Option.empty[AnswersRow])
          case _ =>
            createTaskQuery(answer).map(Option(_))
        }
      } yield action).transactionally
    }

  override def update(answer: AnswersRow): Future[Option[AnswersRow]] =
    db.run {
      for {
        qupdate <- {
          answers.filter(t => t.answerid === answer.answerid).
            map(t=>(
              t.answer,
              t.isright)).
            update((
              answer.answer,
              answer.isright
              ))
        }
        qselect <- qupdate match {
          case 0 => DBIO.successful(Option.empty[AnswersRow])
          case _ => answers.filter(_.answerid === answer.answerid).result.map(_.headOption)
        }
      } yield qselect
    }


  override def get(id: Long*): Future[Seq[AnswersRow]] =
    db.run{
      answers.filter (_.answerid inSet id).result
    }

  override def get(id: Long): Future[AnswersRow] =
    get (Seq(id):_*).map(_.head)

  override def getForTask(taskId: Long): Future[Seq[AnswersRow]] =
    db.run {
      answers.filter(_.taskid === taskId).result
    }

  override def getForTasks(taskId: Long*): Future[Seq[AnswersRow]] =
    db.run {
      answers.filter(_.taskid inSet taskId).result
    }

  override def getLogForTask(logId:Long, taskId: Long): Future[Seq[AnswersLogRow]] =
    db.run {
      answersLog.filter(al => al.taskid === taskId && al.testlogid === logId).result
    }

  override def getLogForTask(logId:Long, taskId: Long*): Future[Seq[AnswersLogRow]] =
    db.run {
      answersLog.filter(al => (al.taskid inSet taskId) && al.testlogid === logId).result
    }

  //TODO add checking for owner
  override def delete(answerId: Long, taskId: Long, ownerId:Long): Future[Int] =
    db.run {
      val found = tests.filter(t => t.creatorid === ownerId && (t.testid in
        tasks.filter(_.taskid === taskId).map(_.testid)
        )).exists
      answers.filter(t => t.answerid === answerId && found).delete
    }

  override def listAll: Future[Seq[AnswersRow]] =
    db.run{
      answers.result
    }
}

object AnswersDao{
  def apply: AnswersDao = new AnswersDao()
}

object AnswerHelper {
  implicit class TaskExtended[A<:IAnswerDao](answer:AnswersRow)(implicit dao:A) {
    def create = dao.create(answer)
    def update = dao.update(answer)
    //def delete = dao.delete(answer.answerid)
  }
}

