package testing.dao

import db.Crypt
import db.dbpoolconf.Datasource

/**
  * Created by luger on 26.02.16.
  */
trait Db {
  implicit val db = Datasource("test").database
  val crypt = Crypt
  def uuid = java.util.UUID.randomUUID.toString
}

object Db extends Db
