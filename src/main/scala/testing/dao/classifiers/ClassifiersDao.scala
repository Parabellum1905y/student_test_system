package testing.dao.classifiers


import testing.dao.Db
import db.PostgresDriverPgExt.api._
import testing.model.classifiers.{Languages, LanguagesRow}

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global
/**
  * Created by luger on 02.06.16.
  */
object ClassifiersDao extends Db with xitrum.Log {
  lazy val langs = Languages.get

  def getByLocale (locale:String): Future[Option[LanguagesRow]] =
    db.run (langs.filter(_.langlocale === locale).result.map(_.headOption))

  def getById (id:Long): Future[Option[LanguagesRow]] =
    db.run (langs.filter(_.langid === id).result.map(_.headOption))

  def getByCode (code:String): Future[Option[LanguagesRow]] =
    db.run (langs.filter(_.langcode === code).result.map(_.headOption))
}
