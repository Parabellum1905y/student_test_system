package quickstart.action

import xitrum.FutureAction

trait DefaultHelloLayout extends FutureAction {
  beforeFilter{
    if (sessiono[Long]("langId").isEmpty) {
      session("langId") = 1L
    }
    language = "ru"
  }

  override def layout = renderViewNoLayout[DefaultHelloLayout]()
}
