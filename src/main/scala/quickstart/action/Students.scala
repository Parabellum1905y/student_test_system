package quickstart.action

import xitrum.annotation.GET

/**
  * Created by luger on 27.04.16.
  */
@GET("hellostudents")
class Students extends DefaultHelloLayout{
  def execute () = respondView()
}
