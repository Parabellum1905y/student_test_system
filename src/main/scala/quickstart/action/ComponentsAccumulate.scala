package quickstart.action

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

/**
  * Created by luger on 06.03.16.
  */
object ComponentsAccumulate {

  def accum[A<:FutureComponent] (comps:(String, A)*):Future[Seq[(String, String)]] = {
    val futures = comps map{
      (x:(String, A)) =>{
        x._2.render(x._1)
      }
    }
    Future sequence futures
  }

  def accum[A<:FutureComponent] (params:List[List[Any]], comps:(String, A)*):Future[Seq[(String, String)]] = {
    val futures = comps map{
      (x:(String, A)) =>{
        x._2.render(x._1)//(params?)
      }
    }
    Future sequence futures
  }
}
