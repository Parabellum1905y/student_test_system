package quickstart.action

import xitrum.annotation.GET

/**
  * Created by luger on 25.04.16.
  */
@GET("/admin")
class Admin extends DefaultHelloLayout{
  def execute() {
    sessiono[Int]("authLevel").getOrElse(-1) match {
      case -1 => //guest
        redirectTo[Hello]()
      case 0 => // default user
        redirectTo[SiteIndex]()
      case 1 => // admin
        respondView()
    }
  }
}