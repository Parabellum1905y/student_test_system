package quickstart.action

import io.netty.channel.ChannelFuture
import xitrum.annotation.GET

import scala.util.{Failure, Success}

@GET("")
class SiteIndex extends DefaultLayout{
  def respondMain (defaultUserRespond:()=>Unit):Unit =
    sessiono[Int]("authLevel").getOrElse(-1) match {
      case -1 => //guest
        redirectTo[Hello]()
      case 0 => // default user
        defaultUserRespond ()
      case 1 => // admin
        redirectTo[Admin]()
    }

  def respondMain (defaultUserRespond:()=>ChannelFuture) =
    sessiono[Int]("authLevel").getOrElse(-1) match {
      case -1 => //guest
        redirectTo[Hello]()
      case 0 => // default user
        defaultUserRespond ()
      case 1 => // admin
        redirectTo[Admin]()
    }

  def execute = {
    val func:()=>ChannelFuture = respondView
    respondMain(func)
  }
}