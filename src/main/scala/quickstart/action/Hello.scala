package quickstart.action

import xitrum.annotation.GET

/**
  * Created by luger on 25.04.16.
  */
@GET("/hello")
class Hello extends DefaultHelloLayout{
  def execute() {
    respondView()
  }
}