package quickstart.action

import com.sksamuel.elastic4s.RichGetResponse
import testing.dao.tests._
import testing.model.tests.{TasksRow, TestsLogRow, TestsRow}
import testing.model.users.User
import xitrum.annotation.GET
import xitrum.util.SeriDeseri

import scala.concurrent.Future
import scala.util.{Failure, Success}

/**
  * Created by luger on 12.06.16.
  */
object TestRunServ extends xitrum.Log {
  import scala.concurrent.ExecutionContext.Implicits.global

  lazy val tasksDao = TasksDao.apply
  lazy val elasticRunTest = ElasticTestRunDao.apply

  def loadOrCreateQueue (currentUserId:Long, qu: RichGetResponse, t: TestsRow, logid:Long): Future[Option[List[MixedTask]]] =
    if (qu.isExists)
      Future{
        log info s"qu val :${qu}, val : ${qu.field("queue").getValue.toString}"
        val rs = SeriDeseri.fromJson[List[Map[String, Any]]](qu.field("queue").getValue.toString)
        rs.map { m =>
          m.map { r =>
            MixedTask(r("taskId").toString.toLong, r("orderNum").toString.toInt)
          }
        }
      }
    else {
      if (t.randomtasks.contains(1))
        tasksDao.mixedArrayOfTasks(t.testid, shuffle = true).map{ shuffleTasks =>
          elasticRunTest.indexTaskMixingQueue(currentUserId, logid, shuffleTasks) onComplete {
            case Success(x) =>
              log info s"succeed: ${x.getId}"
            case Failure(err) =>
              log warn s"$err"
          }
          log info s"qu val :${qu}, val : ${shuffleTasks}"
          Option(shuffleTasks)
        }
      else
        tasksDao.mixedArrayOfTasks(t.testid, shuffle = false).map{shuffleTasks =>
          elasticRunTest.updateTaskMixingQueue(currentUserId, logid, shuffleTasks)
          Option(shuffleTasks)
        }
    }

  def getQueue (currentUserId:Long, test: Option[TestsRow], logid:Long) = test match {
    case None =>
      Future{
        Option.empty[List[MixedTask]]
      }
    case Some(t) =>
      for {
        qu <- elasticRunTest.getTaskMixingQueue(logid)
        st <- loadOrCreateQueue(currentUserId, qu, t, logid)
      }yield st
  }


  def getTask (test: Option[TestsRow], queue:List[MixedTask], num:Int): Future[Option[TasksRow]] =
    test match {
      case None =>
        Future{
          Option.empty[TasksRow]
        }
      case Some(t) =>
        val id = if (num > queue.size) queue.last.taskId
        else queue.find(_.orderNum == num).getOrElse(MixedTask(-1L, -1)).taskId
        tasksDao.get(id).map(t => Option(t))
    }
}

@GET("testrun/:testlogid<[0-9]+>/:tasknum<[0-9]+>")
class TestRun extends SiteIndex{
  lazy val testsDao = TestsDao
  lazy val tasksDao = TasksDao.apply
  lazy val elasticRunTest = ElasticTestRunDao.apply
  lazy val currentUserId = sessiono[Long]("userId") getOrElse (-1L)
  lazy implicit val answerDao = AnswersDao.apply

  def a() = {
    val number = paramo[Int]("tasknum").filter(_ > 0).getOrElse(1)
    val logid =  paramo[Long]("testlogid").filter(_ > 0).getOrElse(-1L)
    (logid, number) match {
      case (_, 0) => redirectTo(url[TestRun]("testlogid"->logid, "tasknum"->1))
      case (-1L, _) => redirectTo[SiteIndex]()
      case _ =>
        val tasking = for {
          test        <- testsDao.getForLogId(logid)
          queue       <- TestRunServ.getQueue(currentUserId, test, logid)
          task        <- TestRunServ.getTask(test, queue.getOrElse(Nil), number)
          answerList  <- if (task.isDefined) answerDao.getForTask (task.get.taskid) else Future{Nil}
          answerLog   <- if (task.isDefined) answerDao.getLogForTask (logid, task.get.taskid) else Future{Nil}
          allCompleted<- if (task.isDefined) tasksDao.isAllCompleted(logid) else Future{false}
          //newTaskLog <-
        }yield (test, task, queue, answerList, answerLog, allCompleted)
        tasking.onComplete{
          case Success(x)=>
            elasticRunTest.getLastTaskId(currentUserId) onComplete {
              case Success(lx) =>
                if (lx.isExists){
                  val num = x._3.get.find(_.taskId == x._2.get.taskid).
                    getOrElse(MixedTask(x._2.get.taskid, number)).orderNum
                  elasticRunTest.updateLastTaskId(currentUserId,logid,x._2.get.taskid, num)
                  at("test") = x._1.get
                  at("task") = x._2.get
                  at("queue") = x._3.get
                  at("answers") = x._4
                  at("allCompleted") = x._6
                  at("num") = num
                  at("answerlog") = x._5
                  at("logid") = logid
                  respondView ()
                }else{
                  val lastNum = lx.field("lasttasknum").getValue.toString.toInt
                  val lasttaskid = lx.field("lasttaskid").getValue.toString.toLong
                  val num = x._3.get.find(_.taskId == x._2.get.taskid).
                    getOrElse(MixedTask(x._2.get.taskid, number)).orderNum
                  if (lasttaskid == -1L){
                    elasticRunTest.updateLastTaskId(currentUserId,logid,x._2.get.taskid, num)
                  }
                  at("test") = x._1.get
                  at("task") = x._2.get
                  at("queue") = x._3.get
                  at("answers") = x._4
                  at("answerlog") = x._5
                  at("allCompleted") = x._6
                  at("num") = num
                  at("logid") = logid
                  respondView ()
                }
              case Failure(err)=>
                val num = x._3.get.find(_.taskId == x._2.get.taskid).
                  getOrElse(MixedTask(x._2.get.taskid, number)).orderNum
                elasticRunTest.updateLastTaskId(currentUserId,logid,x._2.get.taskid, num)
                at("test") = x._1.get
                at("task") = x._2.get
                at("queue") = x._3.get
                at("answers") = x._4
                at("answerlog") = x._5
                at("allCompleted") = x._6
                at("num") = num
                at("logid") = logid
                respondView ()
            }
          case Failure(ex) =>
            log error  s"$ex"
            respondView ()
        }
    }
  }

  override def execute = {
    val b:()=> Unit = a
    respondMain(b)
  }
}

@GET("viewlog/:testlogid<[0-9]+>")
class ViewLoggedTest extends SiteIndex{
  lazy val testsDao = TestsDao
  lazy val tasksDao = TasksDao.apply
  lazy val elasticRunTest = ElasticTestRunDao.apply
  lazy val currentUserId = sessiono[Long]("userId") getOrElse (-1L)
  lazy implicit val answerDao = AnswersDao.apply

  def a() = {
    val logid =  paramo[Long]("testlogid").filter(_ > 0).getOrElse(-1L)
    logid match {
      case -1L => redirectTo(url[Reports]())
      case _ =>
        val tasking = for {
          testNLog: Seq[(TestsLogRow, TestsRow, /*creator*/User, User)] <- testsDao.getForReport(logid, currentUserId)
          queue       <- TestRunServ.getQueue(currentUserId, testNLog.headOption.map(_._2), logid)
          task        <- {
            if (testNLog.headOption.map(_._2).isDefined &&
              testNLog.headOption.map(_._2).get.allowviewdetailedresults.contains(1.toShort))
              tasksDao.getForTest(testNLog.headOption.map(_._2.testid).getOrElse(-1L))
            else Future{Nil}
          }
          answerList  <- if (task.isEmpty) Future{Nil} else answerDao.getForTasks (task.map(_.taskid):_*)
          answersLogList<-if (task.isEmpty) Future{Nil} else answerDao.getLogForTask (logid, task.map(_.taskid):_*)
          userPoint   <- testsDao.selectAnalyzeUserPoint(logid, currentUserId)
        //newTaskLog <-
        }yield (testNLog, task, queue, answerList, answersLogList, userPoint)
        tasking.onComplete{
          case Success(x)=>
            at("test") = x._1.map(_._2).head
            at("testlog") = x._1.map(_._1).head
            at("creator") = x._1.map(_._3).head
            at("usertester") = x._1.map(_._4).head
            at("task") = x._2
            at("queue") = x._3.get
            at("answers") = x._4
            at("answerslog") = x._5
            at("userpoint") = x._6
            at("logid") = logid
            respondView ()
          case Failure(ex) =>
            log error  s"$ex"
            respond500Page()
        }
    }
  }

  override def execute = {
    val b:()=> Unit = a
    respondMain(b)
  }
}
