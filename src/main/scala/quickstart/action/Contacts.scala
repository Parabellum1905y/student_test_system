package quickstart.action

import xitrum.Action
import xitrum.annotation.GET

@GET("hellocontacts")
class Contacts extends DefaultHelloLayout{
  def execute() {
    respondView() 
  }
}