package quickstart.action

import xitrum.Action
import xitrum.annotation.GET

@GET("product")
class ProductDescriptionShow extends DefaultLayout{
  def execute() {
    respondView()
  }
}