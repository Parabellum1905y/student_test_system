package quickstart.action

import xitrum.annotation.GET

/**
  * Created by luger on 25.04.16.
  */
@GET("/signup")
class SignUp extends DefaultLayout{
  def execute() {
    respondView()
  }
}