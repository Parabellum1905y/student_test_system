package quickstart.action

import testing.dao.classifiers.ClassifiersDao
import xitrum.{Component, FutureAction}
import xitrum.annotation.GET

import scala.util.{Failure, Success}
/**
  * Created by luger on 02.06.16.
  */
class LangSet extends Component{
  def render = {
    val lang = sessiono[String]("lang").getOrElse("ru")
    at("lang") = lang
    renderView
  }
}

@GET("lang")
class ChangeLang extends FutureAction{
  def execute = {
    val lang = paramo[String]("lang").getOrElse(browserLanguages.headOption.getOrElse("ru").
      substring(0, 2).toLowerCase)
    language = lang
    session("lang") = language
    ClassifiersDao getByCode lang onComplete {
      case Success (langRow) =>
        if (langRow.isDefined)  session("langId") = langRow.get.langid
        respondJson("lang"->lang)
      case Failure (err) =>
        log error s"$err"
        respondJson("lang"->lang)
    }

  }
}