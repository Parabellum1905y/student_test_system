package quickstart.action

import xitrum.FutureAction

trait DefaultLayout extends FutureAction {
  beforeFilter{
    if (sessiono[String]("lang").isEmpty) {
      session("langId") = 1L
      language = browserLanguages.headOption.getOrElse("ru").substring(0, 2).toLowerCase
      session("lang") = language
    }else {
      language = sessiono[String]("lang").get
    }
    true
  }

  override def layout = renderViewNoLayout[DefaultLayout]()
}
