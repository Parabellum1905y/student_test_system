package quickstart.action

import xitrum.annotation.GET

/**
  * Created by luger on 27.04.16.
  */
@GET("hellocapabilities")
class Capabilities extends DefaultHelloLayout{
  def execute () = respondView()
}
