package quickstart.action

import xitrum.Component

/**
  * Created by luger on 20.06.16.
  */
class SuccessMailReport extends Component with xitrum.Log{
  def render(testLogId:Long, nick:String, testName:String) = {
    at("nick") = nick
    at("test") = testName
    at("rurl") = absUrl[ViewLoggedTest]("testlogid"->testLogId)
    renderFragment("successMailReport")
  }
}