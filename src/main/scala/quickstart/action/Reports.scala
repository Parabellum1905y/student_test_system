package quickstart.action

import testing.dao.tests.{ElasticTestRunDao, TasksDao, TestsDao}
import xitrum.annotation.GET

import scala.util.{Failure, Success}

@GET("reports")
class Reports extends SiteIndex {
  lazy val testsDao = TestsDao
  lazy val tasksDao = TasksDao.apply
  lazy val elasticRunTest = ElasticTestRunDao.apply
  lazy val currentUserId = sessiono[Long]("userId") getOrElse (-1L)

  def a() =
    testsDao.getRunnedForUser(currentUserId).onComplete {
      case Success(seq) =>
        at("tests") = seq
        respondView()
      case Failure(ex) =>
        log error s"$ex"
        respondView()
    }

  override def execute = {
    val b: () => Unit = a
    respondMain(b)
  }
}