package quickstart.action

import testing.dao.usersDAO.GroupsDao
import testing.model.users.{GroupRelRow, User, UserGroupsRow}

import scala.concurrent.Future

class GroupListBlock extends FutureComponent{
  def reorgGroups (seq:Seq[(UserGroupsRow, GroupRelRow)], userId:Long) =
    seq.groupBy(_._1).map {x =>
      val head = x._2.filter (d => d._2.typeid == 1.toShort && d._2.objectid == userId).
        map(_._2.isOwner).headOption
      (x._1, head)
    }.toList.sortBy {
        gr=>gr._1.groupid
    }

  def render(ats:String):Future[(String, String)] = {
    val currentUserId = sessiono[Long]("userId") getOrElse (-1L)
    at("userId") = currentUserId
    GroupsDao searchUsersGroups currentUserId map {groups =>
        at("groups") = reorgGroups (groups, currentUserId)
        val r = renderView()
        (ats, r)
      }
  }

  def render(ats:String, x:Any*):Future[(String, String)] = {
    val currentUserId = sessiono[Long]("userId") getOrElse (-1L)
    GroupsDao searchUsersGroups currentUserId map (groups => {
      at("groups") = groups
      val r = renderView()
      (ats, r)
    })
  }
}

