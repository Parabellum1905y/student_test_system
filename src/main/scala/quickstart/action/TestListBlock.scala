package quickstart.action

import testing.dao.tests.TestsDao

import scala.concurrent.Future

/**
  * Created by luger on 02.06.16.
  */
class TestListBlock extends FutureComponent{
  def render(ats:String):Future[(String, String)] = {
    val currentUserId = sessiono[Long]("userId") getOrElse (-1L)
    at("userId") = currentUserId
    TestsDao searchUsersTests currentUserId map { tests =>
      at("tests") = tests
      val r = renderView()
      (ats, r)
    }
  }

  def render(ats:String, x:Any*):Future[(String, String)] = {
    val currentUserId = sessiono[Long]("userId") getOrElse (-1L)
    at("userId") = currentUserId
    TestsDao searchUsersTests currentUserId map { tests =>
      at("tests") = tests
      val r = renderView()
      (ats, r)
    }
  }
}

