package quickstart.action

import xitrum.annotation.GET

import scala.util.{Failure, Success}

@GET("tests")
class Tests extends SiteIndex {
  def a() =
    ComponentsAccumulate.accum[FutureComponent] (("Tests", newComponent[TestListBlock])).onComplete {
      case Success(seq)=>
        seq foreach {x=>at(x._1) = x._2}
        respondView ()
      case Failure(ex) =>
        log error  s"$ex"
        respondView ()
    }

  override def execute = {
    val b:()=> Unit = a
    respondMain(b)
  }
}

