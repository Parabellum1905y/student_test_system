package quickstart.action

import testing.model.users.User
import xitrum.Component

/**
  * Created by luger on 30.05.16.
  */
trait ProfilesView extends Component {
  def render() = {
    if (sessiono[User]("user").isDefined) at("user") = sessiono[User]("user").get

    renderView()
  }
}

class ProfileBlock extends ProfilesView

class ProfileEditBlock extends ProfilesView

class ProfileEditSecurityBlock extends ProfilesView