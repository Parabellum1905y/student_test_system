package quickstart.action

import testing.dao.tests.TestsDao
import xitrum.annotation.GET

import scala.util.{Failure, Success}

@GET("leaderboard")
class LeaderBoard extends SiteIndex{
  lazy val testsDao = TestsDao
  lazy val currentUserId = sessiono[Long]("userId") getOrElse (-1L)

  def a() =
    testsDao.loadLeaderShip.onComplete {
      case Success(seq) =>
        at("rating") = seq.map{x =>
          (x._2, x._1._2, x._1._3, (x._1._2*1.0)/x._1._3)
        }.sortBy(_._4).reverse
        respondView()
      case Failure(ex) =>
        log error s"$ex"
        respondView()
    }

  override def execute = {
    val b: () => Unit = a
    respondMain(b)
  }
}

