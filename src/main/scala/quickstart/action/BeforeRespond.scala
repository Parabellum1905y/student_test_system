package quickstart.action

import xitrum.FutureAction

/**
  * Created by luger on 20.05.16.
  */
trait BeforeRespond extends FutureAction {
  beforeFilter{
    if (sessiono[String]("lang").isEmpty) {
      session("langId") = 1L
      language = browserLanguages.headOption.getOrElse("ru").substring(0, 2).toLowerCase
      session("lang") = language
    }else {
      language = sessiono[String]("lang").get
    }
    sessiono[Int]("authLevel").getOrElse(-1) match {
      case -1 => //guest
        if (isAjax) respondJson(Map ("status"->"error", "data"->"UNAUTHORIZED")) else redirectTo[Hello]()
        false
      case 0 | 1=> // default user && admin
        true
    }
  }
}
