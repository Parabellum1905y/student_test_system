package quickstart.action

import testing.dao.tests.MixedTask
import testing.model.users.User
import xitrum.Component

/**
  * Created by luger on 13.05.16.
  */
class DashboardNavbar extends Component {
  def render(id:Int) = {
    at("idpage") = id
    renderView()
  }
}

class MainNavbar extends Component {
  def render() = {
    if (sessiono[User]("user").isDefined) at("user") = sessiono[User]("user").get
    at("langpanel") = newComponent[LangSet].render
    renderView()
  }
}

class TasksNavbar extends Component {
  def render(taskNum:List[MixedTask], logid:Long, num:Int) = {
    at("taskNum") = taskNum
    at("logid") = logid
    at("num") = num
    renderView()
  }
}
