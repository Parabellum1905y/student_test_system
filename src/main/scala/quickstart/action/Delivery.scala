package quickstart.action

import xitrum.Action
import xitrum.annotation.GET

@GET("delivery")
class Delivery extends DefaultLayout{
  def execute() {
    respondView()
  }
}
