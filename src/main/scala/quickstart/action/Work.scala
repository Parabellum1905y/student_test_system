package quickstart.action

import xitrum.annotation.GET

/**
  * Created by luger on 27.04.16.
  */

@GET("hellowork")
class HelloWork extends DefaultHelloLayout{
  def execute () = respondView()
}
