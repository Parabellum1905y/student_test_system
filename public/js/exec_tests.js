$(document).ready(function(){
    //loadAnswers ()
    $(".answer-check,.answer-check-group").click (function(){
       var foundChecked = false;
       $(".answer-check:checkbox").each (function (i, elem){
            foundChecked = foundChecked || $(elem).is(":checked");
       });
       $(".answer-check-group").each (function (i, elem){
            foundChecked = foundChecked || $(elem).is(":checked");
       });
       if (foundChecked) {
        $("#send-answer").show();
       }else{
        $("#send-answer").hide();
       }
    });

    $("#form-answer").submit(function (e){newPostCallback(e, this, afterSendCallback, false);});

    $("#finish-test").submit(function (e){newPostCallback(e, this, afterFinishCallback, false);});

    $("#skip-answer").click(function (e){
        e.preventDefault ();
        $("#form-answer [name=skip]").val(1);
        var skipAction = $("#form-answer").data("next-action");
        $("#form-answer").prop ("action", skipAction);
        $("#form-answer").submit ();
    });

    $("#send-answer").click(function (e){
        e.preventDefault ();
        var foundChecked = false;
       $(".answer-check:checkbox").each (function (i, elem){
            foundChecked = foundChecked || $(elem).is(":checked")
       });
       $(".answer-check-group").each (function (i, elem){
            foundChecked = foundChecked || $(elem).is(":checked");
       });
       if (foundChecked) {

        $("#form-answer [name=skip]").val(0);
        var sendAnswerAction = $("#form-answer").data("answer-action");
        $("#form-answer").prop ("action", sendAnswerAction);
        $("#form-answer").submit ();
       }else{
        alert ("Необходимо заполнить ответ");
       }
    });
});

function loadAnswers (){
    var url = $("#ansurl").val ();
    var tid = $("#taskid").val ();
    if (isEmpty(tid)) tid = 0;
    loadDataAsync (url,
        {type:"GET",his:false, p:null, setParams:false},
       {}, null,
       function(data){
        if (data != "EMPTY"){
            $("#answerslist").html (data);
        }
    });
}

function afterSendCallback(data){
    if (data.status == "success"){
        var uri = data.uri;
        location.href = uri;
    }else {
        if (data.uri){
            location.href = data.uri;
        }else {
            location.reload();
        }

    }
}

function afterFinishCallback(data){
    if (data.status == "success"){
        var uri = data.data;
        location.href = uri;
    }else {
        if (data.uri){
            location.href = data.uri;
        }else {
            location.reload();
        }

    }
}
