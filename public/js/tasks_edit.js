$(document).ready(function(){
    $('#editTask :checkbox').checkboxpicker().change(function() {
      if ($(this).is(":checked")) $(this).val (1);
      else $(this).val (0);
    });

    $("#editTask").submit (function (e){
        $("#editTask [name=task]").val ($('#editTask .tasksnote').summernote('code'));
        newPostCallback(e, this, afterSubmitNewTask, false);
    });

    $("#deleteTask").submit (function (e){
        //$("#editTask [name=task]").val ($('#editTask .tasksnote').summernote('code'));
        newPostCallback(e, this, afterSubmitNewTask, false);
    });

    $("#open-new-task").click (function (){
        var newAction = $("#editTest").data ("newtask-action");
        $("#editTask input[name=taskid]").val (-1);
        $("#editTask").attr ("action", newAction);
        $("#editTask .tasksnote").summernote ("code", "");
        $("#taskeditrow").collapse("show");
        $("#editTask").trigger('reset');
    });


// fired on edit test's tasks tab shown
    $("body").on ("show.bs.tab", "#edit-task-tab", function (e) {
        //e.target
        var testid = $("#editTestAccesses .testForGroupAccessId").val ();
        loadTasks (testid, function (data){
            afterloadTasks(data, true);
        });
    });

    $("body").on ("click", ".edittask", function (e){
        $("#taskeditrow").collapse("show");
        var testId = $(this).data("testid");
        var taskId = $(this).data("taskid");
        $("#editTask input[name=testid]").val (testId);
        $("#editTask input[name=taskid]").val (taskId);
        $(".testForGroupAccessId").val (testId);
        var editAction = $("#editTask").data ("edittask-action");
        var getAction = $(this).data ("geturl");
        $("#editTask").attr ("action", editAction);
        //$("#edit-test-accesses-tab").trigger("show.bs.tab");
        loadTask (testId, taskId,
               function(data){
                   if (data.status == "success" && data.data != "EMPTY_TASK") {
                        var task = data.data;
                        for (key in task){
                            var value = task[key];
                            if ($("#editTask [name="+key+"]").is(":checkbox")){
                                if (value == 1) $("#editTask [name="+key+"]").prop ("checked", true);
                                else $("#editTask [name="+key+"]").prop ("checked", false);
                            }else if ($("#editTask [name=view-"+key+"]").length){
                                $("#editTask [name=view-"+key+"]").
                                        timepicker('setTime', secToTime(value));
                            }else if ($("#editTask [name="+key+"]").prop("tagName") &&
                            $("#editTask [name="+key+"]").prop("tagName").toLowerCase() == "textarea"){
                                $("#editTask [name="+key+"]").val (value);
                                $("#editTask .tasksnote").summernote ("code", value);
                            }else{
                                $("#editTask [name="+key+"]").val (value);
                            }
                        }
                   }
            });
    });

    // TODO
    $("body").on ("click", ".deletetask", function (e){
        var testId = $(this).data("testid");
        var taskId = $(this).data("taskid");
        $("#deleteTask input[name=testid]").val (testId);
        $("#deleteTask input[name=taskid]").val (taskId);
        $(".testForGroupAccessId").val (testId);
        $("#deleteTask").submit ();
    });
});

function afterSubmitNewTask(data){
    dialogs.hidePleaseWait();
    if (data.status == "success"){
        $("#editTask .alert.task-saver-success").show();
        var testid = data.data.testid;
        $("#taskeditrow").collapse("hide");
        loadTasks (testid, function (data){
            afterloadTasks(data);
        });
    }else{
        $("#editTask .alert.task-saver-error").show();
    }
}

function loadTask (testid, taskId, func){
    if (isEmpty(testid)) testid = 0;
    if (isEmpty(taskId)) taskId = 0;
    loadDataAsync (xitrum.withBaseUrl("task/"+testid+"/"+taskId),
            {type:"GET",his:false, p:null, setParams:false},
           {}, null,
           function(data){
            func(data);
        });
}

function loadTasks (testid, func){
    if (isEmpty(testid)) testid = 0;
    loadDataAsync (xitrum.withBaseUrl("tasks/"+testid),
            {type:"GET",his:false, p:null, setParams:false},
           {}, null,
           function(data){
            func(data);
        });
}

function afterloadTasks(data){
   dialogs.hidePleaseWait();
   if (data.status == "success" && data.data.length) {
        var tasks = data.data;
        renderTasks(tasks);
   }else{
        renderTasks([]);
   }
}

function renderTasks (tasks){
    // its fucking shity-hack
    dialogs.hidePleaseWait();
    var parentContainer = $("#tasklist li.cloneable").closest("ul");
    var temp = $("#tasklist li.cloneable").detach();
    var cloneable = temp.clone();
    temp.removeClass("cloneable");
    parentContainer.html("");
    console.log (tasks);
    if (tasks.length && tasks.data != "EMPTY_TASK"){
        for (i in tasks) {
            var template = temp.clone ();
            var templates = {
                "testid": tasks[i].testid,
                "taskid": tasks[i].taskid,
                "title": tasks[i].title,
                "uri" : xitrum.withBaseUrl("ans-list/"+tasks[i].taskid)};
            var html = template.html(templater(templates, template.html ()));
            parentContainer.append (html);
            console.log (html);
        }
    }
    $(parentContainer).append(cloneable);
    $('[data-toggle="tooltip"]').tooltip();
}
