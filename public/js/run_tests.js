$(document).ready(function(){
    $("#test-preview-panel").collapse ("hide");

    $('#search_for_test_query').typeahead({
        source: function (query, process) {
            return loadDataAsync (xitrum.withBaseUrl("t-dropdown?pattern="+query),
                                            {type:"GET",his:false, p:null, setParams:false},
                           {}, null,
                           function(data){
                               if (data.status == "success") {
                                    testsName = [];
                                    testsId = {};
                                    $.each( data.data, function (index, data){
                                        test = data.test;
                                        testsName.push (test.name + "/" + test.description);
                                        testsId[test.name + "/" + test.description] = data.id;
                                    });
                                    return process (testsName);
                               }
                               return process ([]);
                        });

                },
                updater : function (item) {
                    loadTest (testsId[item]);
                    return item;
                }
    })/*.on('keyup', this, function (event) {
              if (event.keyCode == 13) {
                  $('#input').typeahead('close');
              }
          })*/;

    $("body").on("click", "#startTest", function(e){
        e.preventDefault();
        var testid = $(this).data("testid");
        checkAccess (testid, function(data){
            loadDataAsync (xitrum.withBaseUrl("start-test"),
                   {type:"POST",his:false},
                   {"testid":data.testid}, null,
                   function(data){
                        console.log (data);
                       if (data.status == "success" && data.data != "EMPTY") {
                            location.href = data.data;
                       }else if (data.status == "error"){
                            if (data.accessed && data.accessed == true) location.href = data.uri;
                            else $(".testsaver-error").show();
                       }
                       dialogs.hidePleaseWait();
                });
        });
    });
});

function loadDropdown (){
}

function checkAccess (testid, func){
    dialogs.showPleaseWait();
    loadDataAsync (xitrum.withBaseUrl("test-accessed/"+testid),
       {type:"GET",his:false, p:null, setParams:false},
       {}, null,
       function(data){
           if (data.status == "success" && data.data != "EMPTY") {
                $(".testsaver-error").hide();
                func (data.data);
           }else{
                $(".testsaver-error").show();
           }
           dialogs.hidePleaseWait();
    });
}

function loadTest (id){
    dialogs.showPleaseWait();
    loadDataAsync (xitrum.withBaseUrl("test-preview/"+id),
       {type:"GET",his:false, p:null, setParams:false},
       {}, null,
       function(data){
           if (data.status == "success" && data.data != "EMPTY") {
                renderTestPreview (data.data);
           }else{
            $(".testsaver-error").show();
            $("#requestTest").show();
           }
           dialogs.hidePleaseWait();
    });
}

function renderTestPreview (test){
    // its fucking shity-hack
    dialogs.hidePleaseWait();
    var parentContainer = $("#test-preview-panel div.cloneable").parent();
    var temp = $("#test-preview-panel div.cloneable").detach();
    var cloneable = temp.clone();
    temp.removeClass("cloneable");
    parentContainer.html("");

    if (test){
        var template = temp.clone ();
        var templates = {
            "testname": test.testname,
            "testdescription": test.testdescription,
            "jumptaskfine": test.jumptaskfine,
            "testexectime" : secToTime(test.testexectime),
            "testid" : test.testid
        };
        var html = template.html(templater(templates, template.html ()));
        parentContainer.append ($(html).show());
    }
    $(parentContainer).append(cloneable);
    $("#requestTest").hide();
    $(".testsaver-error").hide();
    $("#test-preview-panel").collapse ("show");
    $("#search_for_test_query").val ("");
}

var testsName = new Array ();
var testsId = {};