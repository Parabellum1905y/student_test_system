function afterNewAvatarCallback(data){
    dialogs.hidePleaseWait ();
    if (data.status == "success"){
        $(".userphoto").attr ("src", xitrum.withBaseUrl("/image/"+data.data));
        $("input#changeAvatarFileId").val (data.data);
        $(".alert.usersaver-photo-info").show();
    }else $(".alert.usersaver-error").show();
}

function afterChangeProfileCallback(data){
    dialogs.hidePleaseWait ();
    if (data.status == "success"){
        location.href = xitrum.withBaseUrl("");
    }else{
        $(".alert.usersaver-error").show();
    }
}

function getProfile(url, obj){
	    //$('.right-sidebar.full-text.profile').show('slow');
		loadDataAsync (xitrum.withBaseUrl(url),
		                    {type:"GET",his:false, p:null, setParams:false},
				{}, null,
				function(data){
					if (!isEmpty (data)){
						$(obj).html(data);
						//$(".profile-page").hide();
						//setRightSideBarScroll(".profile-page");
						//$("#profile-view").show("slow");
				    	//$(".profile-open-page:first").addClass ("opened");
					}
				});
//	    openRightSidebar ();
}

function after400Error(xhr){
    dialogs.hidePleaseWait ();
    if (xhr.responseText.startsWith("Request content body is too big.")){
        alert ("Файл слишком большой");
    }
}

$(document).ready(function(){
    $("#openProfile").click (function (){
    });

    $("#open-profile").click (function (e){
        e.preventDefault();
        $(".profile#settings").hide("slow");
        $(".profile#profile").show("slow");
    });

    $("#newavatar").change(function(e){
        $("#changePhoto").submit();
        //newPostCallback(e, $("#changePhoto"), afterNewAvatarCallback);
    });

    $("#changePhoto").submit (function(e){
        newUploadCallback(e, this, afterNewAvatarCallback, after400Error);
    });

    $("#changeProfile").submit (function(e){
        newUploadCallback(e, this, afterChangeProfileCallback, after400Error);
    });

    $("#changeSecurityProfile").submit (function(e){
        newUploadCallback(e, this, afterChangeProfileCallback, after400Error);
    });


    $("body").on ("click", ".link2.author", function () {
        $(".rightsidebar").hide ();
        $("#anotherProfileBlock").show("slow");
    	getProfile ($(this).data ("url"), $("#anotherProfileBlock"));
    });

    $("[data-hide]").on("click", function(){
        $(this).closest("." + $(this).attr("data-hide")).hide();
    });

    $("#close-mobile").on("click", function(){
        $('#profileBlock').hide();
    });

    $("#openProfile").click (function (e){
        e.preventDefault();
        $('#profileBlock').show();
        $('.nav-tabs a[href="#profile"]').tab('show');
    });

    $("#openSettings").click (function (e){
        e.preventDefault();
        $('#profileBlock').show();
        $('.nav-tabs a[href="#settings"]').tab('show');
    });
});