/* ================================================================================================
 * history.custom.js
 * 
 * Work with history and navigation
 * ================================================================================================ */
$( document ).ready(function() {
	var State = History.getState();
	var $category_navigation_links = $("#category_navigation").find("a");

	History.log('initial:', State.data, State.title, State.url);
	
	History.Adapter.bind(window, 'statechange', function() {
		var State = History.getState(); 
		
		History.log('statechange:', State.data, State.title, State.url);
		// loadPage(State.url);
	});

	$category_navigation_links.on('click',  function(e) {
		History.log('Click:', window.location.pathname, State.title, State.url);
		History.pushState(null, document.title, $(this).attr('href'));
		var category = window.location.search;
		loadPage( category );
		
		appendBreadcrumb( $(this).text() );
	});
	function loadPage(url) {
		console.log( "url:", url.substring(1) );
	}
	setBreadcrumb();
});

var PATH = {
		product : '/product',
		category : '/category',
		compare : '/compare'
}
/**
 * Updated breadcrumb(navigation) based on part of the path </br> 
 * like '/category/2' or '/about'
 **/
function setBreadcrumb(){
	var STR_COMPARE = "Сравнение товаров"
	var NOT_ARRAY_LENGTH = 1,
		search = window.location.search=="" ? "" 
				: "?"+window.location.search.split('?')[1],
		pathname = window.location.pathname,
		locationFromGet = "";
	
	/*switch ( pathname ){
		case PATH.product: 
			locationFromGet = PATH.product + search.split('/')[0]; 
			console.info(search.split('/')[0]); 
			
			break;
		case PATH.category: 
			locationFromGet = pathname + search; break;
	}*/
	if( pathname != PATH.compare){
		locationFromGet = pathname + search;
		$linkCategory = $("a[href='"+locationFromGet+"']");
		if( $linkCategory.length == NOT_ARRAY_LENGTH ){
			appendBreadcrumb( $linkCategory.text() )
		}
	} else {
		appendBreadcrumb( STR_COMPARE )
	}
}

function appendBreadcrumb( title ){
	$(".breadcrumb > li").not(":first").remove();
	$(".breadcrumb").append( '<li class="active">'+ title + '</li>' );
}