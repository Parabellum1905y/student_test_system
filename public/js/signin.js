$(function () {
	
	jQuery.support.placeholder = false;
	test = document.createElement('input');
	if('placeholder' in test) jQuery.support.placeholder = true;
	
	if (!$.support.placeholder) {
		
		$('.field').find ('label').show ();
		
	}
	
});

$(document).ready(function(){
    $("#loginForm").submit(function (e){newPostCallback(e, this, afterLoginCallback, false);});

    $("#signUpForm").submit(function (e){newPostCallback(e, this, afterRegCallback,false);});

    $("#logout").submit(function (e){newPostCallback(e, this, afterLogoutCallback, false);});

    $(".setlang").click (function(e) {
        e.preventDefault ();
        loadDataAsync ($(this).attr("href"),{type:"GET",his:false, p:null, setParams:false},
        {}, null,
        function(data){
            location.reload();
        });
     });

});

function afterLoginCallback(data){
    dialogs.hidePleaseWait();
    if (data.status == "success"){
        location.href = xitrum.withBaseUrl("");
    }else{
    //TODO
        if (data.data === "INVALID_LOGIN") $("#email-form-2").parent().find(".w-form-fail").find("p")
                                                .html ("Неверный логин").parent().show("slow");
        if (data.data === "INVALID_PWD") $("#email-form-2").parent().find(".w-form-fail").find("p")
                                                .html ("Неверный пароль").parent().show("slow");
        if (data.data === "USER_NOT_FOUND") $("#email-form-2").parent().find(".w-form-fail").find("p")
                                                .html ("Неверный логин или пароль").parent().show("slow");
        if (data.data === "USER_NOT_ENABLED") $("#email-form-2").parent().find(".w-form-fail").find("p")
                                                .html ("Пользователь не подтвержден").parent().show("slow");
    }
}

function afterLogoutCallback(data){
    dialogs.hidePleaseWait();
    location.href = xitrum.withBaseUrl("");
}

function afterRegCallback(data){
    dialogs.hidePleaseWait();
    if (data.status == "success"){
    //TODO
        alert("Поздравляем! Вы зарегистрировались у нас на сайте.<br>"+
                   "Для подтверждения регистрации Вам выслано письмо на почту!");
        location.href = xitrum.withBaseUrl("login");
    }else{
    //TODO
        if (data.data === "INVALID_LOGIN") $("#reg2").parent().find(".w-form-fail").find("p")
                                                .html ("Неверный логин").parent().show("slow");
        if (data.data === "INVALID_PWD") $("#reg2").parent().find(".w-form-fail").find("p")
                                                .html ("Неверный пароль").parent().show("slow");
        if (data.data === "USER_NOT_FOUND") $("#reg2").parent().find(".w-form-fail").find("p")
                                                .html ("Неверный логин или пароль").parent().show("slow");
        if (data.data === "INVALID_PWD_CONFIRM") $("#reg2").parent().find(".w-form-fail").find("p")
                                                .html ("Неверное подтверждение пароля").parent().show("slow");
        if (data.data === "NOT_EQ_PWD_CONFIRM") $("#reg2").parent().find(".w-form-fail").find("p")
                                                .html ("Введенные пароли не идентичны").parent().show("slow");
        if (data.data === "USER_NOT_CREATED") $("#reg2").parent().find(".w-form-fail").find("p")
                                                .html ("Ошибка :(").parent().show("slow");
        if (data.data === "ALREADY_FOUND") $("#reg2").parent().find(".w-form-fail").find("p")
                                                .html ("Такой пользователь уже существует").parent().show("slow");
    }
}

