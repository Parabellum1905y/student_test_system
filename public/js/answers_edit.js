$(document).ready(function(){
    $('#editAnswer :checkbox').checkboxpicker().change(function() {
      if ($(this).is(":checked")) $(this).val (1);
      else $(this).val (0);
    });

    $("#editAnswer").submit (function (e){
        $("#editAnswer [name=answer]").val ($('#editAnswer .tasksnote').summernote('code'));
        newPostCallback(e, this, afterSubmitNewAnswer, false);
    });

    $("body").on ("submit","#delanswer", function (e){
        newPostCallback(e, this, afterSubmitNewAnswer, false);
    });

    $("body").on ("click",".addanswer", function (){
        var newAction = $("#editAnswer").data ("newanswer-action");
        var taskid = $(this).data ("taskid");
        $("#editAnswer input[name=answerid]").val (-1);
        $("#editAnswer input[name=taskid]").val (taskid);
        $("#editAnswer").attr ("action", newAction);
        $("#answereditrow").collapse ("show");
        $("#editAnswer").trigger('reset');
    });

// fired on edit test's tasks tab shown // TODO
    /*$("body").on ("show.bs.tab", "#edit-task-tab", function (e) {
        //e.target
        var testid = $("#editTestAccesses .testForGroupAccessId").val ();
        loadTasks (testid, function (data){
            afterloadTasks(data, true);
        });
    });
*/
    $("body").on ("click", ".answers-toggle", function (e){
        var url = $(this).data ("usersuri");
        var tid = $(this).data ("tid");
        if (isEmpty(tid)) tid = 0;
        loadDataAsync (url,
            {type:"GET",his:false, p:null, setParams:false},
           {}, null,
           function(data){
            if (data != "EMPTY"){
                $("#answer"+tid).html (data);
                $("#answer"+tid).collapse ("show");
            }
            $('[data-toggle="tooltip"]').tooltip();
        });
    });

    $("body").on ("click", ".deleteanswer", function (e){
        var taskid = $(this).data("taskid");
        var answerid = $(this).data("answerid");
        $("#delanswer [name=taskid]").val(taskid);
        $("#delanswer [name=answerid]").val(answerid);
        $("#delanswer").submit ();
    });

    $("body").on ("click", ".editanswer", function (e){
        var answerId = $(this).data("answerid");
        var taskId = $(this).data("taskid");
        $("#answereditrow").collapse ("show");
        $("#editAnswer input[name=answerId]").val (answerId);
        $("#editAnswer input[name=taskid]").val (taskId);
        var editAction = $("#editAnswer").data ("editanswer-action");
        $("#editAnswer").attr ("action", editAction);
        //$("#edit-test-accesses-tab").trigger("show.bs.tab");
        loadAnswer (taskId, answerId,
               function(data){
                   if (data.status == "success" && data.data != "EMPTY_ANSWER") {
                        var task = data.data;
                        for (key in task){
                            var value = task[key];
                            if ($("#editAnswer [name="+key+"]").is(":checkbox")){
                                if (value == 1) $("#editAnswer [name="+key+"]").prop ("checked", true);
                                else $("#editAnswer [name="+key+"]").prop ("checked", false);
                            }else if ($("#editAnswer [name=view-"+key+"]").length){
                                $("#editAnswer [name=view-"+key+"]").
                                        timepicker('setTime', secToTime(value));
                            }else if ($("#editAnswer [name="+key+"]").prop("tagName") &&
                            $("#editAnswer [name="+key+"]").prop("tagName").toLowerCase() == "textarea"){
                                $("#editAnswer [name="+key+"]").val (value);
                                $("#editAnswer .tasksnote").summernote ("code", value);
                            }else{
                                $("#editAnswer [name="+key+"]").val (value);
                            }
                        }
                   }
            });
    });
});

function afterSubmitNewAnswer(data){
    dialogs.hidePleaseWait();
    $('#editAnswer .tasksnote').summernote('code', "");
    if (data.status == "success"){
        $("#editAnswer .alert.answer-saver-success").show();
        $("#answer"+data.data.taskid).collapse("hide");
        /*var testid = data.data.testid;
        loadAnswer (testid, function (data){
            afterloadAnswers(data);
        });*/
    }else{
        $("#editAnswer .alert.answer-saver-error").show();
    }

    $("#answereditrow").collapse ("hide");
}

function loadAnswer (taskId, answerId, func){
    if (isEmpty(answerId)) answerId = 0;
    if (isEmpty(taskId)) taskId = 0;
    loadDataAsync (xitrum.withBaseUrl("answer/"+taskId+"/"+answerId),
            {type:"GET",his:false, p:null, setParams:false},
           {}, null,
           function(data){
            func(data);
        });
}
