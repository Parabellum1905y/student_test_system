xitrum.ajaxLoadingImg = xitrum.withBaseUrl("/images/ajax-loader.gif");

function sendFile(file, editor) {
    dialogs.showPleaseWait();
        data = new FormData();
        data.append("file", file);
        $.ajax({
            data: data,
            type: 'POST',
            url: xitrum.withBaseUrl("/newfile"),
            cache: false,
            contentType: false,
            processData: false,
            success: function(data) {
                editor.summernote('insertImage', xitrum.withBaseUrl("/image/"+data.data));
                dialogs.hidePleaseWait();
            },
            error:function( jqXHR, textStatus, errorThrown) {
                dialogs.hidePleaseWait();
            }
        });
}

$(document).ready(function(){
	$(document).mouseup(function (e) {
	    if(!$('#authBlock').is(e.target) && $('#authBlock').has(e.target).length === 0){
	    	$('#authBlock').hide();
	    }

	    if(!$('#regBlock').is(e.target) && $('#regBlock').has(e.target).length === 0){
	    	$('#regBlock').hide();
	    }

	    if(!$('#profileBlock').is(e.target) && $('#profileBlock').has(e.target).length === 0){
	    	$('#profileBlock').hide();
	    }

	    if(!$('#anotherProfileBlock').is(e.target) && $('#anotherProfileBlock').has(e.target).length === 0){
	    	$('#anotherProfileBlock').hide();
	    }

	    if(!$('#viewBlock').is(e.target) && $('#viewBlock').has(e.target).length === 0){
	    	$('#viewBlock').hide();
	    }
	});

	$('.tasksnote').summernote({
      height: 300,                 // set editor height
      minHeight: null,             // set minimum height of editor
      maxHeight: null,             // set maximum height of editor
      minWidth:600,
      //focus: true                  // set focus to editable area after initializing summernote
      callbacks:{
        onImageUpload: function(files) {
            sendFile(files[0], $(this));
        }
      }
    });

    jQuery('#scrollup img').mouseover( function(){
		jQuery( this ).animate({opacity: 0.65},100);
	}).mouseout( function(){
		jQuery( this ).animate({opacity: 1},100);
	}).click( function(){
		window.scroll(0 ,0);
		return false;
	});

	jQuery(window).scroll(function(){
		if ( jQuery(document).scrollTop() > 0 ) {
			jQuery('#scrollup').fadeIn('fast');
		} else {
			jQuery('#scrollup').fadeOut('fast');
		}
	});

    $(window).scroll(function() {
        var x = $(document).height() - $(window).height()-$("#contact").height();
        if($(window).scrollTop() >= x-10 && $(window).scrollTop() >= x+10) {
            var date = $(".w-col.w-col-3.column-0-padding.bordering").last ().data("date");
            if (date){
                getPrevPosts ($("#postsMode").val (), "prev", date.split(",").join(""));
            }
        }
    });

    $(".setlang").click (function(e) {
        e.preventDefault ();
        loadDataAsync ($(this).attr("href"),{type:"GET",his:false, p:null, setParams:false},
        {}, null,
        function(data){
            location.reload();
        });
     });
});


function newPostCallback(e, obj, after, hideLoading){
    e.preventDefault(); //STOP default action
    var postData = $(obj).serializeArray();
    var formURL = $(obj).attr("action");
    if (!hideLoading)dialogs.showPleaseWait();
    loadDataAsync (formURL, {type:"POST",his:false}, postData, null, after);
}

/**
* sic! this method difference from newPostCallback. Look at this line:
* var postData = new FormData($(obj)[0]);
*/
function newUploadCallback(e, obj, after, after400, hideLoading){
    e.preventDefault(); //STOP default action
    var postData = new FormData($(obj)[0]);
    var formURL = $(obj).attr("action");
    if (!hideLoading)dialogs.showPleaseWait();
    loadDataAsync (formURL, {type:"POST",his:false,file:true}, postData, null, after, null,after400);
}