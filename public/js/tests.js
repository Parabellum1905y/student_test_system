$(document).ready(function(){
/*
    $(':checkbox').data({
      html: true,
      offLabel: '<span class="glyphicon glyphicon-remove">',
      onLabel: '<span class="glyphicon glyphicon-ok">'
    });
*/

    $(':checkbox').checkboxpicker();
 /*$(':checkbox').checkboxpicker({
    groupCls: 'm-b'
  });*/

    $('#new_test_block :checkbox').checkboxpicker().change(function() {
      if ($(this).is(":checked")) $(this).val (1);
      else $(this).val (0);
    });

    $('.spin-edit').spinedit({
        minimum: 0,
        maximum: 100,
        step: 1,
        value: 0,
        numberOfDecimals: 0
    });

    $('.timing').timepicker({
        minuteStep: 1,
        secondStep: 1,
        template: 'modal',
        appendWidgetTo: 'body',
        showSeconds: true,
        showMeridian: false,
        defaultTime: false
    });

    $('.timing').timepicker().on('changeTime.timepicker', function(e) {
        var name = $(this).prop("name").replace('view-', '');
        $(this).closest("div").find ("input[name="+name+"]").val (timeToSec (e.time.value));
    });

    $("#editTest").submit (function (e){newPostCallback(e, this, afterSubmitNewTest, false);});
    $("#deleteTest").submit (function (e){newPostCallback(e, this, afterSubmitNewTest, false);});
    $("#form-add-group-to-access-test").submit (function (e){newPostCallback(e, this, afterSubmitNewGroupAccess, false);});
    $("#form-add-user-to-access-test").submit (function (e){newPostCallback(e, this, afterSubmitNewGroupAccess, false);});
    // set access to group "all"
    $("#editTestAccesses").submit (function (e){newPostCallback(e, this, afterSubmitAllAccess, false);});

    $("body").on("click", ".deletegroupaccess", function (){
        var parent = $("#form-add-group-to-access-test");
        parent.prop("action", parent.data("delgroupaccess-action"));
        $("#addingForTestAccessGroupId").val ($(this).data("groupid"));
        $("#form-add-group-to-access-test").submit ();
        parent.prop("action", parent.data("newgroupaccess-action"));
    });

    $("body").on("click", ".deleteuseraccess", function (){
        var parent = $("#form-add-user-to-access-test");
        parent.prop("action", parent.data("deluseraccess-action"));
        $("#addingForTestAccessUserId").val ($(this).data("userid"));
        $("#form-add-user-to-access-test").submit ();
        parent.prop("action", parent.data("newuseraccess-action"));
    });

    $("body").on ("click", ".edittest", function (e){
        var testId = $(this).data("testid");
        $("#editTest input[name=testid]").val (testId);
        $(".testForGroupAccessId").val (testId);
        var editAction = $("#editTest").data ("edittest-action");
        var getAction = $(this).data ("geturl");
        $("#editTest").attr ("action", editAction);
        $("#new_test_title").hide ();
        $("#edit_test_title").show ();
        $("#edit-test-tab").tab('show');
        //$("#edit-test-accesses-tab").trigger("show.bs.tab");
        loadDataAsync (getAction, {type:"GET",his:false, p:null, setParams:false},
               {}, null,
               function(data){
                   if (data.status == "success") {
                        var test = data.data;
                        for (key in test){
                            var value = test[key];
                            if ($("#editTest [name="+key+"]").is(":checkbox")){
                                if (value == 1) $("#editTest [name="+key+"]").prop ("checked", true);
                                else $("#editTest [name="+key+"]").prop ("checked", false);
                            }else if ($("#editTest [name=view-"+key+"]").length){
                                $("#editTest [name=view-"+key+"]").
                                        timepicker('setTime', secToTime(value));
                            }else{
                                $("#editTest [name="+key+"]").val (value);
                            }
                        }
                   }
            });
    });

/*
deletetest
*/
    $("body").on ("click", ".deletetest", function (e){
        var testId = $(this).data("testid");
        $("#deleteTest input[name=testid]").val (testId);
        $("#deleteTest").submit ();
    });

// fired on edit test's accesses tab shown
    $("body").on ("show.bs.tab", "#edit-test-accesses-tab", function (e) {
        //e.target
        var testid = $("#editTestAccesses .testForGroupAccessId").val ();
        loadAccesses (testid, function (data){
            afterLoadAccesses(data, true);
        });
    });

    $("#editTestAccesses input#access-test-for-all").checkboxpicker().change (function(e){
        //console.log ("WTF2:"+$("#editTestAccesses [name=access-test-for-all]").is(":checked"));
        var parent = $(this).closest("form");
        if ($(this).is(":checked")){
            parent.prop("action", parent.data("newgroupaccess-action"));
        }else{
            parent.prop("action", parent.data("delgroupaccess-action"));
        }
        $("#editTestAccesses").submit ();
    });

    $("#open-new-test-from").click (function (){
        var newAction = $("#editTest").data ("newtest-action");
        $("#editTest input[name=testid]").val (0);
        $("#editTestAccesses .testForGroupAccessId").val (0);
        $("#edit-test-tab").tab('show');
        $("#editTest").attr ("action", newAction);
        $("#editTest").trigger('reset');
        $("#new_test_title").show ();
        $("#edit_test_title").hide ();
    });

    $('#add_access_for_user_query').typeahead({
        source: function (query, process) {
            return loadDataAsync (xitrum.withBaseUrl("dropdown?pattern="+query),
                                            {type:"GET",his:false, p:null, setParams:false},
                           {}, null,
                           function(data){
                               if (data.status == "success") {
                                    usersName = [];
                                    usersId = {};
                                    $.each( data.data, function (index, user){
                                        usersName.push (user.suggest);
                                        usersId[user.suggest] = user.id;
                                    });
                                    return process (usersName);
                               }
                               return process ([]);
                        });

                },
                updater : function (item) {
                    $("#addingForTestAccessUserId").val (usersId[item]);
                    $("#form-add-user-to-access-test").submit ();
                    return item;
                }
            //['alpha','allpha2','alpha3','bravo','charlie','delta','epsilon','gamma','zulu']
    });

    $('#add_access_for_group_query').typeahead({
            source: function (query, process) {
                    return loadDataAsync (xitrum.withBaseUrl("gr-dropdown?pattern="+query),
                                            {type:"GET",his:false, p:null, setParams:false},
                           {}, null,
                           function(data){
                               if (data.status == "success") {
                                    groupsName = [];
                                    groupsId = {};
                                    $.each( data.data, function (index, group){
                                        groupsName.push (group.suggest);
                                        groupsId[group.suggest] = group.id;
                                    });
                                    return process (groupsName);
                               }
                               return process ([]);
                        });

                },
                updater : function (item) {
                    $("#addingForTestAccessGroupId").val (groupsId[item]);
                    $("#form-add-group-to-access-test").submit ();
                    return item;
                }
            //['alpha','allpha2','alpha3','bravo','charlie','delta','epsilon','gamma','zulu']
    });
});

function afterSubmitNewTest (data){
    dialogs.hidePleaseWait();
    if (data.status == "success"){
        $(".alert.testsaver-success").show();
        location.reload ();
    }else{
    //TODO notification
        $(".alert.testsaver-error").show();
    }
}

function afterSubmitNewGroupAccess (data){
    dialogs.hidePleaseWait();
    $('#add_access_for_group_query').val ("");
    $('#add_access_for_user_query').val ("");
    if (data.status == "success"){
        $(".alert.testaccess-saver-success").show();
        $("#edit-test-accesses-tab").trigger("show.bs.tab");
    }else{
    //TODO notification
        $(".alert.testaccess-saver-error").show();
    }
}

function afterSubmitNewGroupAccess (data){
    dialogs.hidePleaseWait();
    if (data.status == "success"){
        $(".alert.testaccess-saver-success").show();
        $("#edit-test-accesses-tab").trigger("show.bs.tab");
    }else{
    //TODO notification
        $(".alert.testaccess-saver-error").show();
    }
}

function loadAccesses (testid, func){
    if (isEmpty(testid)) testid = 0;
    loadDataAsync (xitrum.withBaseUrl("test-accesses/"+testid),
            {type:"GET",his:false, p:null, setParams:false},
           {}, null,
           function(data){
            func(data);
        });
}

function renderGroups (gracc){
    var parentContainer = $("#groupaccesspanel li.cloneable").closest("ul");
    var temp = $("#groupaccesspanel li.cloneable").detach();
    var cloneable = temp.clone();
    temp.removeClass("cloneable");
    parentContainer.html("");
    if (gracc.length){
        for (i in gracc) {
            var template = temp.clone ();
            var templates = {
                "groupid": gracc[i].group.groupid,
                "groupname": gracc[i].group.groupname};
            var html = template.html(templater(templates, template.html ()));
            parentContainer.append (html);
        }
    }
    $(parentContainer).append(cloneable);
}

function renderUsers (usacc){
    var parentContainer = $("#useraccesspanel li.cloneable").closest("ul");
    var temp = $("#useraccesspanel li.cloneable").detach();
    var cloneable = temp.clone();
    temp.removeClass("cloneable");
    parentContainer.html("");
    if (usacc.length){
        for (i in usacc) {
            var template = temp.clone ();
            var templates = {
                "userid": usacc[i].user.userId,
                "surname":usacc[i].user.surname,
                "name":usacc[i].user.name,
                "nick":usacc[i].user.nick,
                "userMail":usacc[i].user.userMail};

            var html = template.html(templater(templates, template.html ()));
            parentContainer.append (html);
        }
    }
    $(parentContainer).append(cloneable);
}

function renderAccesses(gracc, usacc){
    // its fucking shity-hack
    dialogs.hidePleaseWait();
    console.log ("wtf!!!");
    renderGroups (gracc);
    renderUsers (usacc);
    $('[data-toggle="tooltip"]').tooltip();
}

function activateAccessAddingForms (){
   if ($("#editTestAccesses [name=access-test-for-all]").is(":checked")){
        $("#add_access_for_user_query").disable();
        $("#add_access_for_group_query").disable();
   }else{
        $("#add_access_for_user_query").enable();
        $("#add_access_for_group_query").enable();
   }
}

function afterLoadAccesses(data, flag){
   dialogs.hidePleaseWait();
   if (data.status == "success" && data.data != "EMPTY_ACCESS") {
        var accesses = data.data;
        //console.log (accesses);
        var gracc = accesses["gr-acc"];
        var usacc = accesses["us-acc"];
        if (flag){
            //console.log ("WTF!!!!!!!!!!!!!!!!!!!!");
            var foundAllAccessed = false;
            for (i in gracc) {
                if (gracc[i].group.groupid == 2/*all*/){
                    foundAllAccessed = true;
                    break;
                }
            }
            if (foundAllAccessed)
                $("#editTestAccesses [name=access-test-for-all]").prop("checked", true);
            else
                $("#editTestAccesses [name=access-test-for-all]").prop("checked", false);
        }
        renderAccesses(gracc, usacc);
   }else{
        $("#editTestAccesses [name=access-test-for-all]").prop("checked", false);
        renderAccesses([], []);
   }
   activateAccessAddingForms();
}


function afterSubmitAllAccess (data){
    dialogs.hidePleaseWait();
    //console.log ("step 3: "+data);
    if (data.status == "success"){
        $(".alert.testaccess-saver-success").show();
        var testid = $("#editTestAccesses .testForGroupAccessId").val ();
        //console.log ("step 4: testid= "+testid);
        loadAccesses (testid, function (data){
            afterLoadAccesses(data, false);
        });
    }else{
    //TODO notification
        $(".alert.testaccess-saver-error").show();
    }
}

