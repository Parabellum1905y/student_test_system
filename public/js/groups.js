function afterAddUserToGroup(data){
    dialogs.hidePleaseWait();
    if (data.status == "success"){
        var groupId = data.data["groupId"];
        var userId = data.data["userId"];
        var userSugg;
        for (usuggest in usersId)
            if (usersId[usuggest] == userId) {
                userSugg = usuggest;
                break;
            }
        $('.collapse.demos#demo'+groupId).collapse('hide');
        $('.collapse.demos#demo'+groupId).collapse('show');
        $("#query").val("");
    }else{
    //TODO notification
        $("#query").val("");
    }
}

function afterAddGroup(data){
    dialogs.hidePleaseWait();
    if (data.status == "success"){
        var groupId = data.data["groupId"];
        var userId = data.data["userId"];
        var userSugg;
        for (usuggest in usersId)
            if (usersId[usuggest] == userId) {
                userSugg = usuggest;
                break;
            }
        location.reload();
    }else{
    //TODO notification
        $("#query").val("");
    }
}

function afterDellUserToGroup(data){
    dialogs.hidePleaseWait();
    if (data.status == "success"){
        var groupId = data.data["groupId"];
        $('.collapse.demos#demo'+groupId).collapse('hide');
        $('.collapse.demos#demo'+groupId).collapse('show');
    }else{
    //TODO notification
    }
}

function afterDellGroup(data){
    dialogs.hidePleaseWait();
    if (data.status == "success"){
       location.reload();
    }else{
       location.reload();
    }
}

function loadGroupsUsers (object){
    var uri = $(object).data("usersuri");
    var gid = $(object).data("gid");
    var isowner = $(object).data("isowner");
    loadDataAsync (uri,{type:"GET",his:false, p:null, setParams:false},
        {}, null,
        function(data){
            dialogs.hidePleaseWait ();
            object.html(data);
            if (isowner) $(".bgroup" + gid).show();
            /*if (data.status)
            if (data.status == "success")*/
        });
}

$(document).ready (function (){
    $('[data-toggle="tooltip"]').tooltip();

    $('.collapse.demos').on('show.bs.collapse', function () {
        dialogs.showPleaseWait();
        var object = $(this);
        loadGroupsUsers (object);
    });

    $("body").on ("click", ".addusers", function (e){
        var groupid = $(this).data("groupid");
        $("#groupId").val (groupid);
        //$('.collapse.demos').collapse('hide');
    });

    $("body").on ("click", ".delusergroup", function (e){
        var groupid = $(this).data("groupid");
        var userid = $(this).data("userid");
        $("#deluser>[name=userId]").val (userid);
        $("#deluser>[name=groupId]").val (groupid);
        $("#deluser").submit();
        //$('.collapse.demos').collapse('hide');
    });

    $('#query').typeahead({
            source: function (query, process) {
                    return loadDataAsync (xitrum.withBaseUrl("dropdown?pattern="+query),
                                            {type:"GET",his:false, p:null, setParams:false},
                           {}, null,
                           function(data){
                               if (data.status == "success") {
                                    usersName = [];
                                    usersId = {};
                                    $.each( data.data, function (index, user){
                                        usersName.push (user.suggest);
                                        usersId[user.suggest] = user.id;
                                    });
                                    return process (usersName);
                               }
                               return process ([]);
                        });

                },
                updater : function (item) {
                    $("#addingUserId").val (usersId[item]);
                    $("#form-add-user-to-group").submit ();
                    return item;
                }
            //['alpha','allpha2','alpha3','bravo','charlie','delta','epsilon','gamma','zulu']
    });

    $('.tt-query').css('background-color','#fff');

    $("#form-add-user-to-group").submit (function (e){newPostCallback(e, this, afterAddUserToGroup, false);});

    $("#form-add-group").submit (function (e){newPostCallback(e, this, afterAddUserToGroup, false);});

    $("body").on ("submit", "#deluser", function (e){newPostCallback(e, this, afterDellUserToGroup, false);});

    $("body").on ("submit", "#delgroup", function (e){newPostCallback(e, this, afterDellGroup, false);});

    $("body").on ("click", ".deletegroup", function (e){
        $("#delgroup [name=groupid]").val($(this).data("groupid"));
        $("#delgroup").submit();
    });
});

var usersName = new Array ();
var usersId = {};

var groupsName = new Array ();
var groupsId = {};