organization := "tv.cntt"
name         := "tester"
version      := "1.0-SNAPSHOT"

scalaVersion := "2.11.7"
scalacOptions ++= Seq("-deprecation", "-feature", "-unchecked")

// Xitrum requires Java 8
javacOptions ++= Seq("-source", "1.8", "-target", "1.8")
ivyScala := ivyScala.value map { _.copy(overrideScalaVersion = true) }
//ideaExcludeFolders += ".idea"

//ideaExcludeFolders += ".idea_modules"
//------------------------------------------------------------------------------

libraryDependencies += "tv.cntt" %% "xitrum" % "3.26.1"

// Xitrum uses SLF4J, an implementation of SLF4J is needed
libraryDependencies += "ch.qos.logback" % "logback-classic" % "1.1.7"

// For writing condition in logback.xml
libraryDependencies += "org.codehaus.janino" % "janino" % "2.7.8"

libraryDependencies += "org.webjars" % "bootstrap" % "3.3.6"
libraryDependencies += "org.webjars.bower" % "tether" % "1.3.2"
libraryDependencies += "org.webjars" % "Bootstrap-3-Typeahead" % "3.1.1"
libraryDependencies += "org.webjars.bower" % "bootstrap-checkbox" % "1.2.5"
libraryDependencies += "org.webjars.bower" % "bootstrap-timepicker" % "0.5.2"
// dbcp and postgresql
libraryDependencies ++= Seq(
  "org.postgresql" % "postgresql" % "9.4.1208",
  "org.apache.commons" % "commons-dbcp2" % "2.1.1",
  "com.zaxxer" % "HikariCP" % "2.4.6"
)
// AHC Netty Async Http client
libraryDependencies += "com.ning" % "async-http-client" % "1.9.33"
// json4s Native
libraryDependencies += "org.json4s" % "json4s-native_2.11" % "3.3.0"
libraryDependencies += "com.fasterxml.jackson.datatype" % "jackson-datatype-jdk8" % "2.7.4"
// Slick
libraryDependencies += "com.typesafe.slick" %% "slick" % "3.1.1"
libraryDependencies += "com.typesafe.slick" %% "slick-codegen" % "3.1.1"
// Slick PG extensions
libraryDependencies += "com.github.tminglei" %% "slick-pg" % "0.14.0"
libraryDependencies += "com.github.tminglei" %% "slick-pg_date2" % "0.14.0"
libraryDependencies += "com.github.tminglei" %% "slick-pg_json4s" % "0.14.0"
//libraryDependencies += "com.owlike" % "genson" % "1.3"
// elastic search
libraryDependencies += "com.sksamuel.elastic4s" % "elastic4s-core_2.11" % "2.3.0"
libraryDependencies += "com.sksamuel.elastic4s" % "elastic4s-jackson_2.11" % "2.3.0"
// Java mail wrapping
resolvers += "softprops-maven" at "http://dl.bintray.com/content/softprops/maven"

libraryDependencies += "me.lessis" %% "courier" % "0.1.3"
// Scalate template engine config for Xitrum -----------------------------------

libraryDependencies += "tv.cntt" %% "xitrum-scalate" % "2.5"
// Scala wrapper for Image Processing
libraryDependencies += "com.sksamuel.scrimage" %% "scrimage-core" % "2.1.0"

libraryDependencies += "com.sksamuel.scrimage" %% "scrimage-io-extra" % "2.1.0"

libraryDependencies += "com.sksamuel.scrimage" %% "scrimage-filters" % "2.1.0"
// Java HTML sanitizer
libraryDependencies += "org.jsoup" % "jsoup" % "1.8.3"

// Hazelcast Cache


libraryDependencies += "com.hazelcast" % "hazelcast" % "3.4"

libraryDependencies += "com.hazelcast" % "hazelcast-client" % "3.4"

libraryDependencies += "tv.cntt" %% "xitrum-hazelcast3" % "1.13"

/*libraryDependencies += "tv.cntt" %% "glokka" % "2.3"*/
// scheduling
libraryDependencies += "com.enragedginger" %% "akka-quartz-scheduler" % "1.5.0-akka-2.4.x"

// Precompile Scalate templates
seq(scalateSettings:_*)

ScalateKeys.scalateTemplateConfig in Compile := Seq(TemplateConfig(
  baseDirectory.value / "src" / "main" / "scalate",
  Seq(),
  Seq(Binding("helper", "xitrum.Action", true))
))

// xgettext i18n translation key string extractor is a compiler plugin ---------

autoCompilerPlugins := true
addCompilerPlugin("tv.cntt" %% "xgettext" % "1.3")
scalacOptions += "-P:xgettext:xitrum.I18n"

// Put config directory in classpath for easier development --------------------

// For "sbt console"
unmanagedClasspath in Compile <+= (baseDirectory) map { bd => Attributed.blank(bd / "config") }

// For "sbt run"
unmanagedClasspath in Runtime <+= (baseDirectory) map { bd => Attributed.blank(bd / "config") }

// Copy these to target/xitrum when sbt xitrum-package is run
XitrumPackage.copy("config", "public", "script")

